 <?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

wc_print_notices();

do_action( 'woocommerce_before_cart' ); 
wp_enqueue_script('iosSlider'); ?>

<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

<div class="row">
<div class="col">
<?php do_action( 'woocommerce_before_cart_table' ); ?>

<div class="common_units">
<div class="common_units_goods">
<table id="sf_cart" style="width: 100%; border: none;">
<?php do_action( 'woocommerce_before_cart_contents' ); ?>
<tbody>
<?php
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
<tr class="productInCart">
<td rowspan="2"><span class="product-remove">
<?php
							echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s">x</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
						?>
</span>
<?php
							$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

							if ( ! $_product->is_visible() )
								echo $thumbnail;
							else
								printf( '<a href="%s">%s</a>', $_product->get_permalink(), $thumbnail );
						?>
</td>
<td style="color: #1e59a1; font-size: 23px;">Товар</td>
<td style="color: #1e59a1; font-size: 23px;">Цена</td>
</tr>
<tr style="border-bottom: 3px solid #1E59A1;">
<td style="font-size: 13px;">
						<?php
							if ( ! $_product->is_visible() )
								echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key );
							else
								echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', $_product->get_permalink(), $_product->get_title() ), $cart_item, $cart_item_key );

							// Meta data
							echo WC()->cart->get_item_data( $cart_item );

               				// Backorder notification
               				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) )
               					echo '<p class="backorder_notification">' . __( 'Available on backorder', 'woocommerce' ) . '</p>';
						?>
</td>
<td style="font-size: 13px;">
						<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
						?>
</td>
				<?php
			}
		}

		do_action( 'woocommerce_cart_contents' );
		?>
</tr>
<tr class="couponInCart" style="border-bottom: 3px solid #1E59A1;">
<td><img src="/wp-content/uploads/2016/06/2.png" alt="" /></td>
<td style="font-size: 13px;" colspan="2" class="actions">
				<?php if ( WC()->cart->coupons_enabled() ) { ?>
					<div class="coupon">

						<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e( 'Coupon code', 'woocommerce' ); ?>" /> <input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Coupon', 'woocommerce' ); ?>" />

						<?php do_action('woocommerce_cart_coupon'); ?>

					</div>
				<?php } ?>
</td>
</tr>
<?php do_action( 'woocommerce_after_cart_contents' ); ?>
<tr class="collateralsInCart">
<?php do_action( 'woocommerce_cart_collaterals' ); ?>
<td rowspan="4"><img src="/wp-content/uploads/2016/06/1.png" alt="" /></td>
<td style="color: #1e59a1; font-size: 23px;" colspan="2">Cумма в корзине</td>
</tr>
<tr>
<td style="font-size: 13px;">Ваш заказ:</td>
<td style="font-size: 13px;"><?php wc_cart_totals_subtotal_html(); ?></td>
</tr>
<tr>
<td style="font-size: 13px;">
<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
<p class="cart-discount coupon-<?php echo esc_attr( $code ); ?>"><?php wc_cart_totals_coupon_label( $coupon ); ?></p>
<?php endforeach; ?>
</td>
<td style="font-size: 13px;"><?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
<p class="cart-discount coupon-<?php echo esc_attr( $code ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></p>
<?php endforeach; ?></td>
</tr>
<tr style="border-bottom: 3px solid #1E59A1;">
<td style="font-size: 13px;">Всего к оплате:</td>
<td style="font-size: 13px;"><?php wc_cart_totals_order_total_html(); ?></td>
</tr>
</tbody>
</table>
    <a href="/checkout/" class="checkout-button button alt wc-forward">Далее</a>
</div>
</div>





</div><!--/span-8-->
</div><!--/row-->


<?php do_action( 'woocommerce_after_cart_table' ); ?>


</form>

<?php do_action( 'woocommerce_after_cart' ); ?>
