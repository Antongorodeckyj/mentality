<?php 

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));

    if ( is_rtl() ) 
   		wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}

// [login_or_use_it_link]
function create_login_sensitive_link () {
	if(is_user_logged_in()) {
		return "<a class='gray-light-button' href='http://demo.scanface.com.ua/facecheck_upload/'>Тестирование</a>";
	} else {
		return "<a class='gray-light-button' href='http://demo.scanface.com.ua/facecheck_login/'>Регистрация</a>";
	}
}
add_shortcode ('login_or_use_it_link', 'create_login_sensitive_link' );

add_filter( 'woocommerce_add_to_cart_message', 'custom_add_to_cart_message' );
add_filter( '_add_to_cart_message', 'custom_add_to_cart_message' );
function custom_add_to_cart_message( $message  ){
return '';
}

// [workplace_user_name]
function create_workplace_user_name () {
	$current_user = wp_get_current_user();
	if(is_user_logged_in()) {
            if(sfcanface_user_tests()>0)
                $msg = "Доступно <span>" . sfcanface_user_tests() . "</span> полных тестирований";
            elseif(scanface_user_days()>0)
                $msg = "Доступно <span>" . scanface_user_days() . " дней</span> полных тестирований";
            else 
                $msg = "Нет доступных полных тестирований";
            
		return "<p class='userName'>" . $current_user->display_name . "</p><p>".$msg."</p><a href='/upload-photo' class='button'>Начать тестирование</a>";
	} else {
		return "<p class='linksCTA'><a href='".home_url()."/my-account/?action=register'>Регистрация</a> | <a href='".home_url()."/my-account/'>Вход</a></p><a href='/upload-photo' class='button'>Бесплатное тестирование</a>";
	}
}
add_shortcode ('workplace_user_name', 'create_workplace_user_name' );

// [workplace_banner_widget]
function create_workplace_banner_widget () {
	if(!is_page(1323)) {
		if(is_user_logged_in()) { ?>
<div class='sfBanner'>
<h2>Варианты получения<br>бесплатного тестирования:</h2>
<p class='sfBannerMessage bannerMessageCTA'>Оставь свой отзыв и<br> получи 5 тестирований</p>
<a href='/promotions/' class='button'>Открыть возможность</a>
</div>
<?php	} else { ?>
<div class='sfBanner'>
<h2>Варианты получения<br>бесплатного тестирования:</h2>
<p class='sfBannerMessage bannerMessageCTA'>Оставь свой отзыв и<br> получи 5 тестирований</p>
<a href='/promotions/' class='button'>Открыть возможность</a>
</div>
<?php	}
	}
}
add_shortcode ('workplace_banner_widget', 'create_workplace_banner_widget' );

// [workplace_section_user_name]
function create_workplace_section_user_name () {
	$current_user = wp_get_current_user();
	if(is_user_logged_in()) {
		return $current_user->display_name;
	} else {
		return "<a href='".home_url()."/registration/' class='linksCTA'>Регистрация</a> | <a href='".home_url()."/login/'>Вход</a>";
	}
}
add_shortcode ('workplace_section_user_name', 'create_workplace_section_user_name' );

// [user_orders_count]
function get_user_orders_count() {

if ( ! empty( $customer_orders ) ) {
return count($customer_orders);
} else {
	return "bla-bla-bla";
}
}
add_shortcode ('user_orders_count', 'get_user_orders_count' );


// [history_sidebar_widget]
function create_history_sidebar_widget () {
	if(is_page( 1323 )) {
		echo do_shortcode('[history_scanface_widget]');
	}
}
add_shortcode ('history_sidebar_widget', 'create_history_sidebar_widget' );


// ----- woocommerce data shorcodes:
// [sf_woocommerce_table]
function sf_show_woocommerce_last_orders_table () {
$wcTableForSf = wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) );
return $wcTableForSf;
}
add_shortcode ('sf_woocommerce_table', 'sf_show_woocommerce_last_orders_table');

/*// Separete Login form and registration form */
 add_action('woocommerce_before_customer_login_form','load_registration_form', 2);
 function load_registration_form(){
 if(isset($_GET['action'])=='register'){
 woocommerce_get_template( 'myaccount/form-registration.php' );
 }
 }
 
//add script on report page (id 1323)
	function sf_script_incl(){
        wp_enqueue_script('scanface_report_script', get_template_directory_uri().'/scanface.js');
	}
	add_action('wp_header','sf_script_incl');
 