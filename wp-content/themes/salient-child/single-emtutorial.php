<?php get_header(); ?>

<div class="container-wrap workplaceContainer"><div class="row">
	
	<div class="workplaceContent">
			
			<div id="post-area" class="col span_9 col_last">
				<?php 
				if(have_posts()){ while(have_posts()){ the_post(); ?>
       			<div class="tutorial-name-sc"><h1><?php the_title(); ?></h1></div>
       			<div class="row sfAnotherPostsLinks">
					<div class="prev-sf"><?php previous_post_link( '%link', 'Предыдущая статья' ); ?></div>
					<div class="next-sf"><?php next_post_link( '%link', 'Следующая статья' ); ?></div>
				</div>
       			<?php
       			    the_content(); 
                    }
                }?>
                <div class="row sfAnotherPostsLinks">
					<div class="prev-sf"><?php previous_post_link( '%link', 'Предыдущая статья' ); ?></div>
					<div class="next-sf"><?php next_post_link( '%link', 'Следующая статья' ); ?></div>
				</div>
			</div>
	</div>
	
	<div class="workplaceSidebar">
			
			<div id="sidebar" class="col span_3 left-sidebar">
				<?php get_sidebar(); ?>
			</div>	
	</div>

</div></div>

<?php get_footer(); ?>