<?php get_header(); ?>

<div class="container-wrap">
	
	<div class="container main-content">
		
		<div class="row">
			
			<div class="col span_12 flexContainer404">
				
				<div id="error-404">
				<div class="message404">
					<p>К сожалению, запрашиваемая страница не найдена.</p>
				</div>
				<div class="cta404">
					<p>Всё, что тебе нужно, это</p>
					<p><a href="/upload-photo">пройти тестирование</a></p>
				</div>
				</div>
				
			</div><!--/span_12-->
			
		</div><!--/row-->
		
	</div><!--/container-->

</div>
<?php get_footer(); ?>

