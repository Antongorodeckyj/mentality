<?php
/*
Template Name: UserWorkplace
*/
?>
<?php get_header(); ?>

<div class="container-wrap workplaceContainer"><div class="row">

	<div class="workplaceContent col span_9 col_last" style="margin-right: 0;">
			
			<div id="post-area" class="">
				<?php 
				if(have_posts()){
                                    while(have_posts()){
                                        the_post();
                                        the_content(); 
                                    }
                                }
                                ?>
			</div>
	</div>
	
	<div class="workplaceSidebar col span_3 left-sidebar" style="margin-right: 0;">
			
			<div id="sidebar" class="">
				<?php get_sidebar(); ?>
			</div>	
	</div>

</div></div>
<script>
jQuery(document).ready(function(){
    jQuery("#Howto").hide();
    jQuery(".infoSwitcher").click(function(){
        jQuery("#Howto").toggle();
    });
});
</script>
<?php get_footer(); ?>