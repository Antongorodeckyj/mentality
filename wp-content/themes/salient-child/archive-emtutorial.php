<?php get_header(); ?>

<div class="container-wrap workplaceContainer"><div class="row">
	
	<div class="workplaceContent">	
			<div id="post-area" class="col span_9 col_last">
				<div class="instruction-sf"><h1>Инструкция использования сервиса Сканфейс</h1>
				<ul>
				<?php
					if(have_posts()) : while(have_posts()) : the_post(); ?>
						<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" ><?php the_title(); ?></a></li>

				<?php endwhile; endif; ?>
				</ul>
				</div>
			</div>
	</div>
	
	<div class="workplaceSidebar">
			
			<div id="sidebar" class="col span_3 left-sidebar">
				<?php get_sidebar(); ?>
			</div>	
	</div>

</div></div>

<?php get_footer(); ?>