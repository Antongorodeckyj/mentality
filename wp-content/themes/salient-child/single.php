<?php get_header(); ?>
	<div class="container-wrap">
	<!-- section -->
	<div class="container main-content">

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
<div class="row">
			<div class="Post-name-sc"><h1 style="text-align: center; padding-top: 14px; font-size: 30px; font-weight: 200;"><?php the_title(); ?></h1></div>
		<!-- article -->
		<article id="post-<?php the_ID(); ?>" class="col span_9" style="margin-top: -28px">
			<div class="row sfMainPost">
		<div class="row postBackgroundImg">
			<!-- post thumbnail -->
			<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<?php the_post_thumbnail(); // Fullsize image for the single post ?>
			<?php endif; ?>
			<!-- /post thumbnail -->
		</div><!--/div.postBackgroundImg -->
		<!-- post details -->
			<div class="row postDetails">
			<!-- post title -->
			<p class="date-sc"><?php the_time('F j, Y'); ?> <?php the_category(', '); // Separated by commas ?></p> 
			<div class="tags-sc"><?php the_tags( 'tags: ', ', ', '<br>'); // Separated by commas with a line break at the end ?></div>
			<!-- /post title -->
			</div>
			<!-- /post details -->
		<div class="row sfMainPostContent">
			<?php the_content(); // Dynamic Content ?>
		</div><!--/div.sfMainPostContent -->
		<div class="row sfAnotherPostsLinks">
			<?php previous_post_link( '%link', 'Предыдущая статья' ); ?>
			<?php next_post_link( '%link', 'Следующая статья' ); ?>
		</div>
		</div><!--/div.sfMainPost -->
		</article>
		<!-- /article -->

			<div id="sidebar" class="col span_3 col_last">
				<?php get_sidebar(); ?>
			</div><!--/span_9-->
		</div>
</div>

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1>Sorry, nothing to display.</h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</div>
	<!-- /section -->
	</div>

<?php get_footer(); ?>
