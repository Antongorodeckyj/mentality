<?php 
/* 
Plugin Name: Emed Tutorials Plugin
Description: This plugin is for users tutorials.
Version: 0.0.1
Author: Maeve
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Copyright 2016  Maeve  (email  :  maeve@ex.ua) This program is free software;  you can redistribute it and/or modify it under the terms of the GNU General Public  License as published by the Free Software Foundation;  either version 2 of the License,  or (at your option)  any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program;  if not, write to the Free Software Foundation,  Inc.,  51 Franklin St,  Fifth Floor,  Boston,  MA  02110-1301  USA
*/

// activate actions
function emtuts_activate()
{
    // trigger our function that registers the custom post type
    emtuts_tutorial_post_type();
 
    // clear the permalinks after the post type has been registered
    flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'emtuts_activate');

//deactivate actions
function emtuts_deactivation()
{
    // our post type will be automatically removed, so no need to unregister it
 
    // clear the permalinks to remove our post type's rules
    flush_rewrite_rules();
}
register_deactivation_hook(__FILE__, 'emtuts_deactivation');

// include folders
define('EMED_REGISTRATION_INCLUDE_URL', plugin_dir_url(__FILE__).'includes/');
define('EMED_REGISTRATION_IMAGES_URL', plugin_dir_url(__FILE__).'images/');
define('EMED_REGISTRATION_CSS_URL', plugin_dir_url(__FILE__).'css/');
define('EMED_REGISTRATION_JS_URL', plugin_dir_url(__FILE__).'js/');

//Adding frontend Styles from includes folder
/* function emed_style_incl(){
          wp_enqueue_style('emed_style_css_and_js', EMED_REGISTRATION_CSS_URL."tutorial-style.css");
          wp_enqueue_script('emed_style_css_and_js', EMED_REGISTRATION_JS_URL."tutorial-script.js");
}
add_action('wp_footer','emed_style_incl');
*/
// register "tutorial" custom post type
if ( ! function_exists('emtuts_tutorial_post_type') ) {
// Register Custom Post Type
function emtuts_tutorial_post_type() {
  $labels = array(
    'name'                  => _x( 'tutorials', 'Post Type General Name', 'emed_tutorial' ),
    'singular_name'         => _x( 'tutorial', 'Post Type Singular Name', 'emed_tutorial' ),
    'menu_name'             => __( 'tutorials', 'emed_tutorial' ),
    'name_admin_bar'        => __( 'tutorial', 'emed_tutorial' ),
    'archives'              => __( 'Item Archives', 'emed_tutorial' ),
    'parent_item_colon'     => __( 'Parent Item:', 'emed_tutorial' ),
    'all_items'             => __( 'All tutorials', 'emed_tutorial' ),
    'add_new_item'          => __( 'Add New tutorial', 'emed_tutorial' ),
    'add_new'               => __( 'New tutorial', 'emed_tutorial' ),
    'new_item'              => __( 'New Item', 'emed_tutorial' ),
    'edit_item'             => __( 'Edit tutorial', 'emed_tutorial' ),
    'update_item'           => __( 'Update tutorial', 'emed_tutorial' ),
    'view_item'             => __( 'View tutorial', 'emed_tutorial' ),
    'search_items'          => __( 'Search tutorials', 'emed_tutorial' ),
    'not_found'             => __( 'No tutorials found', 'emed_tutorial' ),
    'not_found_in_trash'    => __( 'No tutorials found in Trash', 'emed_tutorial' ),
    'featured_image'        => __( 'Featured Image', 'emed_tutorial' ),
    'set_featured_image'    => __( 'Set featured image', 'emed_tutorial' ),
    'remove_featured_image' => __( 'Remove featured image', 'emed_tutorial' ),
    'use_featured_image'    => __( 'Use as featured image', 'emed_tutorial' ),
    'insert_into_item'      => __( 'Insert into item', 'emed_tutorial' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'emed_tutorial' ),
    'items_list'            => __( 'Items list', 'emed_tutorial' ),
    'items_list_navigation' => __( 'Items list navigation', 'emed_tutorial' ),
    'filter_items_list'     => __( 'Filter items list', 'emed_tutorial' ),
  );
  $args = array(
    'label'                 => __( 'tutorial', 'emed_tutorial' ),
    'description'           => __( 'tutorial information pages.', 'emed_tutorial' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
    'taxonomies'            => array( 'category', 'purpose', 'post_tag' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-images-alt2',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'emtutorial', $args );
}
add_action( 'init', 'emtuts_tutorial_post_type', 0 );
}