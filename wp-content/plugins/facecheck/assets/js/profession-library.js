jQuery(document).ready(function ($) {
    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $(".LinkLibraryCat"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            jQuery('.LinkLibraryCat ul').hide();
            jQuery('.linklistcatname').removeClass('open');
        }
    });
});
