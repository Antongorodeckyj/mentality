/**
 * Created by Vlad on 06.04.2016.
 */

'use strict';
/*
var videoElement = document.querySelector('video');
// var audioSelect = document.querySelector('select#audioSource');
var videoSelect = document.querySelector('select#videoSource');
var canvas = document.querySelector('canvas');
var ctx = canvas.getContext('2d');
navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
*/
    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
var dataUrl;

function snapshot() {
/*    if (window.stream) {
        var width = videoElement.videoWidth;
        var height = videoElement.videoHeight;
        jQuery('canvas').attr('height', height);
        jQuery('canvas').attr('width', width);
        ctx.drawImage(videoElement, 0, 0);
        // "image/webp" works in Chrome 18. In other browsers, this will fall back to image/png.
        document.getElementById('snapshotImg').src = canvas.toDataURL('image/jpeg');
    }*/
    jQuery('#SendImg').removeAttr('disabled').addClass('active');
    jQuery('#SendImg-img').addClass('active');
    jQuery('#my_camera').hide();
    jQuery('#snapshot').hide();
    jQuery('#snapshot-img').hide();
    jQuery('#back_snapshot, #back_snapshot-img').show();
    Webcam.snap( function(data_uri) {
        document.getElementById('snapshotImg').src = data_uri;
        dataUrl = data_uri;
    } );
}
jQuery('#snapshot').on('click', function (e) {
    e.preventDefault();
    snapshot();
});
jQuery('#back_snapshot').on('click', function (e) {

    jQuery('#SendImg-img').addClass('active');
    jQuery('#back_snapshot, #back_snapshot-img').hide();
    jQuery('#SendImg').attr('disabled', true).removeClass('active');
    jQuery('#SendImg-img').removeClass('active');
    jQuery('#snapshot, #snapshot-img').show();
    jQuery('#my_camera').show();
    document.getElementById('snapshotImg').src = '';
    jQuery(this).hide();

});
/*
function gotSources(sourceInfos) {
    for (var i = 0; i !== sourceInfos.length; ++i) {
        var sourceInfo = sourceInfos[i];
        var option = document.createElement('option');
        option.value = sourceInfo.id;
        if (sourceInfo.kind === 'audio') {
            option.text = sourceInfo.label || 'microphone ' +
                (audioSelect.length + 1);
            // audioSelect.appendChild(option);
        } else if (sourceInfo.kind === 'video') {
            option.text = sourceInfo.label || 'camera ' + (videoSelect.length + 1);
            videoSelect.appendChild(option);
        } else {
            console.log('Some other kind of source: ', sourceInfo);
        }
    }
}

if (typeof MediaStreamTrack === 'undefined' ||
    typeof MediaStreamTrack.getSources === 'undefined') {
    //alert('This browser does not support MediaStreamTrack.\n\nTry Chrome.');
} else {
    MediaStreamTrack.getSources(gotSources);
}

function successCallback(stream) {
    window.stream = stream; // make stream available to console
    videoElement.src = window.URL.createObjectURL(stream);
    videoElement.play();
}

function errorCallback(error) {
    console.log('navigator.getUserMedia error: ', error);
    jQuery('#webCamCapture').text('Фото с веб-камеры');
    jQuery('#uploader-wrapper').show();
    jQuery('.wrap-capture').hide();
    chk = false;
    // alert("Извините произошла ошибка, Либо Вы не дали разрешение на использование камеры либо Ваша камера не подключена");
    jQuery.magnificPopup.close();
    jQuery.magnificPopup.open({
        items: {
            src: jQuery('#error-cam'),
            type: 'inline'
        }
    });
}
*/
function start() {
    jQuery('#uploader-wrapper').hide();
    jQuery('.wrap-capture').show();
   
    Webcam.reset();
    Webcam.attach( '#my_camera' );
    jQuery('video').css('visibility','visible');

/*    if (window.stream) {
        videoElement.src = null;
        videoElement.pause();
        // window.stream.stop();
    }
    // var audioSource = audioSelect.value;
    // var videoSource = videoSelect.value;
    var constraints = {
        // audio: {
        //     optional: [{
        //         sourceId: audioSource
        //     }]
        // },
        // video: {
        //     optional: [{
        //         sourceId: videoSource
        //     }]
        // }
        video: {
            maxHeight: 180,
            maxWidth: 320
        }
    };
    navigator.getUserMedia = navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;
    // if (navigator.getUserMedia) {
        navigator.getUserMedia(constraints, successCallback, function (err) {
            errorCallback(err);
        });
    // }else{
    //     errorCallback("error");
        //alert("Произошла ошибка");
    // }
*/
}

// audioSelect.onchange = start;
// videoSelect.onchange = start;
var chk;
jQuery('.webCamCapture').on('click', function (e) {
    if (!chk) {
        jQuery(this).hide();
        chk = 1;
 //       e.preventDefault();
        start();
    } else {
/*        if (window.stream) {
            videoElement.src = "";
            videoElement.pause();
            // window.stream.stop();
        }*/
        //jQuery('#SendImg').attr('disabled', true);
        jQuery('.webCamCapture').show();
        jQuery('#uploader-wrapper').show();
        jQuery('.wrap-capture').hide();
        chk = false;
    }

});
var timer;
// SendImg
jQuery('#SendImg').on('click', function (e) {
    jQuery.magnificPopup.close();
    jQuery.magnificPopup.open({
        items: {
            src: jQuery('#upload-photo'),
            type: 'inline'
        }
    });
//    var dataUrl = canvas.toDataURL('image/jpeg');
    jQuery.ajax({
        url: '/wp-admin/admin-ajax.php',
        type: 'post',
        data: {
            action: 'facecheck_base64',
            data: dataUrl
        },
        success: function (data) {
            data = data.replace(/\r|\n| /g, '');
            if (data == "error_permission") {
                jQuery.magnificPopup.close();
                jQuery.magnificPopup.open({
                    items: {
                        src: jQuery('#permission-err-photo'),
                        type: 'inline'
                    }
                });
            } else {
                if (data == 0 || data) {
                    var photo_id = data;
                    timer = setInterval(function () {
                        jQuery.ajax({
                            url: '/wp-admin/admin-ajax.php',
                            type: 'post',
                            data: {
                                action: 'facecheck_process_photo',
                                id: photo_id
                            },
                            success: function (data) {
                                data = data.replace(/\r|\n| /g, '');
                                if (data == 'not_face') {
                                    clearInterval(timer);
                                    jQuery.magnificPopup.close();
                                    jQuery.magnificPopup.open({
                                        items: {
                                            src: jQuery('#bad-photo'),
                                            type: 'inline'
                                        }
                                    });
                                } else if (data == 1) {
                                    clearInterval(timer);
                                    document.location = redirectUrl + (redirectUrl.match(/\?/) ? '&facecheck_photo_id=' : '?facecheck_photo_id=' ) + photo_id;
                                }
                            }
                        })
                    }, 2000);
                } else {
                    jQuery.magnificPopup.close();
                    jQuery.magnificPopup.open({
                        items: {
                            src: jQuery('#bad-photo'),
                            type: 'inline'
                        }
                    });
                }
            }
        },
        error: function () {
            jQuery.magnificPopup.close();
            jQuery.magnificPopup.open({
                items: {
                    src: jQuery('#err-photo'),
                    type: 'inline'
                }
            });
        }
    })
});
// start();