function initUploader() {
    var $input = jQuery('#file-input');
    var timer;

   

    $input.fileupload({
        url: '/wp-admin/admin-ajax.php',
        dataType: 'json',
        dropZone: $input,
        pasteZone: null,
        fileInput: $input,
        limitMultiFileUploads: 1,
        formData: {
            action: 'facecheck_upload_photo'
        },
        start: function () {
            jQuery.magnificPopup.open({
                items: {
                    src: jQuery('#upload-popup'),
                    type: 'inline'
                },
                closeOnBgClick: false
            })
        },
        done: function (e, data) {
            console.log(data);
            if (data.result == "error_permission") {
                jQuery.magnificPopup.close();
                jQuery.magnificPopup.open({
                    items: {
                        src: jQuery('#permission-err-photo'),
                        type: 'inline'
                    }
                });
            } else {
                if (data.result == 0 || data.result) {
                    var photo_id = data.result;
                    var adminLog = jQuery('#admin_logs').is( ":checked" ) ? "1" : ""
                    timer = setInterval(function () {
                        jQuery.ajax({
                            url: '/wp-admin/admin-ajax.php',
                            type: 'post',
                            data: {
                                action: 'facecheck_process_photo',
                                id: photo_id,
                                log:adminLog
                            },
                            success: function (data) {
                                if (data == 'not_face') {
                                    clearInterval(timer);
                                    jQuery.magnificPopup.close();
                                    jQuery.magnificPopup.open({
                                        items: {
                                            src: jQuery('#bad-photo'),
                                            type: 'inline'
                                        }
                                    });
                                } else if (data == 1) {
                                    clearInterval(timer);
                                    document.location = redirectUrl + (redirectUrl.match(/\?/) ? '&facecheck_photo_id=' : '?facecheck_photo_id=' ) + photo_id;
                                }
                            }
                        })
                    }, 2000);
                } else {
                    jQuery.magnificPopup.close();
                    jQuery.magnificPopup.open({
                        items: {
                            src: jQuery('#bad-photo'),
                            type: 'inline'
                        }
                    });
                }
            }
        },
        fail: function (e, data) {
            jQuery.magnificPopup.close();
            jQuery.magnificPopup.open({
                items: {
                    src: jQuery('#err-photo'),
                    type: 'inline'
                }
            });
        }
    });
}

jQuery(function () {
    initUploader();

    jQuery(document).bind('drop dragover', function (e) {
        e.preventDefault();
    });
});

// jQuery(document).ready(function(){
//         jQuery.ajax({
//             url: '/wp-admin/admin-ajax.php',
//             type: 'post',
//             data: {
//                 action: 'test'
//             },
//             success: function (data) {
//                 console.log(data)
//             }
//         })
// });