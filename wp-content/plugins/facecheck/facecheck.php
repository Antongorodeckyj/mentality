<?php
/*
Plugin Name: Facecheck
Description: Make Psychoreports by uploaded photos. Manage reports.
Version: 2.0
Author: Web101
Author URI: http://web101.com.ua/
*/

include __DIR__ . '/classes/facecheck-user.php';//Класс FacecheckUser отвечает за авторизацию и регистрацию пользователей
include __DIR__ . '/classes/facecheck.php';//Главный класс Facecheck отвечает за работу плагина

register_activation_hook(__FILE__, 'facecheck_activate');
register_deactivation_hook(__FILE__, 'facecheck_deactivate');
register_uninstall_hook(__FILE__, 'facecheck_uninstall');

global $params_shortcode;
global $no_access;
/**
 *Активация плагина
 */
function facecheck_activate()
{
    find_shortcode_pages();
    Facecheck::install();
}

/**
 *деактивация плагина
 */
function facecheck_deactivate()
{
    Facecheck::deactivate();
}

/**
 *Удаление плагина
 */
function facecheck_uninstall()
{
    Facecheck::uninstall();
}

add_action('init', 'facecheck_init');//вызов функции facecheck_init()


/**
 *Инициализация плагина
 */
function facecheck_init()
{
    /* Отключаем админ панель для всех, кроме администраторов. */
    if (!current_user_can('administrator')):
        show_admin_bar(false);
    endif;


    add_rewrite_tag('%facecheck_photo_id%', '([0-9]+)');
    add_rewrite_tag('%filter%', '(.*)');
    add_rewrite_tag('%reverse%', '(.*)');
    add_rewrite_tag('%name%', '(.*)');
    add_rewrite_tag('%facecheck_category%', '([0-9]+)');
    add_rewrite_tag('%profession%', '([0-9]+)');

    add_option('facecheck_constant_eyetoeye', '10');
    add_option('facecheck_constant_eyestoface', '1.5');
    add_option('facecheck_constant_mlrtombt', '0.3');
    add_option('facecheck_constant_yogl', '5');
    add_option('facecheck_constant_yrkb', '20');

    // Custom routes
    add_action('init', 'add_routes');

    flush_rewrite_rules();

    //NEW PRODUCT TYPE    
    new_product_type();


    find_shortcode_pages();//поиск страниц с шорткодами

    shortcode_init();//регистрация шорткодов


    Facecheck::init();// инициализация класса плагина

    //logout
    if (isset($_GET['action']) && $_GET['action'] == 'facecheck_user_logout') {
        facecheck_user_logout();
    }

    //защита от просмотра чужих фото
    if (isset($_GET['facecheck_photo_id'])) {
        facecheck_get_photo($_GET['facecheck_photo_id']);
    }

    //check login
    check_login();

}


//admin actions
add_action('admin_init', 'facecheck_admin_init');
add_action('admin_menu', 'facecheck_admin_menu');
add_action('admin_head-facecheck/admin/specifications.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/profcats.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/professions.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/sections.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/reports.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/access_level.php', 'facecheck_admin_head');
add_action('admin_head-facecheck/admin/logs.php', 'facecheck_admin_head');


/**
 * инициализация констант для тестирования
 */
function facecheck_admin_init()
{
    register_setting('facecheck_constants', 'facecheck_constant_eyetoeye');
    register_setting('facecheck_constants', 'facecheck_constant_eyestoface');
    register_setting('facecheck_constants', 'facecheck_constant_mlrtombt');
    register_setting('facecheck_constants', 'facecheck_constant_yogl');
    register_setting('facecheck_constants', 'facecheck_constant_yrkb');
}

/**
 *Создание админ-меню
 */
function facecheck_admin_menu()
{
    global $menu;
    add_menu_page('Scanface', 'Scanface', 'manage_options', 'facecheck/admin/facecheck.php', '', '', 7);
    add_submenu_page('facecheck/admin/facecheck.php', 'Характеристики', 'Характеристики', 'manage_options', 'facecheck/admin/specifications.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Группы профессий', 'Группы профессий', 'manage_options', 'facecheck/admin/profcats.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Профессии', 'Профессии', 'manage_options', 'facecheck/admin/professions.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Sections', 'Sections', 'manage_options', 'facecheck/admin/sections.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Reports', 'Reports', 'manage_options', 'facecheck/admin/reports.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Пользователи', 'Пользователи', 'manage_options', 'facecheck/admin/users.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Уровни доступа', 'Уровни доступа', 'manage_options', 'facecheck/admin/access_level.php');
    add_submenu_page('facecheck/admin/facecheck.php', 'Логи тестирований', 'Логи тестирований', 'manage_options', 'facecheck/admin/logs.php');
//    add_submenu_page('facecheck/admin/reports.php', 'Faceparts', 'Faceparts', 'manage_options', 'facecheck/admin/faceparts.php');
}

/**
 *Ресурсы для админки
 */
function facecheck_admin_head()
{
    echo '<link rel="stylesheet" href="' . plugins_url('facecheck/admin/style.css') . '" />';
    echo '<script type="text/javascript" src="' . includes_url('js/jquery/ui/core.min.js') . '"></script>';
    echo '<script type="text/javascript" src="' . includes_url('js/jquery/ui/widget.min.js') . '"></script>';
    echo '<script type="text/javascript" src="' . includes_url('js/jquery/ui/mouse.min.js') . '"></script>';
    echo '<script type="text/javascript" src="' . includes_url('js/jquery/ui/draggable.min.js') . '"></script>';
    echo '<script type="text/javascript" src="' . plugins_url('facecheck/admin/jquery.fileupload.js') . '"></script>';
}

//ajax request actions

add_action('wp_ajax_facecheck_upload_photo', 'facecheck_upload_photo');
add_action('wp_ajax_nopriv_facecheck_upload_photo', 'facecheck_upload_photo');

add_action('wp_ajax_facecheck_process_photo', 'facecheck_process_photo');
add_action('wp_ajax_nopriv_facecheck_process_photo', 'facecheck_process_photo');

//add_action('wp_ajax_facecheck_reset_markers', 'facecheck_reset_markers');
//add_action('wp_ajax_facecheck_save_markers', 'facecheck_save_markers');
add_action('wp_ajax_facecheck_delete_photo', 'facecheck_delete_photo');
add_action('wp_ajax_facecheck_save_report', 'facecheck_save_report');
add_action('wp_ajax_facecheck_save_user_report_profession', 'facecheck_save_user_report_profession');
add_action('wp_ajax_facecheck_del_user_report_profession', 'facecheck_del_user_report_profession');
add_action('wp_ajax_facecheck_find_profession', 'facecheck_find_profession');

add_action('wp_ajax_facecheck_report_letter', 'facecheck_report_letter');
add_action('wp_ajax_facecheck_upload_sicon', 'facecheck_upload_sicon');
add_action('wp_ajax_facecheck_upload_pcicon', 'facecheck_upload_pcicon');
add_action('wp_ajax_facecheck_delete_profession', 'facecheck_delete_profession');


add_action('wp_ajax_facecheck_base64', 'facecheck_base64');
add_action('wp_ajax_nopriv_facecheck_base64', 'facecheck_base64');


/**
 *функция приема фото с вебкамеры пользователя
 */
function facecheck_base64()
{
    //проверяет права на загрузку
    if (!Facecheck::checkPermissionUpload(get_current_user_id())) {
        echo json_encode("error_permission");
//        echo "error";
        exit();
    }
    $data = $_POST['data'];

    list($type, $data) = explode(';', $data);
    list(, $data) = explode(',', $data);
    $data = base64_decode($data);

    file_put_contents(WP_CONTENT_DIR . '/photos/photo_base64.jpg', $data);

    $id = Facecheck::addPhoto(WP_CONTENT_DIR . '/photos/photo_base64.jpg');

    //инициализация проверки загрузки фото для того чтобы уменьшить в купленном тарифном плане кол-во
    //доступных загрузок
    if (isset($_SESSION['facecheck_user'])) {
        $_SESSION['chk_upl'] = 1;
    }
    echo $id;
    exit;
}

/**
 *функци срабатывает при загрузке фото
 */
function facecheck_upload_photo()
{
    //проверяет права на загрузку
    if (!Facecheck::checkPermissionUpload(get_current_user_id())) {
        echo json_encode("error_permission");
//        echo "error";
        exit();
    }

    if (empty($_FILES['photo']) || $_FILES['photo']['error'] || $_FILES['photo']['size'] == 0) {
        exit;
    }


    $id = Facecheck::addPhoto($_FILES['photo']['tmp_name']);

    //инициализация проверки загрузки фото для того чтобы уменьшить в купленном тарифном плане кол-во
    //доступных загрузок
    if (isset($_SESSION['facecheck_user'])) {
        $_SESSION['chk_upl'] = 1;
    }
    echo $id;
    exit;
}

/**
 *Получение маркеров по фото
 */
function facecheck_process_photo()
{
    if (empty($_POST['id']) || $_POST['id'] <= 0) {
        echo 'not_face';
        exit;
    }
    $_SESSION["facecheck_logs"] = false;
    if (!empty($_POST['log']) && $_POST['log'] == "1") {
        $_SESSION["facecheck_logs"] = true;
    }

    $result = Facecheck::getMarkers($_POST['id']);
    if ($result == 'not_face') {
        echo 'not_face';
    } elseif ($result) {
        echo '1';
    }

    exit;
}

/**
 *Удаление фото
 */
function facecheck_delete_photo()
{
    if (empty($_REQUEST['id'])) {
        exit;
    }

    Facecheck::deletePhoto($_REQUEST['id']);

    if (isset($_REQUEST['redirect'])) {
        wp_redirect($_REQUEST['redirect']);
    }

    exit;
}
/**
 * Сохранение профессии добавленной в отчет
 */
function facecheck_save_user_report_profession()
{
    global $wpdb;
    if (!isset($_POST['prof_id']) || empty($_POST['prof_id']) || !isset($_POST['photo_id']) || empty($_POST['photo_id'])) {
        echo 'error: no all data';
        exit;
    }
    Facecheck::saveUserReportProfession($_SESSION['facecheck_user']->id, $_POST['photo_id'], $_POST['prof_id']);
}
/**
 * Удаление профессии добавленной в отчет
 */
function facecheck_del_user_report_profession()
{
    global $wpdb;
    if (!isset($_POST['id'])) {
        echo 'error: no all data';
        exit;
    }
    $wpdb->delete($wpdb->prefix.'facecheck_user_report_professions', array('id'=>$_POST['id']));
}
/**
 * Поиск профессии
 */
function facecheck_find_profession()
{
    global $wpdb;
    if (!isset($_POST['val']) || !isset($_POST['photo_id'])) {
        echo 'error: no all data';
        exit;
    }
    Facecheck::findProf($_POST['val'],$_POST['photo_id']);
}
/**
 *Сохранение результата
 */
function facecheck_save_report()
{
    global $wpdb;

    if (!isset($_POST['id']) || empty($_POST['id'])) {
        exit;
    }

    if (isset($_POST['groups']) && count($_POST['groups'])) {
        foreach ($_POST['groups'] as $index => $group) {
            if (strpos($group, 'group_') === 0) {
                $group = str_replace('group_', '', $group);
                $_POST['groups'][$index] = $_SESSION['facecheck_user']->addGroup($group);
            }
        }

        Facecheck::updatePhotoGroups($_POST['id'], $_POST['groups']);
    } else {
        Facecheck::updatePhotoGroups($_POST['id'], array());
    }

    Facecheck::savePhoto($_POST['id'], $_POST['report_name'], $_POST['comment']);

    echo 1;
    die();
}

/**
 *Отправка письма
 */
function facecheck_report_letter()
{
    global $wpdb;

    if (isset($_POST['email']) && count($_POST['email'])) {
        $report = facecheck_get_result($_POST['id']);
        $photo = Facecheck::getPhoto($_POST['id']);

        include_once __DIR__ . '/classes/facecheck-report-pdf.php';

        $pdf = new FacecheckReportPDF($_POST['id']);
        $pdf->Output(WP_CONTENT_DIR . '/uploads/report.pdf', 'F');

        ob_start();
        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections`", ARRAY_A);
        $sections = array();
        foreach ($rows as $row) {
            $sections[$row['id']] = $row;
        }

//      $profession = $_POST['profession'];
        $specifications = Facecheck::getSpecifications();

        include __DIR__ . '/templates/report-mail.php';
        $content = ob_get_clean();

        add_filter('wp_mail_content_type', 'facecheck_html_email_type');
        foreach ($_POST['email'] as $email) {
            wp_mail($email, 'Scanface анализ', $content, 'From: "Scanface" <report@scanface.com.ua>', array(WP_CONTENT_DIR . "/uploads/report.pdf"));
            $wpdb->replace("{$wpdb->prefix}facecheck_users_emails", array('id_user' => $_SESSION['facecheck_user']->id, 'email' => $email));
        }
        remove_filter('wp_mail_content_type', 'facecheck_html_email_type');

        echo 1;
    }

    die();
}

/**
 * Добавление иконки секции в админке
 */
function facecheck_upload_sicon()
{
    if (empty($_FILES['sicon']) || $_FILES['sicon']['error'] || $_FILES['sicon']['size'] == 0) {
        exit;
    }

    move_uploaded_file($_FILES['sicon']['tmp_name'], WP_CONTENT_DIR . '/plugins/facecheck/sicons/temp.jpg');
    echo plugins_url('facecheck/sicons/temp.jpg');
    exit;
}

function facecheck_upload_pcicon()
{
    if (empty($_FILES['pcicon']) || $_FILES['pcicon']['error'] || $_FILES['pcicon']['size'] == 0) {
        exit;
    }

    move_uploaded_file($_FILES['pcicon']['tmp_name'], WP_CONTENT_DIR . '/plugins/facecheck/pcicons/temp.jpg');
    echo plugins_url('facecheck/pcicons/temp.jpg');
    exit;
}

/**
 *Удаление профессии
 */
function facecheck_delete_profession()
{
    global $wpdb;

    if (empty($_POST['profession'])) {
        exit;
    }

    $profession = $wpdb->get_row("
        SELECT * FROM `wp_facecheck_professions`
        WHERE `id` = {$_POST['profession']}
    ", ARRAY_A);

    if (empty($profession) || $profession['id_user'] != $_SESSION['facecheck_user']->id) {
        exit;
    }

    $wpdb->query("DELETE FROM `wp_facecheck_professions` WHERE `id` = {$_POST['profession']}");
    $wpdb->query("DELETE FROM `wp_facecheck_professions_specifications` WHERE `id_profession` = {$_POST['profession']}");

    echo 'success';
    exit;
}

//forms actions
//add_action('admin_post_facecheck_user_registration', 'facecheck_user_registration');
//add_action('admin_post_facecheck_user_login', 'facecheck_user_login');
//add_action('admin_post_facecheck_oauth_facebook', 'facecheck_oauth_facebook');
//add_action('admin_post_facecheck_oauth_linkedin', 'facecheck_oauth_linkedin');
//add_action('admin_post_facecheck_user_logout', 'facecheck_user_logout');
//add_action('facecheck_user_logout', 'facecheck_user_logout');
//add_action('admin_post_facecheck_user_password', 'facecheck_user_password');
//add_action('admin_post_facecheck_save_profile', 'facecheck_save_profile');
//add_action('admin_post_facecheck_save_markers', 'facecheck_save_markers');
//add_action('admin_post_facecheck_delete_photo', 'facecheck_delete_photo');
add_action('admin_post_facecheck_save_section', 'facecheck_save_section');
add_action('admin_post_facecheck_save_profession', 'facecheck_save_profession');
add_action('admin_post_facecheck_save_profession_category', 'facecheck_save_profession_category');
add_action('admin_post_facecheck_save_specification', 'facecheck_save_specification');
add_action('admin_post_facecheck_save_access', 'facecheck_save_access');
//add_action('admin_post_facecheck_save_custom_specifications', 'facecheck_save_custom_specifications');


/**
 *Сохранение уровней доступа в админке
 */
function facecheck_save_access()
{
    global $wpdb;
    $access_name = "";
    $slug = "";
    $not_reg = "";
    $registered = "";
    if (isset($_POST['access_name'])) {
        $access_name = $_POST['access_name'];
    } else {
        wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/access_level.php');
    }
    if (isset($_POST['slug'])) {
        $slug = $_POST['slug'];
    } else {
        wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/access_level.php');
    }
    if (isset($_POST['not_reg'])) {
        $not_reg = $_POST['not_reg'];
    } else {
        wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/access_level.php');
    }
    if (isset($_POST['registered'])) {
        $registered = $_POST['registered'];
    } else {
        wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/access_level.php');
    }
    $data = array(
        'name' => $access_name,
        'slug' => $slug,
        'not_reg' => $not_reg,
        'registered' => $registered

    );
    if (isset($_POST['id']) && $_POST['id'] != "") {
        $wpdb->update($wpdb->prefix . 'facecheck_access_level', $data, array('id' => $_POST['id']));
        $wpdb->delete($wpdb->prefix . 'facecheck_access_to_sections', array('id_access' => $_POST['id']));
        $wpdb->delete($wpdb->prefix . 'facecheck_access_to_spec', array('id_access' => $_POST['id']));
    } else {
        $wpdb->insert($wpdb->prefix . 'facecheck_access_level', $data);
        $id = $wpdb->insert_id;
    }
    if (isset($_POST['sections-id'])) {
        $sections = $_POST['sections-id'];
        foreach ($sections as $section) {
            $data = [
                'id_access' => $_POST['id'] ? $_POST['id'] : $id,
                'id_sections' => $section,
            ];
            $wpdb->insert($wpdb->prefix . 'facecheck_access_to_sections', $data);
        }
    }
    if (isset($_POST['specification-id'])) {
        $specifications = $_POST['specification-id'];
        foreach ($specifications as $specification) {
            $data = [
                'id_access' => $_POST['id'] ? $_POST['id'] : $id,
                'id_spec' => $specification,
            ];
            $wpdb->insert($wpdb->prefix . 'facecheck_access_to_spec', $data);
        }
    }


    wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/access_level.php');

}

/**
 * Регистрация пользователя
 * @return array
 */
function facecheck_user_registration()
{
    global $wpdb;
    $errors = array();

    if (empty($_POST['name_scanface'])) {
        $errors['name'] = 'Введите имя и фамилию';
    } elseif (get_user_by('login', $_POST['name_scanface'])) {
        $errors['name'] = 'Такой пользователь существует';
    }

    if (empty($_POST['email'])) {
        $errors['email'] = 'Введите email';
    } elseif (!preg_match('/[0-9a-zA-Z][-0-9a-zA-Z.+_]+@[0-9a-zA-Z][-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $_POST['email'])) {
        $errors['email'] = 'Некорректный email';
    } elseif (FacecheckUser::getByEmail($_POST['email'])) {
        $errors['email'] = 'Пользователь с таким email уже зарегистрирован';
    }

    if (empty($_POST['password'])) {
        $errors['password'] = 'Введите пароль';
    }

    if ($errors) {
        return array(
            'errors' => $errors,
            'name_scanface' => $_POST['name_scanface'],
            'email' => $_POST['email'],
            'day' => $_POST['facecheck_day'],
            'month' => $_POST['facecheck_month'],
            'year' => $_POST['facecheck_year']
        );
    } else {
        $user = new FacecheckUser();
        $user->name = $_POST['name_scanface'];
        $user->email = $_POST['email'];
//        $user->password = md5($_POST['password']);
        $user->password = $_POST['password'];
        $user->date_birth = $_POST['facecheck_year'] . '-' . $_POST['facecheck_month'] . '-' . $_POST['facecheck_day'];
        $res = $user->save();
        if ($res !== true) {
            return $res;
        }

        $wpdb->insert("{$wpdb->prefix}facecheck_users_emails", array('id_user' => $user->id, 'email' => $user->email));

        $groups = array(
            array('user_id' => $user->id, 'name' => 'Работа', 'date' => date('Y-m-d H:i:') . '00'),
            array('user_id' => $user->id, 'name' => 'Сотрудники', 'date' => date('Y-m-d H:i:') . '01'),
            array('user_id' => $user->id, 'name' => 'Личное', 'date' => date('Y-m-d H:i:') . '02')
        );

        $user->groups($groups);

        $creds = array();
        $creds['user_login'] = $user->name;
        $creds['user_password'] = $_POST['password'];
        $creds['remember'] = true;
        $auth = wp_signon($creds, false);
        if (!is_wp_error($auth)) {
            $_SESSION['facecheck_user'] = $user;
            wp_redirect($_POST['success_page']);
//            echo "<script> window.location.href = '" . $_POST['success_page'] . "';</script>";
        }

    }
    return array(
        'errors' => $errors,
        'name_scanface' => $_POST['name_scanface'],
        'email' => $_POST['email'],
        'day' => empty($_POST['day']) ? "" : $_POST['day'],
        'month' => empty($_POST['month']) ? "" : $_POST['month'],
        'year' => empty($_POST['year']) ? "" : $_POST['year']
    );
}
add_action( 'user_register', 'facecheck_registration_save', 10, 1 );

function facecheck_registration_save( $user_id ) 
{
    // Добавление order с 5 заказами
    $order = wc_create_order();
    $order->add_product( get_product('352'), 1, array('totals' => array('subtotal' => 0,'total' => 0))); // продукт 5 тестирований
    $order->calculate_totals();
    $order->update_status('completed');  
    update_post_meta( $order->id, '_customer_user', $user_id );
    Facecheck::saveUserOrder($order->id);
}
/**
 * Залогиневание пользователя
 * @return array
 */
function facecheck_user_login()
{
    $errors = array();

    if (empty($_POST['email'])) {
        $errors['email'] = 'Введите email';
    } elseif (!preg_match('/[0-9a-zA-Z][-0-9a-zA-Z.+_]+@[0-9a-zA-Z][-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $_POST['email'])) {
        $errors['email'] = 'Некорректный email';
    } elseif (!$user = FacecheckUser::getByEmail($_POST['email'])) {
        $errors['email'] = 'Пользователь с таким email не найден';
    }

    if (empty($_POST['password'])) {
        $errors['password'] = 'Введите пароль';
        /* } elseif ($user && md5($_POST['password']) != $user->password) {
             $errors['password'] = 'Неверный пароль';
         }*/
    } elseif (isset($user)) {
        $creds = array();
        $creds['user_login'] = $user->name;
        $creds['user_password'] = $_POST['password'];
        $creds['remember'] = true;
        $auth = wp_signon($creds, false);
        if (is_wp_error($auth)) {
            $errors['password'] = 'Неверный пароль';
        }
    }

    if ($errors) {
        return array(
            'errors' => $errors,
            'email' => $_POST['email']
        );
    } else {
        $_SESSION['facecheck_user'] = $user;
//        wp_redirect($_POST['success_page']);
        echo "<script> window.location.href = '" . $_POST['success_page'] . "';</script>";
    }
}

/**
 * проверка залогинен ли пользователь
 */
function check_login()
{
    if (is_user_logged_in()) {
        $current_user = wp_get_current_user();

        if ($user = FacecheckUser::getByEmail($current_user->user_email)) {
            $_SESSION['facecheck_user'] = $user;
        }
    } else {
        $user = new FacecheckUser();
        $user->id = 0;
        $user->name = '';
        $_SESSION['facecheck_user'] = $user;

        $link = trim($_SERVER["REQUEST_URI"],'/');        
        $only_for_reg_user = [
            'user-profile',
            //'history-report',
        ];
        
        if(in_array($link, $only_for_reg_user)){
            header('Location: '.home_url().'/my-account/');
            exit;
        }

    }
}

/**
 *Вход с помощью Facebook
 */
function facecheck_oauth_facebook()
{
    global $facecheck_shortcode;
    if (isset($_GET['code'])) {
        $url = 'https://graph.facebook.com/oauth/access_token?';
        $url .= 'client_id=759154490783588';
        $url .= '&redirect_uri=' . urlencode(get_permalink($facecheck_shortcode['login_scanface']) . '?action=facecheck_oauth_facebook');
        $url .= '&client_secret=49b42ef3f1d3f06a19e76d4b491e84be';
        $url .= '&code=' . $_GET['code'];

        parse_str(file_get_contents($url), $response);
        if (!isset($response['access_token'])) {
            wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
        }

        $token = $response['access_token'];
        $response = json_decode(file_get_contents('https://graph.facebook.com/me?access_token=' . $token), true);

        if (!$response || isset($response['error']) || !$response['email']) {
            wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
        }

        $user = FacecheckUser::getByEmail($response['email']);
        if (!isset($user)) {
            $user = new FacecheckUser();
            $user->email = $response['email'];
            $user->name = $response['name'];
            $user->date_birth = substr($response['birthday'], 6, 4) . '-' . substr($response['birthday'], 0, 2) . '-' . substr($response['birthday'], 3, 2);
            $user->save();
        }

        $_SESSION['facecheck_user'] = $user;

        if (isset($_SESSION['redirect_result_id'])) {
            wp_redirect(add_query_arg('facecheck_photo_id', $_SESSION['redirect_result_id'], get_permalink($facecheck_shortcode['result_scanface'])));
        } else {
            wp_redirect(get_permalink($facecheck_shortcode['upload_scanface']));
        }
    } else {
        wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
    }
}

/**
 *Вход с помощью linkedin
 */
function facecheck_oauth_linkedin()
{
    global $facecheck_shortcode;
    if (isset($_GET['code'])) {
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?grant_type=authorization_code';
        $url .= '&code=' . $_GET['code'];
        $url .= '&redirect_uri=' . urlencode(get_permalink($facecheck_shortcode['login_scanface']) . '?action=facecheck_oauth_linkedin');
        $url .= '&client_id=75ffuo7grpudeh';
        $url .= '&client_secret=Q6wgsPax2BoCBEXB';

        $response = json_decode(file_get_contents($url), true);

        if (!isset($response['access_token'])) {
            wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
        }

        $token = $response['access_token'];
        $response = json_decode(file_get_contents('https://api.linkedin.com/v1/people/~:(first-name,last-name,email-address,date-of-birth)?format=json&oauth2_access_token=' . $token), true);

        if (!$response || isset($response['error']) || !$response['emailAddress']) {
            wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
        }

        $user = FacecheckUser::getByEmail($response['emailAddress']);

        if (!isset($user)) {
            $user = new FacecheckUser();
            $user->email = $response['emailAddress'];
            $user->name = $response['firstName'] . ($response['firstName'] ? ' ' . $response['lastName'] : $response['lastName']);
            if (isset($response['dateOfBirth'])) {
                $user->date_birth = $response['dateOfBirth']['year'] . '-' . $response['dateOfBirth']['month'] . '-' . $response['dateOfBirth']['day'];
            }
            $user->save();
        }

        $_SESSION['facecheck_user'] = $user;
        if (isset($_SESSION['redirect_result_id'])) {
            wp_redirect(add_query_arg('facecheck_photo_id', $_SESSION['redirect_result_id'], get_permalink($facecheck_shortcode['result_scanface'])));
        } else {
            wp_redirect(get_permalink($facecheck_shortcode['upload_scanface']));
        }
    } else {
        wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
    }
}

/**
 *Разлогирование пользователя
 */
function facecheck_user_logout()
{

    unset($_SESSION['facecheck_user']);
    wp_logout();
    wp_redirect(isset($_GET['redirect']) ? $_GET['redirect'] : '/');
    exit();
}

/**
 * Смена пароля
 * @return array
 */
function facecheck_user_password()
{
    $errors = array();

    if (empty($_POST['email'])) {
        $errors['email'] = 'Введите email';
    } elseif (!preg_match('/[0-9a-zA-Z][-0-9a-zA-Z.+_]+@[0-9a-zA-Z][-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/', $_POST['email'])) {
        $errors['email'] = 'Некорректный email';
    } elseif (!$user = FacecheckUser::getByEmail($_POST['email'])) {
        $errors['email'] = 'Пользователь с таким email не найден';
    }

    if ($errors) {
        return array(
            'errors' => $errors,
            'email' => $_POST['email']
        );
    } else {
        $newpass = substr(uniqid(), 0, 6);

//        $user->password = md5($newpass);
        $user->password = $newpass;
        $user->save();

        ob_start();
        include __DIR__ . '/templates/password-mail.php';
        $content = ob_get_clean();

        add_filter('wp_mail_content_type', 'facecheck_html_email_type');
        wp_mail($_POST['email'], 'Новый пароль', $content, 'From: "Scanface" <report@scanface.com.ua>');
        remove_filter('wp_mail_content_type', 'facecheck_html_email_type');

        $_SESSION['new_password'] = true;
//        wp_redirect($_POST['success_page']);
        global $facecheck_shortcode;
        wp_redirect(get_permalink($facecheck_shortcode['login_scanface']));
    }
}

/**
 * Сохранение профиля
 * @return array
 */
function facecheck_save_profile()
{
    $errors = array();

    $user = $_SESSION['facecheck_user'];

    if (empty($_POST['user_name'])) {
        $errors['name'] = 'Введите имя';
    }
    if (!empty($_POST['user_old_password'])) {
        if (!wp_check_password($_POST['user_old_password'], $user->password, $user->id)) {
            $errors['old_password'] = 'Неверный старый пароль';
        };
    }


    if (!empty($_POST['user_old_password']) && empty($errors['old_password']) && empty($_POST['user_new_password'])) {
        $errors['new_password'] = 'Введите новый пароль';
    }

    if (empty($errors)) {
        $_SESSION['facecheck_user']->name = $_POST['user_name'];
        $_SESSION['facecheck_user']->date_birth = $_POST['user_year'] . '-' . $_POST['user_month'] . '-' . $_POST['user_day'];

        if ($_POST['user_old_password']) {
            $_SESSION['facecheck_user']->password = $_POST['user_new_password'];
        }

        $_SESSION['facecheck_user']->save();
    } else {
        return $errors;
    }
}

/*
function facecheck_reset_markers() {
    if (empty($_POST['id'])) {
        exit;
    }

    $markers = Facecheck::getMarkers($_POST['id'], true);

    echo json_encode($markers);
    exit;
}
*/

/**
 *Сохранение маркеров
 */
function facecheck_save_markers()
{
    if (empty($_POST['id']) || empty($_POST['marker'])) { // || empty($_POST['date'])
        exit;
    }
    Facecheck::saveMarkers($_POST['id'], json_encode($_POST['marker']), date('Y-m-d h:i:s')); //$_POST['date']
}

/**
 *Сохранение Секции отчетов
 */
function facecheck_save_section()
{
    global $wpdb;

    if ($_POST['id']) {
        $wpdb->update($wpdb->prefix . 'facecheck_report_sections', array('name' => $_POST['name']), array('id' => $_POST['id']));
        $id = $_POST['id'];
    } else {
        $position = $wpdb->get_var("SELECT MAX(`position`) FROM `{$wpdb->prefix}facecheck_report_sections`") + 1;
        $wpdb->insert($wpdb->prefix . 'facecheck_report_sections', array('name' => $_POST['name'], 'position' => $position));
        $id = $wpdb->insert_id;
    }

    if (file_exists(WP_CONTENT_DIR . '/plugins/facecheck/sicons/temp.jpg')) {
        rename(WP_CONTENT_DIR . '/plugins/facecheck/sicons/temp.jpg', WP_CONTENT_DIR . '/plugins/facecheck/sicons/' . $id . '.jpg');
    }

    wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/sections.php');
}


/**
 *Сохранение Группы профессий
 */
function facecheck_save_profession_category()
{
    global $wpdb;

    if ($_POST['id']) {
        $wpdb->update($wpdb->prefix . 'facecheck_profession_categories', array('name' => $_POST['name'], 'parent' => isset($_POST['parent']) ? $_POST['parent'] : null), array('id' => $_POST['id']));
        $id = $_POST['id'];
    } else {
        $wpdb->insert($wpdb->prefix . 'facecheck_profession_categories', array('name' => $_POST['name'], 'parent' => isset($_POST['parent']) ? $_POST['parent'] : null));
        $id = $wpdb->insert_id;
    }
    if (file_exists(WP_CONTENT_DIR . '/plugins/facecheck/pcicons/temp.jpg')) {
        rename(WP_CONTENT_DIR . '/plugins/facecheck/pcicons/temp.jpg', WP_CONTENT_DIR . '/plugins/facecheck/pcicons/' . $id . '.jpg');
    }

    wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/profcats.php');
}


/**
 *Сохранение профессий
 */
function facecheck_save_profession()
{
    global $wpdb;

    if ($_POST['id']) {
        $wpdb->update($wpdb->prefix . 'facecheck_professions', array('name' => $_POST['name'], 'id_category' => $_POST['facecheck_category']), array('id' => $_POST['id']));
        $id = $_POST['id'];
    } else {
        $wpdb->insert($wpdb->prefix . 'facecheck_professions', array('name' => $_POST['name'], 'id_category' => $_POST['facecheck_category']));
        $id = $wpdb->insert_id;
    }

    foreach ($_POST['specification'] as $key => $spec) {
        $data = array(
            'id_profession' => $id,
            'id_specification' => $key,
            'left_value' => $spec['left'],
            'right_value' => $spec['right'],
            'description' => $spec['descr']
        );
        $wpdb->replace($wpdb->prefix . 'facecheck_professions_specifications', $data);
    }

    wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/professions.php');
}


/**
 *Сохранение Характеристики
 */
function facecheck_save_specification()
{
    global $wpdb;

    $data = array(
        'name' => $_POST['name'],
        'left_value' => $_POST['left-value'],
        'right_value' => $_POST['right-value'],
        'left_text' => $_POST['left-text'],
        'right_text' => $_POST['right-text'],
        'left_title' => $_POST['left-title'],
        'right_title' => $_POST['right-title']
    );

    if ($_POST['id']) {
        $wpdb->update($wpdb->prefix . 'facecheck_specifications', $data, array('id' => $_POST['id']));
    } else {
        $wpdb->insert($wpdb->prefix . 'facecheck_specifications', $data);
        $id = $wpdb->insert_id;
    }

    wp_redirect(admin_url() . 'admin.php?page=facecheck/admin/specifications.php');
}

/**
 * @return array
 */
function facecheck_save_custom_specifications()
{
    global $wpdb;

    if ($_SESSION['facecheck_user']) {
        $errors = array();
        if (empty($_POST['name_scanface'])) {
            $errors['name_scanface'] = 'Введите название профессии';
        } else {
            $exists = $wpdb->get_row("
                SELECT *
                FROM `{$wpdb->prefix}facecheck_professions`
                WHERE `id_user` = {$_SESSION['facecheck_user']->id} AND `name` = '{$_POST['name_scanface']}'
            ", ARRAY_A);

            if (!empty($exists)) {
                $errors['name_scanface'] = 'Вы уже создали профессию с таким названием';
            }
        }

        if (empty($errors)) {
            if (empty($_POST['id_profession'])) {
                $wpdb->insert($wpdb->prefix . 'facecheck_professions', array('name' => $_POST['name_scanface'], 'id_category' => $_POST['id_category'], 'id_user' => $_SESSION['facecheck_user']->id));
                $_POST['id_profession'] = $wpdb->insert_id;

                foreach ($_POST['specval'] as $id => $specval) {
                    $wpdb->insert(
                        $wpdb->prefix . 'facecheck_professions_specifications',
                        array(
                            'id_profession' => $_POST['id_profession'],
                            'id_specification' => $id,
                            'left_value' => $specval['left'],
                            'right_value' => $specval['right'],
                            'description' => ''
                        )
                    );
                }
            } else {
                $wpdb->update($wpdb->prefix . 'facecheck_professions', array('name' => $_POST['name_scanface']), array('id' => $_POST['id_profession']));

                foreach ($_POST['specval'] as $id => $specval) {
                    $wpdb->update(
                        $wpdb->prefix . 'facecheck_professions_specifications',
                        array(
                            'left_value' => $specval['left'],
                            'right_value' => $specval['right'],
                        ),
                        array(
                            'id_profession' => $_POST['id_profession'],
                            'id_specification' => $id
                        )
                    );
                }
            }
            global $facecheck_shortcode;
            echo '<script>location.href="'.get_permalink($facecheck_shortcode['professions_scanface']).'";</script>'; exit;
            wp_redirect(get_permalink($facecheck_shortcode['professions_scanface']));
        } else {
            return array(
                'errors' => $errors,
                'specval' => $_POST['specval']
            );
        }
    }

    exit;
}

//служебные функции
function facecheck_get_photo($id)
{
    if (empty($id)) {
        return null;
    }

    return Facecheck::getPhoto($id);
}

function facecheck_get_result($id)
{
    return Facecheck::getResult($id);
}

function facecheck_html_email_type()
{
    return 'text/html';
}


/*Шорткоды страниц*/


// фильтр передает переменную $template - путь до файла шаблона. Изменяя этот путь мы изменяем файл шаблона.
//add_filter('template_include', 'my_template');

// функция фильтрации
function my_template($template)
{


    # аналог второго способа
    // если это страница со слагом portfolio, используем файл шаблона page-portfolio.php
    // используем условный тег is_page()
    global $post;

    if (is_object($post)) {
        global $facecheck_shortcode;
        $val = array_flip($facecheck_shortcode);

        if (array_key_exists($post->ID, $val)) {
            if ($val[$post->ID] == 'login_scanface') {
                return __DIR__ . '/template/login.php';
            }
            if ($val[$post->ID] == 'registration_scanface') {
                return __DIR__ . '/template/registration.php';
            }
            if ($val[$post->ID] == 'history_scanface') {
                return __DIR__ . '/template/history.php';
            }
            if ($val[$post->ID] == 'upload_scanface') {
                return __DIR__ . '/template/upload.php';
            }
            if ($val[$post->ID] == 'markers_scanface') {
                return __DIR__ . '/template/markers.php';
            }
            if ($val[$post->ID] == 'result_scanface') {
                return __DIR__ . '/template/result.php';
            }
            if ($val[$post->ID] == 'professions_scanface') {
                return __DIR__ . '/template/professions.php';
            }
            if ($val[$post->ID] == 'profile_scanface') {
                return __DIR__ . '/template/profile.php';
            }
            if ($val[$post->ID] == 'specifications_scanface') {
                return __DIR__ . '/template/specifications.php';
            }
            if ($val[$post->ID] == 'new_password_scanface') {
                return __DIR__ . '/template/new_password.php';
            }
        }
    }

    return $template;

}

/*--------поиск страниц с шорткодами-------------*/

function find_shortcode_pages()
{
    global $wpdb;
    $pages = [
        "'%[login_scanface]%'",
        "'%[registration_scanface]%'",
        "'%[history_scanface]%'",
        "'%[upload_scanface]%'",
        "'%[markers_scanface]%'",
        "'%[result_scanface]%'",
        "'%[professions_scanface]%'",
        "'%[profile_scanface]%'",
        "'%[new_password_scanface]%'",
        "'%[specifications_scanface]%'"
    ];

    $sql = implode(" OR `post_content` LIKE ", $pages);

    $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}posts` WHERE `post_status` IN('publish','draft') AND ( `post_content` LIKE " . $sql . ")", ARRAY_A);


    global $facecheck_shortcode;
    $facecheck_shortcode = [];
    foreach ($rows as $row) {
        if (preg_match('/\[login_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['login_scanface'] = $row['ID'];
        } elseif (preg_match('/\[registration_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['registration_scanface'] = $row['ID'];
        } elseif (preg_match('/\[history_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['history_scanface'] = $row['ID'];
        } elseif (preg_match('/\[upload_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['upload_scanface'] = $row['ID'];
        } elseif (preg_match('/\[markers_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['markers_scanface'] = $row['ID'];
        } elseif (preg_match('/\[result_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['result_scanface'] = $row['ID'];
        } elseif (preg_match('/\[professions_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['professions_scanface'] = $row['ID'];
        } elseif (preg_match('/\[profile_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['profile_scanface'] = $row['ID'];
        } elseif (preg_match('/\[specifications_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['specifications_scanface'] = $row['ID'];
        } elseif (preg_match('/\[new_password_scanface\]/u', $row['post_content']) == 1) {
            $facecheck_shortcode['new_password_scanface'] = $row['ID'];
        }
    }
}


/*------ SHORTCODE INIT-------*/
function shortcode_init()
{


    wp_register_style("magnific-popup", plugin_dir_url(__FILE__) . 'assets/js/magnific-popup/magnific-popup.css');
    wp_enqueue_style('magnific-popup');
    wp_register_script('magnific-popup', plugin_dir_url(__FILE__) . 'assets/js/magnific-popup/jquery.magnific-popup.js', 'jquery', '1.0', TRUE);
    wp_enqueue_script('magnific-popup');

    add_shortcode('upload_scanface', 'upload_scanface');
    add_shortcode('markers_scanface', 'markers_scanface');
    add_shortcode('result_scanface', 'result_scanface');
    add_shortcode('profile_scanface', 'profile_scanface');
    add_shortcode('professions_scanface', 'professions_scanface');
    add_shortcode('specifications_scanface', 'specifications_scanface');
    add_shortcode('registration_scanface', 'registration_scanface');
    add_shortcode('login_scanface', 'login_scanface');
    add_shortcode('new_password_scanface', 'new_password_scanface');
    add_shortcode('capture_video', 'capture_video');
    add_shortcode('facecheck_logs', 'facecheck_logs');

    add_shortcode('history_scanface', 'history_scanface');
    add_shortcode('sf-report-candidate-profile','report_candidate_profile');
    add_shortcode('sf-report-restrict-message','report_restrict_message');
    add_shortcode('sf-report-candidate-characteristic','report_candidate_characteristic');
    add_shortcode('sf-report-profinfo','report_profinfo');
    add_shortcode('sf-report-restrict-info','report_restrict_info');
    
    add_shortcode('testing-history', 'testing_history');
    add_shortcode('history_scanface_widget', 'history_scanface_widget');
    add_shortcode('sf-title','sf_title');
    add_shortcode('sf-question','sf_question');
    
    add_shortcode('sfcanface-user-tests','sfcanface_user_tests');
    add_shortcode('scanface-user-days','scanface_user_days');
    add_shortcode('scanface-user-reports-number','scanface_user_reports_number');
}

function sfcanface_user_tests()
{
    global $wpdb;
    $id = ($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : false;
    if($id) {
        $count_test = $wpdb->get_row("SELECT sum(count_test) count_test FROM `wp_facecheck_order_user` WHERE `id_user`='$id' AND count_test > 0",ARRAY_A);
        if(count($count_test))
            return ($count_test['count_test']==null || $count_test['count_test']<0) ? 0 : $count_test['count_test'];
    }
    return 0;
}
function scanface_user_days()
{
    global $wpdb; 
    $id = ($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : false;
    if($id) {
        $count_days = $wpdb->get_row("SELECT max(date_exp) date_exp FROM `wp_facecheck_order_user` WHERE `id_user`='$id' AND date_exp > ".time(),ARRAY_A);
        if($count_days['date_exp']>0)
            return floor(($count_days['date_exp']+86400 - time())/86400);
    }
    return 0;
}
function scanface_user_reports_number()
{
    global $wpdb;
    $id = ($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : false;
    if($id) {
        $count_photos = $wpdb->get_row("SELECT count(*) coun FROM `wp_facecheck_photos` WHERE `user_id`='$id'",ARRAY_A);
        if(count($count_photos))
            return $count_photos['coun']==null ? 0 : $count_photos['coun'];
    }
    return 0;
}
/**
 *Шорт код чекбокса для активации логирования
 */
function facecheck_logs()
{
    if (current_user_can('administrator')) {
        echo <<<TAG
    <div class='admin - log'>
        <label for='admin_logs'>Вести лог тестирования</label>
        <input id='admin_logs' type='checkbox' name='logs' style='display: inline-block;vertical-align: middle;'>
    </div>
TAG;
    }
}

/**
 *шорткод для фото с вебкамеры
 */
function capture_video()
{
    wp_enqueue_script('jquery');
    wp_register_script('webcam', plugin_dir_url(__FILE__) . 'assets/js/webcam.min.js', 'jquery');
    wp_enqueue_script('webcam'); 
    wp_register_script('capture', plugin_dir_url(__FILE__) . 'assets/js/capture.js', 'jquery');
    wp_enqueue_script('capture');

    include __DIR__ . '/shortcode/capture_video.php';
}

/**
 *Страница загрузки Фото
 */
function upload_scanface()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-widget');
    wp_enqueue_script('uploaderscanface', plugin_dir_url(__FILE__) . 'assets/js/jquery.fileupload.js', array('jquery'), '1.0', true);
    wp_enqueue_script('uploadscanface', plugin_dir_url(__FILE__) . 'assets/js/upload.js', array('jquery', 'uploaderscanface'), '1.0', true);

    $output_string = '';
    include __DIR__ . '/shortcode/upload_scanface.php';
    return $output_string;
}

/**
 *Страница Маркеров
 */
function markers_scanface()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-mousehold', plugin_dir_url(__FILE__) . 'assets/js/jquery.ui.mousehold.js', array('jquery', 'jquery-ui-core', 'jquery-ui-widget', 'jquery-ui-mouse'), '1.10.3', true);
    wp_enqueue_script('jquery-ui-draggable');
    wp_enqueue_script('markers', plugin_dir_url(__FILE__) . 'assets/js/markers.js', array('jquery'), '1.0', true);

    $output_string = '';
    include __DIR__ . '/shortcode/markers_scanface.php';
    return $output_string;
}

/**
 *Страница результатов
 */
function result_scanface()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('markers', plugin_dir_url(__FILE__) . 'assets/js/report.js', array('jquery'), '1.0', true);

    $output_string = '';
    include __DIR__ . '/shortcode/result_scanface.php';
    return $output_string;
}

/**
 *Страница историй
 */
function history_scanface($params, $content = null)
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-multiselect', plugin_dir_url(__FILE__) . 'assets/js/jquery.multiselect.js', array('jquery', 'jquery-ui-core', 'jquery-ui-widget'), '1.0', true);
    wp_enqueue_script('filter.js', plugin_dir_url(__FILE__) . 'assets/js/filter.js', array('jquery'), '1.0', true);
    wp_enqueue_script('markers', plugin_dir_url(__FILE__) . 'assets/js/report.js', array('jquery'), '1.0', true);

    do_shortcode($content);
    $output_string = '';
    include __DIR__ . '/shortcode/history_scanface.php';
    return $output_string;
}
function report_candidate_profile($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['report_candidate_profile'] = $content;
    return '';
}
function report_restrict_message($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['report_restrict_message']['params'] = $params;
    $params_shortcode['report_restrict_message']['content'] = $content;
    return '';
}
function report_candidate_characteristic($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['report_candidate_characteristic'] = $content;
    return '';
}
function report_profinfo($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['report_profinfo'] = $content;
    return '';
}
function report_restrict_info($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['report_restrict_info'] = $content;
    return '';
}  
/**
 *Виджет историй
 */
function history_scanface_widget($params, $content = null)
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-multiselect', plugin_dir_url(__FILE__) . 'assets/js/jquery.multiselect.js', array('jquery', 'jquery-ui-core', 'jquery-ui-widget'), '1.0', true);
    wp_enqueue_script('filter.js', plugin_dir_url(__FILE__) . 'assets/js/filter.js', array('jquery'), '1.0', true);
    wp_enqueue_script('markers', plugin_dir_url(__FILE__) . 'assets/js/report.js', array('jquery'), '1.0', true);

    do_shortcode($content);
    $output_string = '';
    include __DIR__ . '/shortcode/history_scanface_widget.php';
    return $output_string;
}
function testing_history($params, $content = null)
{
    do_shortcode($content);
    $output_string = '';
    include __DIR__ . '/shortcode/history_scanface_widget.php';
    return $output_string;
}
function sf_title($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['title'] = $content;
    return '';
}
function sf_question($params, $content = null)
{
    global $params_shortcode;
    $params_shortcode['question'] = $content;
    return '';
}  
/**
 *Страница профиля
 */
function profile_scanface($params, $content = null)
{
    do_shortcode($content);
    $output_string = '';
    include __DIR__ . '/shortcode/profile_scanface.php';
    return $output_string;
}

/**
 *Страница профессий
 */
function professions_scanface($params, $content = null)
{
    do_shortcode($content);
    $output_string = '';
    include __DIR__ . '/shortcode/professions_scanface.php';
    return $output_string;
}

/**
 *Страница специальностей
 */
function specifications_scanface()
{
    wp_enqueue_script('jquery-ui-draggable');
    include __DIR__ . '/shortcode/specifications_scanface.php';
}

/**
 *Страница регистрации
 */
function registration_scanface()
{
    include __DIR__ . '/shortcode/registration_scanface.php';
}

/**
 *Страница входа
 */
function login_scanface()
{
    include __DIR__ . '/shortcode/login_scanface.php';
}


/**
 *Страница восстановления пароля
 */
function new_password_scanface()
{
    include __DIR__ . '/shortcode/new_password_scanface.php';
}

/*------ SHORTCODE INIT END -------*/

// Custom routes
function add_routes()
{
    $rules = get_option('rewrite_rules');

    if (empty($rules['facecheck_markers/([0-9]+)/?$'])) {
        add_rewrite_rule('facecheck_markers/([0-9]+)/?$', 'index.php?pagename=facecheck_markers&facecheck_photo_id=$matches[1]', 'top');
        flush_rewrite_rules();
    }
}

/*Секция интеграции с  WooCommerce*/

//NEW PRODUCT TYPE
function new_product_type()
{
    add_filter('product_type_selector', 'wdm_add_custom_product_type');
    add_action('plugins_loaded', 'wdm_create_custom_product_type');
    add_action('woocommerce_product_options_general_product_data', 'wdm_add_custom_settings');
    add_action('woocommerce_process_product_meta', 'wdm_save_custom_settings');
}

function wdm_add_custom_product_type($types)
{
    $types['wdm_custom_product'] = __('Тарифные планы');
    return $types;
}

function wdm_create_custom_product_type()
{
    // declare the product class
    class WC_Product_Wdm extends WC_Product
    {
        public function __construct($product)
        {
            $this->product_type = 'wdm_custom_product';
            parent::__construct($product);
            // add additional functions here
        }
    }
}

function wdm_add_custom_settings()
{
    global $woocommerce, $post;
    echo '<div class="options_group">';

    $levels = Facecheck::getAccessLevel();
    $options = [];
    foreach ($levels as $id => $value) {
        $options[$id] = $value['name'];
    }
    woocommerce_wp_select(
        array(
            'id' => 'wdm_access_level',
            'label' => __('Выбирите уровень доступа'),
            'placeholder' => '',
            'desc_tip' => 'true',
//            'description' => __('Введите цифровое значение', 'woocommerce'),
            'options' => $options
        ));

    woocommerce_wp_text_input(
        array(
            'id' => 'wdm_upc_field',
            'label' => __('Кол-во дней действия плана', 'woocommerce'),
            'placeholder' => '',
            'desc_tip' => 'true',
            'description' => __('Введите цифровое значение', 'woocommerce'),
            'type' => 'number',
            'custom_attributes' => array(
                'step' => '0.01'
            )

        ));

    woocommerce_wp_text_input(
        array(
            'id' => 'wdm_date_exp',
            'label' => __('До какой даты действует план', 'woocommerce'),
            'placeholder' => '',
            'desc_tip' => 'true',
            'description' => __('Введите дату', 'woocommerce'),
            'type' => 'date'
        ));

    woocommerce_wp_text_input(
        array(
//            'id' => 'wdm_price_field',_regular_price
            'id' => '_regular_price',
            'label' => __('Цена', 'woocommerce'),
            'placeholder' => '',
            'desc_tip' => 'true',
            'description' => __('Введите цену плана', 'woocommerce'),
            'type' => 'number'
        ));
    woocommerce_wp_text_input(
        array(
            'id' => '_sale_price',
            'label' => __('Акционная цена', 'woocommerce'),
            'placeholder' => '',
            'desc_tip' => 'true',
            'description' => __('Введите акционную цену плана', 'woocommerce'),
            'type' => 'number'
        ));
    woocommerce_wp_text_input(
        array(
            'id' => 'wdm_maxuse_field',
            'label' => __('Сколько тестов доступно в плане', 'woocommerce'),
            'placeholder' => '',
            'desc_tip' => 'true',
            'description' => __('Введите цифровое значение', 'woocommerce'),
            'type' => 'number'
        ));

    woocommerce_wp_checkbox(
        array(
            'id' => 'wdm_is_purchasable',
            'label' => __('Unlimited пакет?', 'woocommerce')
        ));

    echo '</div>';
}

function wdm_save_custom_settings($post_id)
{
// save field
    $wdm_access_level = $_POST['wdm_access_level'];
    if (!empty($wdm_access_level))
        update_post_meta($post_id, 'wdm_access_level', esc_attr($wdm_access_level));

    $wdm_product_upc = $_POST['wdm_upc_field'];
    if (!empty($wdm_product_upc)) {
        update_post_meta($post_id, 'wdm_upc_field', esc_attr($wdm_product_upc));
    } else {
        update_post_meta($post_id, 'wdm_upc_field', "");
    }
    $wdm_date_exp = $_POST['wdm_date_exp'];
    if (!empty($wdm_date_exp)) {
        update_post_meta($post_id, 'wdm_date_exp', esc_attr($wdm_date_exp));
    } else {
        update_post_meta($post_id, 'wdm_date_exp', "");
    }


    /*  $wdm_product_upc = $_POST['wdm_price_field'];
      if (!empty($wdm_product_upc))
          update_post_meta($post_id, 'wdm_price_field', esc_attr($wdm_product_upc));*/
    $wdm_product_upc = $_POST['_regular_price'];
    if (!empty($wdm_product_upc))
        update_post_meta($post_id, '_regular_price', esc_attr($wdm_product_upc));

    $wdm_product_upc = $_POST['_sale_price'];
    if (!empty($wdm_product_upc))
        update_post_meta($post_id, '_sale_price', esc_attr($wdm_product_upc));

    $wdm_product_upc = $_POST['wdm_maxuse_field'];
//    if (!empty($wdm_product_upc))
    update_post_meta($post_id, 'wdm_maxuse_field', esc_attr($wdm_product_upc));

// save checkbox option
    $wdm_purchasable = isset($_POST['wdm_is_purchasable']) ? 'yes' : 'no';
    update_post_meta($post_id, 'wdm_is_purchasable', $wdm_purchasable);
    if ($wdm_purchasable == "yes") {
        update_post_meta($post_id, 'wdm_maxuse_field', null);
        update_post_meta($post_id, 'wdm_date_exp', "");
        update_post_meta($post_id, 'wdm_upc_field', null);
    }
}

/**
 * Собитие когда заказ выполнен
 * @param $order_id
 */
function mysite_woocommerce_order_status_completed($order_id)
{
    Facecheck::saveUserOrder($order_id);//сохранение деталей заказа для последующей обработки
}

add_action('woocommerce_order_status_completed',
    'mysite_woocommerce_order_status_completed');

add_action('wp_print_styles', 'print_style');

function print_style()
{
//    echo "111----111";
    wp_register_style("style-scanface", plugin_dir_url(__FILE__) . 'assets/css/style.css');
    wp_enqueue_style('style-scanface');
    wp_register_style("responsive-scanface", plugin_dir_url(__FILE__) . 'assets/css/responsive.css');
//    wp_enqueue_style('responsive-scanface');
    wp_register_style("rgs-scanface", plugin_dir_url(__FILE__) . 'assets/css/rgs.css');
    wp_enqueue_style('rgs-scanface');
    wp_register_style("font-avesome-scanface", plugin_dir_url(__FILE__) . 'assets/css/font-awesome.min.css');
    wp_enqueue_style('font-avesome-scanface');
}

add_filter('wp_print_styles', 'print_style_f');
function print_style_f($style)
{
//    $arg_list = func_get_args();
//    var_dump($arg_list);
//    return $style;
}

if (strpos($_SERVER['REQUEST_URI'], 'profession-library') !== false) {
    wp_enqueue_script('profession-library', plugin_dir_url(__FILE__) . 'assets/js/profession-library.js', array('jquery'), '1.0', true);
}

/* функция генерации абстрактного хеша */
function setRandomSessid() {
    $_SESSION['ses_id'] = md5(date('d.m.Y H:i:s').rand(1, 1000000));
}
wp_enqueue_script( 'password-strength-meter' );





