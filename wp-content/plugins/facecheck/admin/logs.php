<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 30.05.2016
 * Time: 11:56
 */



/** 
 * @var array $logs
 * Получение списка логов
 */
$logs = Facecheck::getLogs();
if (isset($_GET['id_log'])) {

/**
 * @var string $log 
 * Получение лога по Id   
 */
$log = Facecheck::getLogs($_GET['id_log']);
$markers = json_decode($log['markers']);

$mMiddleLine = ($markers->ml->y + $markers->mt->y + $markers->mb->y + $markers->mr->y) / 4; //средняя линия рта
?>
<div class="wrap">
    <h2 style="text-align: center;">Детали тестирования</h2>
    <h3 style="text-align: center;">Маркеры</h3>

    <fieldset>
        <legend><b>Брови</b></legend>

        <table border cellspacing="0" width="100%">
            <thead>
            <tr>
                <th width="200px">Описание точки</th>
                <th colspan="2">Левая бровь</th>
                <th colspan="2">Правая бровь</th>
            </tr>
            <tr>
                <th></th>
                <th>X</th>
                <th>Y</th>
                <th>X</th>
                <th>Y</th>
            </tr>
            </thead>
            <tr>
                <td>наружная (<b>blo-bro</b>)</td>
                <td> <?= $markers->blo->x ?> </td>
                <td> <?= $markers->blo->y ?> </td>
                <td> <?= $markers->bro->x ?> </td>
                <td> <?= $markers->bro->y ?> </td>
            </tr>
            <tr>
                <td>средняя (<b>blm-brm</b>)</td>
                <td> <?= $markers->blm->x ?> </td>
                <td> <?= $markers->blm->y ?> </td>
                <td> <?= $markers->brm->x ?> </td>
                <td> <?= $markers->brm->y ?> </td>
            </tr>
            <tr>
                <td>внутренняя (<b>bli-bri</b>)</td>
                <td> <?= $markers->bli->x ?> </td>
                <td> <?= $markers->bli->y ?> </td>
                <td> <?= $markers->bri->x ?> </td>
                <td> <?= $markers->bri->y ?> </td>
            </tr>
        </table>

        <p>Формула подсчета для бровей:</p>
        <p>(blo:y + blm:y + bli:y + bro:y + brm:y + bri:y) / 6; средняя линия бровей <br>
            <?= "(" . $markers->blo->y . "+" . $markers->blm->y . "+" . $markers->bli->y . "+" . $markers->bro->y . "+" . $markers->brm->y . "+" . $markers->bri->y . ")/6 = " . ($bMiddleLine = ($markers->blo->y + $markers->blm->y + $markers->bli->y + $markers->bro->y + $markers->brm->y + $markers->bri->y) / 6) ?>
        </p>
        <p>Тип бровей:</p>
        <p>
            Если угол Y*blm*brm = 90 ᵒ ±5ᵒ и угол (bro*brm*bri + blo*blm*bli)/ 2 = 180ᵒ±20ᵒ, то это Тип 1<br>
            Y*blm*brm = rad2deg(acos(((blm:x - brm:x)*(brm:x - brm:x)+(blm:y - brm:y)*((brm:y+10) - brm:y))/(sqrt((blm:x - brm:x)*(blm:x - brm:x)+(blm:y - brm:y)*(blm:y - brm:y))*sqrt((brm:x - brm:x)*(brm:x - brm:x)+((brm:y+10) - brm:y)*((brm:y+10) - brm:y)))))<br>
            <?php $ybx=Facecheck::ygol($markers->blm->x,$markers->blm->y,$markers->brm->x,$markers->brm->y,$markers->brm->x,$markers->brm->y+10)?>
            Y*blm*brm = rad2deg(acos(((<?=$markers->blm->x?> - <?=$markers->brm->x?>)*(<?=$markers->brm->x?> - <?=$markers->brm->x?>)+(<?=$markers->blm->y?> - <?=$markers->brm->y?>)*((<?=$markers->brm->y?>+10) - <?=$markers->brm->y?>))/(sqrt((<?=$markers->blm->x?> - <?=$markers->brm->x?>)*(<?=$markers->blm->x?> - <?=$markers->brm->x?>)+(<?=$markers->blm->y?> - <?=$markers->brm->y?>)*(<?=$markers->blm->y?> - <?=$markers->brm->y?>))*sqrt((<?=$markers->brm->x?> - <?=$markers->brm->x?>)*(<?=$markers->brm->x?> - <?=$markers->brm->x?>)+((<?=$markers->brm->y?>+10) - <?=$markers->brm->y?>)*((<?=$markers->brm->y?>+10) - <?=$markers->brm->y?>))))) = <?= $ybx?>ᵒ<br>
            bro*brm*bri = <?= $dej = Facecheck::ygol($markers->blo->x,$markers->blo->y,$markers->blm->x,$markers->blm->y,$markers->bli->x,$markers->bli->y);?><br>
            blo*blm*bli = <?= $abg = Facecheck::ygol($markers->bro->x,$markers->bro->y,$markers->brm->x,$markers->brm->y,$markers->bri->x,$markers->bri->y);?><br>
            <?= "(" . $ybx . " = 90 ᵒ ±".get_option('facecheck_constant_yogl')."ᵒ) && (".$dej." + ".$abg.")/ 2 = 180ᵒ±".get_option('facecheck_constant_yrkb')."ᵒ)" . (($ybx >= 90-get_option('facecheck_constant_yogl') && $ybx <= 90+get_option('facecheck_constant_yogl')) && (($abg+$dej)/2 >= 180-get_option('facecheck_constant_yrkb') && ($abg+$dej)/2 <= 180+get_option('facecheck_constant_yrkb')) ? " /true <b>Тип 1</b>" : " /false") ?>
        </p>
        <p>
            Если угол Y*blm*brm = 90 ᵒ ±5ᵒ и угол (bro*brm*bri + blo*blm*bli)/ 2 <180ᵒ-20ᵒ, то это Тип 2 <br>
            <?= "(" . $ybx . " = 90 ᵒ ±".get_option('facecheck_constant_yogl')."ᵒ) && (".$dej." + ".$abg.")/ 2 < 180ᵒ-".get_option('facecheck_constant_yrkb')."ᵒ)" . (($ybx >= 90-get_option('facecheck_constant_yogl') && $ybx <= 90+get_option('facecheck_constant_yogl')) && (($abg+$dej)/2 < 180-get_option('facecheck_constant_yrkb')) ? " /true <b>Тип 2</b>" : " /false") ?>
        </p>
        <p>
            Угол Y*blm*brm<85ᵒ // Тип 3 <br>
            <?= "(" . $ybx . " < 90 ᵒ -".get_option('facecheck_constant_yogl')."ᵒ)" . (($ybx < 90-get_option('facecheck_constant_yogl')) ? " /true <b>Тип 3</b>" : " /false") ?>
        </p>
        <p>
            Угол Y*blm*brm>95ᵒ // Тип 4 <br>
            <?= "(" . $ybx . " > 90 ᵒ +".get_option('facecheck_constant_yogl')."ᵒ)" . (($ybx > 90+get_option('facecheck_constant_yogl')) ? " /true <b>Тип 4</b>" : " /false") ?>
        </p>

    </fieldset>

    <fieldset>
        <legend><b>Глаза</b></legend>

        <table border cellspacing="0" width="100%">
            <thead>
            <tr>
                <th width="200px">Описание точки</th>
                <th colspan="2">Левый глаз</th>
                <th colspan="2">Правый глаз</th>
            </tr>
            <tr>
                <th></th>
                <th>X</th>
                <th>Y</th>
                <th>X</th>
                <th>Y</th>
            </tr>
            </thead>
            <tr>
                <td>наружная (<b>ylo-yro</b>)</td>
                <td> <?= $markers->ylo->x ?> </td>
                <td> <?= $markers->ylo->y ?> </td>
                <td> <?= $markers->yro->x ?> </td>
                <td> <?= $markers->yro->y ?> </td>
            </tr>
            <tr>
                <td>верхняя (<b>ylt-yrt</b>)</td>
                <td> <?= $markers->ylt->x ?> </td>
                <td> <?= $markers->ylt->y ?> </td>
                <td> <?= $markers->yrt->x ?> </td>
                <td> <?= $markers->yrt->y ?> </td>
            </tr>
            <tr>
                <td>нижняя (<b>ylb-yrb</b>)</td>
                <td> <?= $markers->ylb->x ?> </td>
                <td> <?= $markers->ylb->y ?> </td>
                <td> <?= $markers->yrb->x ?> </td>
                <td> <?= $markers->yrb->y ?> </td>
            </tr>
            <tr>
                <td>внутренняя (<b>yli-yri</b>)</td>
                <td> <?= $markers->yli->x ?> </td>
                <td> <?= $markers->yli->y ?> </td>
                <td> <?= $markers->yri->x ?> </td>
                <td> <?= $markers->yri->y ?> </td>
            </tr>
        </table>

        <p>Формула подсчета для глаз:</p>
        <p>
            ylb:y - ylt:y высота левого
            глаза <?= $markers->ylb->y . " - " . $markers->ylt->y . " = " . ($yLHeight = $markers->ylb->y - $markers->ylt->y) ?>
            <br>
            yrb:y - yrt:y высота правого
            глаза <?= $markers->yrb->y . " - " . $markers->yrt->y . " = " . ($yRHeight = $markers->yrb->y - $markers->yrt->y) ?>
            <br>
            (<?= $yLHeight . " + " . $yRHeight . ") / 2 = " . ($yHeight = ($yLHeight + $yRHeight) / 2) ?>// средняя
            высота глаза
            <br>
            <b>расстояние от средней линии рта до средней линии бровей:</b><br>
            средняя линия рта - средняя линия
            бровей <?= $mMiddleLine . " - " . $bMiddleLine . " = " . ($faceHeight = ($mMiddleLine - $bMiddleLine)) ?>
        </p>
        <p><b>Тип глаз:</b></p>
        <p>если разница в размере глаз больше "eyetoeye"(<?= get_option('facecheck_constant_eyetoeye') ?>) процентов
            среднего глаза
            <br>
            (высота левого глаза - высота правого глаза) > средняя высота глаза
            * <?= get_option('facecheck_constant_eyetoeye') ?> / 100
            <br>
            &nbsp;&nbsp;&nbsp;
            (<?= $yLHeight . " - " . $yRHeight . ") > " . $yHeight . " * " . get_option('facecheck_constant_eyetoeye') . " / 100" . ((abs($yLHeight - $yRHeight) > $yHeight * get_option('facecheck_constant_eyetoeye') / 100) ? " <b> true</b>" : " false") ?>
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; высота левого глаза > высота правого
            глаза <?= $yLHeight . " > " . $yRHeight ?>
            <?php if (abs($yLHeight - $yRHeight) > $yHeight * get_option('facecheck_constant_eyetoeye') / 100) {
                if ($yLHeight > $yRHeight) {
                    echo "true <b> Tип 4</b> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;высота левого глаза < высота правого глаза ({$yLHeight}  <  {$yRHeight}) false  // Tип 3";
                } else {
                    echo "false // Тип 4 <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;высота левого глаза < высота правого глаза ({$yLHeight}  <  {$yRHeight}) true <b> Tип 3</b>";
                }
            } ?><br>
            если разница в размере глаз меньше "eyetoeye"(<?= get_option('facecheck_constant_eyetoeye') ?>) процентов
            среднего глаза
            <br>
            (высота левого глаза - высота правого глаза) < средняя высота глаза
            * <?= get_option('facecheck_constant_eyetoeye') ?> / 100
            <br>
            &nbsp;&nbsp;&nbsp;и если (средняя высота глаза > расстояние от средней линии рта до средней линии бровей
            * <?= get_option('facecheck_constant_eyetoeye') ?> / 100
            (<?= $yHeight . " > " . $faceHeight . " * " . get_option('facecheck_constant_eyestoface') . "/ 100" ?>
            <?php if (!(abs($yLHeight - $yRHeight) > $yHeight * get_option('facecheck_constant_eyetoeye') / 100)) {
                if ($yHeight > $faceHeight * get_option('facecheck_constant_eyestoface') / 100) {
                    echo " true <b> Тип 2</b>";
                } else {
                    echo "false // Тип2 <br>&nbsp;&nbsp;&nbsp; иначе <b> Тип 1</b>";
                }

            } else {
                echo "false Тип 2<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; иначе Тип 1";
            } ?>

        </p>
    </fieldset>

    <fieldset>
        <legend><b>Рот</b></legend>

        <table border cellspacing="0" width="100%">
            <thead>
            <tr>
                <th width="200px">Описание точки</th>
                <th>X</th>
                <th>Y</th>
            </tr>
            </thead>
            <tr>
                <td>левая (<b>ml</b>)</td>
                <td> <?= $markers->ml->x ?> </td>
                <td> <?= $markers->ml->y ?> </td>
            </tr>
            <tr>
                <td>верхняя (<b>mt</b>)</td>
                <td> <?= $markers->mt->x ?> </td>
                <td> <?= $markers->mt->y ?> </td>
            </tr>
            <tr>
                <td>нижняя (<b>mb</b>)</td>
                <td> <?= $markers->mb->x ?> </td>
                <td> <?= $markers->mb->y ?> </td>
            </tr>
            <tr>
                <td>правая (<b>mr</b>)</td>
                <td> <?= $markers->mr->x ?> </td>
                <td> <?= $markers->mr->y ?> </td>
            </tr>
        </table>
        <p>
            Формула подсчета для рта:
        </p>
        (ml:y + mt:y + mb:y + mr:y) / 4
        <?= "(" . $markers->ml->y . " + " . $markers->mt->y . " + " . $markers->mb->y . " + " . $markers->mr->y . ") / 4 = " . $mMiddleLine ?>
        средняя линия рта
        <br>
        abs(ml:y - mr:y) (<?= $markers->ml->y . "-" . $markers->mr->y ?>)
        = <?= ($mLRHeight = abs($markers->ml->y - $markers->mr->y)) ?>// расстояние между крайними точками рта
        <br>
        abs(mb:y - mt:y) (<?= $markers->mb->y . "-" . $markers->mt->y ?>)
        = <?= ($mBTHeight = $markers->mb->y - $markers->mt->y) ?>// расстояние между средними точками рта (высота рта)
        <br>
        если расстояние между крайними точками рта меньше 10% от высоты рта <br>
        (расстояние между крайними точками рта < расстояние между средними точками рта (высота
        рта)* <?= get_option('facecheck_constant_mlrtombt') ?>)
        (<?= $mLRHeight . " < " . $mBTHeight . " * " . get_option('facecheck_constant_mlrtombt') ?>)
        <?php if ($mLRHeight < $mBTHeight * get_option('facecheck_constant_mlrtombt')) {
            echo " true <br> &nbsp;&nbsp;&nbsp;и если (средняя линия бровей - mt:y >= mb:y - средняя линия бровей) (" . $mMiddleLine . " - " . $markers->mt->y . " >= " . $markers->mb->y . " - " . $mMiddleLine . ")";
            if (($mMiddleLine - $markers->mt->y) >= ($markers->mb->y - $mMiddleLine)) {
                echo " true <b> Тип 1</b>";
            } else {
                echo " false // Тип1 <br>  &nbsp;&nbsp;&nbsp; иначе <b> Тип 2</b>";
            }
        } ?>
        <br>
        И если (расстояние между крайними точками рта > расстояние между средними точками рта (высота
        рта)* <?= get_option('facecheck_constant_mlrtombt') ?>)
        (<?= $mLRHeight . " >" . $mBTHeight . " * " . get_option('facecheck_constant_mlrtombt') ?>)
        <?php if ($mLRHeight > $mBTHeight * get_option('facecheck_constant_mlrtombt')) {
            echo " true <br> &nbsp;&nbsp;&nbsp;и если ml:y > mr:y (" . $markers->ml->y . " > " . $markers->mr->y . ")";
            if ($markers->ml->y > $markers->mr->y) {
                echo " true <b> Тип 3</b>";
            } else {
                echo "false <br> <b> Тип4</b>";
            }

        } else {
            echo " false <br>&nbsp;&nbsp;&nbsp; ml:y > mr:y // тип 3 <br> &nbsp;&nbsp;&nbsp; ml:y < mr:y // тип 4";
        } ?>
    </fieldset>

    <?php } else {
    if (!empty($_POST['submit'])) {
        switch ($_POST['action']) {
            case 'delete' :
                global $wpdb;
                Facecheck::deletePhoto($_POST['log_id']);
//                $wpdb->delete($wpdb->prefix . 'facecheck_photos', array('id' => $_POST['log_id']));
                echo "<script> location.reload(true);</script>";
//                $wpdb->delete($wpdb->prefix . 'facecheck_access_to_spec', array('id_access' => $_POST['access']));
//                $wpdb->delete($wpdb->prefix . 'facecheck_access_level', array('id' => $_POST['access']));
//                $section = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections` WHERE `id`='{$_POST['section']}'", ARRAY_A);
//                $wpdb->query("UPDATE `{$wpdb->prefix}facecheck_report_sections` SET `position`=`position`-1 WHERE `position`>'{$section['position']}'");
//                $wpdb->query("DELETE FROM `{$wpdb->prefix}facecheck_report_sections` WHERE `id`='{$section['id']}'");
//                $wpdb->query("DELETE FROM `{$wpdb->prefix}facecheck_reports` WHERE `section_id`='{$section['id']}'");
                break;
        }
    }
    ?>
    <div class="wrap">
        <h2>Логи тестирований</h2>
        <table class="widefat">
            <thead>
            <th width="100px">id</th>
            <th width="100px">Дата</th>
            <th width="100px">Кто проводил тестирование</th>
            <th width="100px"></th>
            <th width="100px" colspan="2"></th>
            </thead>
            <tfoot>
            <th>id</th>
            <th>Дата</th>
            <th>Кто проводил тестирование</th>
            <th colspan="2"></th>
            </tfoot>
            <tbody>

            <?php $alternate = true; ?>
            <?php foreach ($logs as $log) { ?>
                <tr class="<?= ($alternate ? 'alternate' : '') ?>">
                    <?php $alternate = !$alternate ?>
                    <td>
                        <span style="display: block; margin: 15px 0 0 ;">№<?= $log['id'] ?></span>
                    </td>
                    <td>
                        <span style="display: block; margin: 15px 0 0 ;"><?= $log['date'] ?></span>
                    </td>
                    <td style="padding-top: 15px; text-align: center;"><?= get_userdata($log['user_id'])->nickname ?></td>
                    <td style="padding-top: 15px;">
                        <a href="/wp-admin/admin.php?page=facecheck/admin/logs.php&id_log=<?= $log['id'] ?>"
                           class="edit">Просмотреть</a>
                    </td>
                    <td style="padding-top: 15px;">
                        <a href="#" class="delete" data-log-id="<?= $log['id'] ?>">Удалить</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(function () {
                jQuery('a.delete').click(function (e) {
                    e.preventDefault();
                    jQuery('#facecheck-log-action').val(jQuery(this).attr('class'));
                    jQuery('#facecheck-log-id').val(jQuery(this).data('log-id'));
                    jQuery('#facecheck-log-submit').click();
                });
            });
        </script>
        <form id="facecheck-sections" action="/wp-admin/admin.php?page=facecheck/admin/logs.php"
              method="post">
            <input type="hidden" id="facecheck-log-action" name="action" value=""/>
            <input type="hidden" id="facecheck-log-id" name="log_id" value=""/>
            <input type="submit" id="facecheck-log-submit" name="submit" value="1" style="display:none;"/>
        </form>
        <?php } ?>
    </div>

