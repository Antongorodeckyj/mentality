<div class="wrap">
    <h2>
        Группы профессий
        <a href="/wp-admin/admin.php?page=facecheck/admin/profcats.php&new" class="add-new-h2">Добавить новую</a>
    </h2>

    <?php 
    if (isset($_GET['facecheck_category']) || isset($_GET['new'])) {
        if (isset($_GET['facecheck_category'])) {
            $category = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_profession_categories` WHERE `id`='{$_GET['facecheck_category']}'", ARRAY_A);
        }
        $categories = Facecheck::getProfessionCategories();
        ?>

        <div class="wrap">
            <form id="facecheck-profession-category"
                  action="/wp-admin/admin-post.php?action=facecheck_save_profession_category" method="post">
                <input type="hidden" name="id" value="<?= (isset($category) ? $category['id'] : '') ?>"/>
                <label for="category-name">Название:</label>
                <input type="text" id="name" name="name" value="<?= (isset($category) ? $category['name'] : '') ?>"
                       placeholder="Введите название"/><br/><br/>
                <label for="parent">Родительская категория:</label>
                <select name="parent" id="parent">
                    <option>выберите родительскую группу</option>
                    <?php
                    foreach ($categories as $cat) {
                        ?>
                        <option
                            value="<?= $cat['id'] ?>" <?= (isset($category) && !empty($category['parent']) && $category['parent'] == $cat['id']) ? "selected" : "" ?>><?= $cat['name'] ?></option>
                    <?php }
                    ?>
                </select>

                <label for="category-icon">Иконка</label>
                <img id="category-icon" src="<?= ((isset($category) && file_exists(WP_CONTENT_DIR . '/plugins/facecheck/pcicons/' . $category['id'] . '.jpg')) ? plugins_url('facecheck/pcicons/') . $category['id'] . '.jpg' : plugins_url('facecheck/pcicons/') . 'upload.jpg') ?>" />

                <input type="submit" id="category-save" name="submit" class="button button-primary button-large"
                       value="Сохранить"/>
            </form>
            <script type="text/javascript">
                jQuery(function () {
                    var $input = jQuery('<input type="file" name="pcicon" accept="image/*" />');

                    $input.fileupload({
                        url: '/wp-admin/admin-ajax.php?action=facecheck_upload_pcicon',
                        dataType: 'text',
                        dropZone: jQuery('#category-icon'),
                        fileInput: $input,
                        limitMultiFileUploads: 1,
                        done: function (e, data) {
                            if (data.result) {
                                jQuery('#category-icon').attr('src', data.result);
                            } else {
                                alert('upload error');
                            }
                        }
                    });

                    jQuery('#category-icon').click(function () {
                        if (!$input.get(0).files || !$input.get(0).files.length) {
                            $input.click();
                        }
                    });
                })
            </script>
        </div>
    <?php
    } else {
    if (isset($_POST['action']) && $_POST['action'] == 'delete') {
        $category = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_profession_categories` WHERE `id`='{$_POST['facecheck_category']}'", ARRAY_A);
        $wpdb->delete($wpdb->prefix.'facecheck_professions', array('id_category' => $category['id']));
        $wpdb->delete($wpdb->prefix.'facecheck_profession_categories', array('id' => $category['id']));
    }
    $sort = "ASC";
    $sort_name = "ASC";
    $method_sort = false;
    if (isset($_GET['sort_pos'])) {
        $method_sort = 'sort_pos';
        $sort = $_GET['sort_pos'];
    }
    if (isset($_GET['sort_name'])) {
        $method_sort = 'sort_name';
        $sort = $_GET['sort_name'];
    }

    $categories = Facecheck::getProfessionCategories($method_sort, $sort);
    ?>

        <table class="widefat">
            <thead>
            <th>
                <a href="/wp-admin/admin.php?page=facecheck/admin/profcats.php&sort_pos=<?= $sort =='ASC'?'DESC':'ASC' ?>">№</a>
            </th>
            <th width="500px"><a href="/wp-admin/admin.php?page=facecheck/admin/profcats.php&sort_name=<?= $sort =='ASC'?'DESC':'ASC' ?>">Группа</a></th>
            <th width="500px">Родительская группа</th>
            <th width="100px" colspan="2">Действия</th>
            </thead>
            <tfoot>
            <th>Группа</th>
            <th colspan="2">Действия</th>
            </tfoot>
            <tbody>
            <?php $alternate = true; ?>
            <?php
            foreach ($categories as $category) { ?>
                <tr class="<?= ($alternate ? 'alternate' : '') ?>">
                    <?php $alternate = !$alternate ?>
                    <td><?= $category['id'] ?></td>
                    <td>
                        <!--<img src="<? /*(file_exists(WP_CONTENT_DIR . '/plugins/facecheck/pccons/' . $section['id'] . '.jpg') ? plugins_url('facecheck/pccons/' . $section['id'] . '.jpg') : '') */ ?>" width="40px" height="40px" style="float:left;"/>-->
                        <img src="<?=(file_exists(WP_CONTENT_DIR . '/plugins/facecheck/pcicons/' . $category['id'] . '.jpg') ? plugins_url('facecheck/pcicons/' . $category['id'] . '.jpg') : '')?>" width="40px" height="40px" style="float:left;"/>
                        <span style="display: block; margin: 15px 0 0 50px;"><?= $category['name'] ?></span>
                    </td>
                    <td>
                        <!--<img src="<? /*(file_exists(WP_CONTENT_DIR . '/plugins/facecheck/pccons/' . $section['id'] . '.jpg') ? plugins_url('facecheck/pccons/' . $section['id'] . '.jpg') : '') */ ?>" width="40px" height="40px" style="float:left;"/>-->
                        <span
                            style="display: block; margin: 15px 0 0 50px;"><?= !empty($category['parent']) ? $categories[$category['parent']]['name'] : "" ?></span>
                    </td>
                    <td style="padding-top: 15px;">
                        <a href="/wp-admin/admin.php?page=facecheck/admin/profcats.php&facecheck_category=<?= $category['id'] ?>"
                           class="edit">Изменить</a>
                    </td>
                    <td style="padding-top: 15px;">
                        <a href="#" class="delete" data-category-id="<?= $category['id'] ?>" onclick="if (confirm('Вы действительно хотите удалить эту категорию?')) {jQuery('#facecheck-category-action').val('delete');jQuery('#facecheck-category-id').val('<?=$category['id']?>');jQuery('#facecheck-sections').submit();} return false;">Удалить</a>
                    </td>
                </tr>
                <?php
            } ?>
            </tbody>
        </table>
        <script type="text/javascript">
          /*  jQuery(function () {
                jQuery('a.delete, a.moveup, a.movedown').click(function (e) {
                    e.preventDefault();
                    jQuery('#facecheck-category-action').val(jQuery(this).attr('class'));
                    jQuery('#facecheck-category-id').val(jQuery(this).data('section-id'));
                    jQuery('#facecheck-category-submit').click();
                });
            });*/
        </script>
        <form id="facecheck-sections" action="/wp-admin/admin.php?page=facecheck/admin/profcats.php" method="post">
            <input type="hidden" id="facecheck-category-action" name="action" value=""/>
            <input type="hidden" id="facecheck-category-id" name="facecheck_category" value=""/>
        </form>
    <?php } ?>
</div>

