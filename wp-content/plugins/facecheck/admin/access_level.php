<div class="wrap">
    <h2>
        Уровни доступа
        <a href="/wp-admin/admin.php?page=facecheck/admin/access_level.php&new" class="add-new-h2">Добавить уровень
            доступа</a>
    </h2>

    <?php
    if (isset($_GET['access']) || isset($_GET['new'])) {
    if (isset($_GET['access'])) {
        $access = Facecheck::getAccessLevel($_GET['access']);
        $acces_to_sec = Facecheck::getAccessSection($_GET['access']);
        $acces_to_spec = Facecheck::getAccessSpec($_GET['access']);
    }
    $sections = Facecheck::getSections();
    $specifications = Facecheck::getSpecification();

    ?>

    <div class="wrap">
        <form id="facecheck-access" action="/wp-admin/admin-post.php?action=facecheck_save_access" method="post">
            <input type="hidden" name="id" value="<?= (isset($access) ? $access['id'] : '') ?>"/>
            <label for="access_name">Название:</label>
            <br>
            <input type="text" id="access_name" name="access_name"
                   value="<?= (isset($access) ? $access['name'] : '') ?>"
                   placeholder="Введите название"/>
            <br/>
            <label for="slug">Slug:</label>
            <br>
            <input type="text" id="slug" name="slug" value="<?= (isset($access) ? $access['slug'] : '') ?>"/>
            <br>
            <br>
            <input id="not_reg" type="checkbox" name="not_reg"
                   value="1" <?= (isset($access['not_reg']) && $access['not_reg'] !== '0') ? "checked" : "" ?>>
            <label
                for="not_reg">Активно для незарегистрированных пользователей</label><br>
            <br>
            <input id="registered" type="checkbox" name="registered"
                   value="1" <?= (isset($access['registered']) && $access['registered'] !== '0') ? "checked" : "" ?>>
            <label
                for="registered">Активно для зарегистрированных пользователей</label><br>
            <br>
            <div style="overflow: hidden;">
                <div style="float: left;width: 300px;">
                    <h4>Секции:</h4>
                    <?php foreach ($sections as $section) { ?>
                        <input id="access-id-<?= $section['id'] ?>" type="checkbox" name="sections-id[]"
                               value="<?= $section['id'] ?>" <?= (isset($acces_to_sec) && key_exists($section['id'], $acces_to_sec)) ? "checked" : "" ?>>
                        <label
                            for="access-id-<?= $section['id'] ?>"><?= $section['name'] ?></label><br>
                    <?php } ?>
                    <br>
                </div>
                <div style="float: left; width: 300px;">
                    <h4>Характеристики:</h4>
                    <?php foreach ($specifications as $specification) { ?>
                        <input id="specification-id-<?= $specification['id'] ?>" type="checkbox"
                               name="specification-id[]"
                               value="<?= $specification['id'] ?>" <?= (isset($acces_to_spec) && key_exists($specification['id'], $acces_to_spec)) ? "checked" : "" ?>>
                        <label
                            for="specification-id-<?= $specification['id'] ?>"><?= $specification['name'] ?></label><br>
                    <?php } ?>
                    <br>
                </div>
            </div>
            <input type="submit" id="access-save" name="submit" class="button button-primary button-large"
                   value="Сохранить"/>
        </form>
        <?php
        } else {
            if (!empty($_POST['submit'])) {
                switch ($_POST['action']) {
                    case 'delete' :
                        global $wpdb;
                        $wpdb->delete($wpdb->prefix . 'facecheck_access_to_sections', array('id_access' => $_POST['access']));
                        $wpdb->delete($wpdb->prefix . 'facecheck_access_to_spec', array('id_access' => $_POST['access']));
                        $wpdb->delete($wpdb->prefix . 'facecheck_access_level', array('id' => $_POST['access']));
//                $section = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections` WHERE `id`='{$_POST['section']}'", ARRAY_A);
//                $wpdb->query("UPDATE `{$wpdb->prefix}facecheck_report_sections` SET `position`=`position`-1 WHERE `position`>'{$section['position']}'");
//                $wpdb->query("DELETE FROM `{$wpdb->prefix}facecheck_report_sections` WHERE `id`='{$section['id']}'");
//                $wpdb->query("DELETE FROM `{$wpdb->prefix}facecheck_reports` WHERE `section_id`='{$section['id']}'");
                        break;
                }
            }

            $access = Facecheck::getAccessLevel();
            ?>

            <table class="widefat">
                <thead>
                <th width="300px">Название</th>
                <th width="100px">Slug</th>
                <th width="100px">Количество пользователей</th>
                <th width="100px"></th>
                <th width="100px" colspan="2"></th>
                </thead>
                <tfoot>
                <th>Название</th>
                <th>Slug</th>
                <th></th>
                <th colspan="2"></th>
                </tfoot>
                <tbody>
                <?php $alternate = true; ?>
                <?php foreach ($access as $acces) { ?>
                    <tr class="<?= ($alternate ? 'alternate' : '') ?>">
                        <?php $alternate = !$alternate ?>
                        <td>
                            <span style="display: block; margin: 15px 0 0 ;"><?= $acces['name'] ?></span>
                        </td>
                        <td>
                            <span style="display: block; margin: 15px 0 0 ;"><?= $acces['slug'] ?></span>
                        </td>
                        <td style="padding-top: 15px; text-align: center;"><?= $acces['users'] ?></td>
                        <td style="padding-top: 15px;">
                            <a href="/wp-admin/admin.php?page=facecheck/admin/access_level.php&access=<?= $acces['id'] ?>"
                               class="edit">Изменить</a>
                        </td>
                        <td style="padding-top: 15px;">
                            <a href="#" class="delete" data-access-id="<?= $acces['id'] ?>">Удалить</a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <script type="text/javascript">
                jQuery(function () {
                    jQuery('a.delete, a.moveup, a.movedown').click(function (e) {
                        e.preventDefault();
                        jQuery('#facecheck-access-action').val(jQuery(this).attr('class'));
                        jQuery('#facecheck-access-id').val(jQuery(this).data('access-id'));
                        jQuery('#facecheck-access-submit').click();
                    });
                });
            </script>
            <form id="facecheck-sections" action="/wp-admin/admin.php?page=facecheck/admin/access_level.php"
                  method="post">
                <input type="hidden" id="facecheck-access-action" name="action" value=""/>
                <input type="hidden" id="facecheck-access-id" name="access" value=""/>
                <input type="submit" id="facecheck-access-submit" name="submit" value="1" style="display:none;"/>
            </form>
        <?php } ?>
    </div>

