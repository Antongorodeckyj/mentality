<?php
/*template name: Professions */
if (!is_user_logged_in()) {
    include dirname(__DIR__) . '/template/not_login_user.php';
} else {
    
global $facecheck_shortcode;
global $wpdb;

$categories = Facecheck::getProfessionCategories();
$categories[0] = array(
    'id' => 0,
    'name' => 'Другие профессии'
);

$professions = Facecheck::getProfessions($_SESSION['facecheck_user']->id);
foreach ($professions as $profession) {
    $categories[$profession['id_category']]['professions'][] = $profession;
}
$my_professions = Facecheck::getMyProfessions($_SESSION['facecheck_user']->id);
foreach ($my_professions as $profession) {
    $my_categories[$profession['id_category']]['professions'][] = $profession;
}

ob_start();
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    var profs = <?=json_encode(Facecheck::getProfessinsNames())?>;
    $( function() {
        $( "#fprof" ).autocomplete({
          source: profs,
          select: function( event, ui ) {
              jQuery.magnificPopup.open({
                items: {
                    src: jQuery('#upload-popup'),
                    type: 'inline'
                },
                closeOnBgClick: false
              });
              location.href=$('a:contains("'+ui.item.value+'")').attr('href');
          }
        });
    });
</script>
<div class="sfPageHeader">
<div class="col span_8" data-animation="" data-delay="0">
<h1>Каталог профессий</h1>
</div>
<div class="col span_4 col_last" data-animation="" data-delay="0">
<div class="infoButton"><a class="infoSwitcher" href="#Howto"><img src="/wp-content/uploads/2016/07/i-2.png" alt="" /></a><a class="infoSwitcher" href="#Howto">как этим пользоваться?</a></div>
</div><div class="clear"></div></div>
<div class="search"><input type="text" id="fprof" name="fprof" value="<?=$prof?>" placeholder="Поиск по профессии"  AUTOCOMPLETE="off"/></div>
    <div class="facecheck-container-wrap">
        <div class="container main-content">

                <?php //if(count($my_categories)) { ?>
                    <div class="category" id="sfUserCategories">
                        <div class="title">Мои категории:<a href="#" class="toggle"
                                                                      onclick="return false;"></a></div>
                        <ul class="content">
                            <li>
                                <a class="new" href="<?= add_query_arg(array('id_category' => $category['id']), get_permalink($facecheck_shortcode['specifications_scanface'])) ?>">+Новая профессия</a>
                            </li>
                            <?php foreach ($my_categories as $category) {
                            if (isset($category['professions'])) {
                                foreach ($category['professions'] as $profession) { ?>
                                    <li>
                                        <a <?= $profession['id_user'] ? '' : 'class="base"' ?>
                                            href="<?= add_query_arg(array('id_profession' => $profession['id']), get_permalink($facecheck_shortcode['specifications_scanface'])) ?>"><?= $profession['name'] ?></a>
                                        <?php if ($profession['id_user']) { ?>
                                            <a class="delete" href="" data-id="<?= $profession['id'] ?>"
                                               onclick="return false;">удалить</a>
                                        <?php } else { ?>
                                            <span class="base_label" href="">базовая профессия</span>
                                        <?php } ?>
                                    </li>
                            <?php }}} ?>
                        </ul>
                    </div>
                <?php //} ?>
                
            <div class="professions">

                <?php foreach ($categories as $category) { ?>
                    <div class="category">
                        <div class="title"><?= $category['name'] ?><a href="#" class="toggle"
                                                                      onclick="return false;"></a></div>
                        <ul class="content" style="display:none">
<li>
                                <a class="new" href="<?= add_query_arg(array('id_category' => $category['id']), get_permalink($facecheck_shortcode['specifications_scanface'])) ?>">+Новая профессия</a>
                            </li>
                            <?php
                            if (isset($category['professions'])) {
                                foreach ($category['professions'] as $profession) { ?>
                                    <li>
                                        <a <?= $profession['id_user'] ? '' : 'class="base"' ?>
                                            href="<?= add_query_arg(array('id_profession' => $profession['id']), get_permalink($facecheck_shortcode['specifications_scanface'])) ?>"><?= $profession['name'] ?></a>
                                        <?php if ($profession['id_user']) { ?>
                                            <a class="delete" href="" data-id="<?= $profession['id'] ?>"
                                               onclick="return false;">удалить</a>
                                        <?php } else { ?>
                                            <span class="base_label" href="">базовая профессия</span>
                                        <?php } ?>
                                    </li>
                                <?php }
                            }
                            ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
        </div>

        <script type="text/javascript">
    jQuery(function () {   
        jQuery('.professions .title .toggle').click(function () {
        var $this = jQuery(this).parent().next();
        var bar = 0;
        jQuery(this).toggleClass('open'); 
        if(jQuery(this).hasClass('open')) bar = 1;
        jQuery('.toggle').removeClass('open');
        jQuery('.professions ul').hide();
        if(bar) {
            $this.show();
            jQuery(this).addClass('open'); 
        }
        else {
            jQuery(this).removeClass('open'); 
        }
         });
                jQuery('a.delete').click(function () {
                    var $this = jQuery(this);
                    if (confirm("Вы действительно хотите удалить эту профессию?")) {
                        jQuery.ajax({
                            url: '/wp-admin/admin-ajax.php',
                            type: 'post',
                            data: {
                                action: 'facecheck_delete_profession',
                                profession: $this.data('id')
                            },
                            success: function (response) {
                                $this.parent().hide();
                            }
                        });
                    }
                })
            });
        </script>
    </div>
<div style="display: none;">
    <div id="upload-popup" class="fchk popup">
        <div class="content">
            <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/loading.gif"/>
            <p>Пожалуйста подождите, переходим на страницу профессии...</p>
        </div>
    </div>
</div>
<?php
$output_string = ob_get_contents();
ob_end_clean();
} // end if not login