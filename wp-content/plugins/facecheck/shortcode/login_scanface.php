<?php
/*template name: Login */
global $facecheck_shortcode;
if(isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'facecheck_oauth_facebook' : facecheck_oauth_facebook(); break;
        case 'facecheck_oauth_linkedin' : facecheck_oauth_linkedin(); break;
    }
}

$form_data = array();
if (!empty($_POST)) {
    $form_data = facecheck_user_login();
}

get_header();

if (isset($_SESSION['new_password'])) {
    unset($_SESSION['new_password']);
    ?>

    <script type="text/javascript">
        jQuery(function() {
            jQuery.magnificPopup.open({
                items: {
                    src: jQuery('#new-password'),
                    type: 'inline'
                }
            });
        })
    </script>

    <?php
}
?>

    <div class="facecheck-container-wrap">
        <div class="container main-content">
            <div class="auth">
                <h1>Вход</h1>
                <form action="" method="post">
                    <input type="hidden" name="success_page" value="<?= (isset($_SESSION['redirect_result_id']) ? add_query_arg('facecheck_photo_id', $_SESSION['redirect_result_id'], get_permalink($facecheck_shortcode['result_scanface'])) : get_permalink($facecheck_shortcode['upload_scanface'])) ?>" />

                    <div>
                        <input type="text" class="email" name="email" value="<?=(isset($form_data['email']) ? $form_data['email'] : '')?>" placeholder="Электронная почта111" />
                        <?php if (isset($form_data['errors']['email'])) { ?><label for="email" class="error"><?=$form_data['errors']['email']?></label><?php } ?>
                    </div>

                    <div>
                        <input type="password" name="password" value="" placeholder="Пароль" />
                        <?php if (isset($form_data['errors']['password'])) { ?><label for="email" class="error"><?=$form_data['errors']['password']?></label><?php } ?>
                    </div>
                    
                    <div>
                        <input type="checkbox" name="remember" id=remember checked />
                        <label for="remember">Запомнить меня</label>
                        <a href="<?= get_permalink($facecheck_shortcode['new_password_scanface']) ?>" class="remember">Забыли пароль?</a>
                    </div>

                    <input class="fchk-button accent-color submit" type="submit" value="Войти" />
                </form>
                <div class="sfSocialLogin"><?php echo do_shortcode('[miniorange_social_login theme="default"]') ?></div>
            </div>
        </div>
        <div style="display: none;">
            <div id="new-password" class="fchk popup">
                <div class="content">
                    <p>На ваш email отправлен новый пароль для входа.</p>
                </div>
            </div>
        </div>
    </div>

    

<?php get_footer(); ?>