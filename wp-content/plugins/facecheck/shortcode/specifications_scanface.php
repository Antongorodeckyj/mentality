<?php
/* template name: Specifications */
if (is_user_logged_in()) {
    
global $params_shortcode;
global $facecheck_shortcode;
global $wpdb;

if (!isset($_GET['id_profession']) && !isset($_GET['id_category'])) {
    wp_redirect(home_url());
}

if (!empty($_POST)) {
    $result = facecheck_save_custom_specifications();
    $errors = $result['errors'];
    $specval = $result['specval'];
}

if (isset($_GET['id_profession'])) {
    $profession = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_professions` WHERE `id` = {$_GET['id_profession']}", ARRAY_A);
    if ($profession['id_user'] && $profession['id_user'] != $_SESSION['facecheck_user']->id) {
        wp_redirect(home_url());
    }
}
$specifications = Facecheck::getSpecifications(isset($profession) ? $profession['id'] : 0);

if (isset($specval)) {
    foreach ($specval as $id => $val) {
        $specifications[$id]['prof_left'] = $val['left'];
        $specifications[$id]['prof_right'] = $val['right'];
    }
} else if (!isset($profession)) {
    foreach ($specifications as $id => $spec) {
        $specifications[$id]['prof_left'] = 0;
        $specifications[$id]['prof_right'] = 3;
    }
}
}

//get_header();
if (!is_user_logged_in()) {
    include dirname(__DIR__) . '/template/not_login_user.php';
} else {
?>
<?=$params_shortcode['title']?><?=$params_shortcode['question']?>
<div class="facecheck-container-wrap stepContent">
    <div class="container main-content">
        <div class="row">
            <?php if (isset($profession)) { ?>
                <h1 style="text-align: center; margin-top: 20px;">Редактирование профессии</h1>
            <?php } else { ?>
                <h1 style="text-align: center; margin-top: 20px;">Новая профессия</h1>
            <?php } ?>

            <div class="report edit-specs">
                <form action="" method="post">
                    <input type="hidden" name="id_origin"
                           value="<?= !empty($_GET['id_profession']) ? $_GET['id_profession'] : "" ?>"/>
                    <input id="prof-id" type="hidden" name="id_profession"
                           value="<?= isset($profession['id_user']) ? $profession['id'] : 0 ?>"/>
                    <input id="cat-id" type="hidden" name="id_category"
                           value="<?= isset($profession) ? $profession['id_category'] : $_GET['id_category'] ?>"/>

                    <div style="padding: 25px; text-align: center;">
                        <label
                            style="font-family: 'HelveticaNeueCyr-Bold', 'Open Sans', sans-serif; font-size: 18px; line-height: 20px; font-weight: bold; color: #000; margin-right: 20px;">Название</label>
                        <input type="text" name="name_scanface"
                               value="<?= isset($profession['name']) ? $profession['name'] : "" ?>"
                               style="line-height: 16px; padding: 10px; width: 40%;"/>
                        <?php if (isset($errors['name'])) { ?>
                            <label class="error"><?= $errors['name'] ?></label>
                        <?php } ?>
                    </div>

                    <?php foreach ($specifications as $id => $specification) { ?>
                        <div class="legend" data-spec-id="<?= $id ?>">
                            <div class="legend__dash"></div>

                            <div class="legend__title"><?= $specification['name'] ?></div>

                            <div class="legend__left">
                                <div class="legend__circle-dash"></div>

                                <div class="legend__head">
                                    <div class="legend__head-title legend__head-title-from">
                                        <?= $specification['left_title'] ?>
                                        <a href="" class="legend__head-title-info">
                                            i
                                            <span class="legend-hint"><?= $specification['left_text'] ?></span>
                                        </a>
                                    </div>

                                    <div class="legend__head-title legend__head-title-to">
                                        <?= $specification['right_title'] ?>
                                        <a href="" class="legend__head-title-info">
                                            i
                                            <span class="legend-hint"><?= $specification['right_text'] ?></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="legend__diagramm">
                                    <div class="legend__diagramm-progress">
                                        <?php
                                        if ($specification['prof_left'] < 0) {
                                            $left = 0;
                                        } else {
                                            $left = $specification['prof_left'];
                                        }

                                        if ($specification['prof_right'] > 10) {
                                            $width = (10 - $left);
                                        } else {
                                            $width = $specification['prof_right'] - $left;
                                        }
                                        ?>
                                        <div class="legend__diagramm-progress-inner"
                                             style="position: relative; left: <?= 10 * $left ?>%; width: <?= 10 * $width ?>%; "></div>
                                    </div>
                                    <ul class="legend__diagramm-grid">
                                        <?php for ($v = 1; $v <= 10; $v++) { ?>
                                            <li></li>
                                        <?php } ?>
                                    </ul>
                                </div>
<!--
                                <input type="hidden" class="spec-value spec-value-left" name="specval[<?= $id ?>][left]"
                                       value="<?= $specification['prof_left'] ?>"/>
                                <input type="hidden" class="spec-value spec-value-right"
                                       name="specval[<?= $id ?>][right]" value="<?= $specification['prof_right'] ?>"/>
-->                            
</div>

                            
                            <div class="legend__right" data-left-value="<?=$specification['left_value'] ?>" data-right-value="<?=$specification['right_value'] ?>">
                                <div>
                                    <input type="text" class="spec-value spec-value-left" name="specval[<?= $id ?>][left]" value="<?= $specification['prof_left'] ?>"  />
                                    <input type="text" class="spec-value spec-value-right" name="specval[<?= $id ?>][right]" value="<?= $specification['prof_right'] ?>"  />
                                </div>
                                <?php if ($errors["specval_{$id}"]) { ?>
                                    <label class="error"><?= $errors["specval_{$id}"]; ?></label>
                                <?php } ?>
                            </div>
                            


                            <div class="clear"></div>
                        </div>
                    <?php } ?>

                    <div class="clear"></div>

                    <?php if ($profession['id_user'] || !isset($profession)) {?>
                    <div style="text-align: center; margin-top: 40px; padding-bottom: 20px;">
                        <input type="submit" class="fchk-button accent-color" value="Сохранить"/>
                    </div>
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
}
//get_footer() 
?>

<script>
    jQuery(function () {
        jQuery('.legend__diagramm-progress-inner').each(function () {
            var step = jQuery(this).parent().width() / 10;
            jQuery(this).draggable({
                axis: 'x',
                containment: 'parent',
                grid: [step, 0],
                drag: function (event, ui) {
                    var left_value = Math.round(ui.position.left / step);
                    var right_value = left_value + Math.round(jQuery(this).width() / step);
                    jQuery(this).parents('.legend').find('.spec-value-left').val(left_value);
                    jQuery(this).parents('.legend').find('.spec-value-right').val(right_value);
                },
                stop: function (event, ui) {
                    var left_value = jQuery(this).parents('.legend').find('.spec-value-left').val();

                    jQuery(this).css('left', left_value * step + 0.1 + 'px');
                }
            })
        })
        jQuery('.legend__right input').change(function() {
            var $specification = jQuery(this).parents('.legend__right');
            left = parseInt($specification.find('.spec-value-left').val());
            right = parseInt($specification.find('.spec-value-right').val());
            if(left < $specification.data('left-value'))
                left = $specification.data('left-value');
            if(right > $specification.data('right-value')) 
                right = $specification.data('right-value');
            if(left >= right && jQuery(this).attr('class')=='spec-value spec-value-left') left = right-1;
            if(right <= left && jQuery(this).attr('class')=='spec-value spec-value-right') right = parseInt(left)+1;
            $specification.find('.spec-value-left').val(left);
            $specification.find('.spec-value-right').val(right);
            var lpercent = left * 100 / ($specification.data('right-value') - $specification.data('left-value'));
            var rpercent = right * 100 / ($specification.data('right-value') - $specification.data('left-value'));
            jQuery(this).parents('.legend').find('.legend__diagramm-progress-inner').css({left: lpercent + '%','width' : rpercent - lpercent + '%'});
        });
    })
</script>