<?php
/*template name: Upload */
Global $wp_query;
global $facecheck_shortcode;
ob_start();
?>
    <script type="text/javascript">
        var redirectUrl = '<?=get_permalink($facecheck_shortcode['markers_scanface'])?>';
    </script>


    <div class="facecheck-container-wrap stepContent">
		<div id="uploader-wrapper">
            <input id="file-input" type="file" name="photo" accept="image/*"/>
                <div id="uploader"></div>
        </div>
        <?php echo do_shortcode('[capture_video]') ?>
		<div style="display: none;">
            <div id="upload-popup" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/loading.gif"/>
                    <p>Пожалуйста подождите, фото обрабатывается...</p>
                </div>
            </div>


            <div id="bad-photo" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/error1.png"/>
                    <p>Фотография не пригодна для анализа,<br/><a href="#"
                                                                  onclick="jQuery.magnificPopup.close(); initUploader(); return false;">попробуйте
                            еще раз</a></p>
                </div>
            </div>
            <div id="err-photo" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/error1.png"/>
                    <p>Извините произошла ошибка<br/><a href="#"
                                                        onclick="jQuery.magnificPopup.close(); initUploader(); return false;">попробуйте
                            еще раз</a></p>
                </div>
            </div>
            <div id="permission-err-photo" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/error1.png"/>
                    <p>Сожалеем но у Вас нет права загружать фотографию</p>
                </div>
            </div>
            <div id="upload-photo" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/loading.gif"/>
                    <p>Пожалуста подождите, Фото загружается и обрабатывается</p>
                </div>
            </div>

            <div id="error-cam" class="fchk popup">
                <div class="content">
                    <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/error1.png"/>
                    <p>Извините произошла ошибка,<br> Либо Вы не дали разрешение на использование камеры либо Ваша
                        камера не подключена</p>
                </div>
            </div>

            <?php
            /** @var bool $permissions */
            if (!isset($_SESSION['facecheck_user']) || !$permissions) { ?>
                <div id="registration-popup" class="fchk popup">
                    <div class="content">
                        <p>Зарегистрируйтесь, что бы получить<br/>больше возможностей</p>
                        <a class="fchk-button accent-color"
                           href="<?= get_permalink($facecheck_shortcode['registration_scanface']) ?>">Бесплатная
                            регистрация</a>
                    </div>
                </div>

                <div id="permissions-popup" class="fchk popup">
                    <div class="content">
                        <p>Сожалеем но у Вас нет права загружать фотографию</p>
                    </div>
                </div>

                <!--            <script src="/wp-content/plugins/facecheck/assets/js/jquery.fileupload.js"></script>-->
                <!--            <script src="/wp-content/plugins/facecheck/assets/js/upload.js"></script>-->

                <script type="text/javascript">
                    jQuery(function () {
                        showRegistrationPopup = function (e) {
//                            e.stopImmediatePropagation();
//                            e.preventDefault();
                            jQuery.magnificPopup.open({
                                items: {
                                    src: jQuery('#registration-popup'),
                                    type: 'inline'
                                }
                            })
                        };

                        jQuery('.RegPopup').on('click', function () {
                            jQuery.magnificPopup.open({
                                items: {
                                    src: jQuery('#registration-popup'),
                                    type: 'inline'
                                }
                            })
                        })

                    });

                    <?php
                    if(!$permissions){?>

                    showPermissionPopUp = function (e) {
                        e.preventDefault();
                        jQuery.magnificPopup.open({
                            items: {
                                src: jQuery('#permissions-popup'),
                                type: 'inline'
                            }
                        });
                    };
                    jQuery('#file-input').on('click', function (e) {
//                    e.preventDefault();
//                    showPermissionPopUp();
                        console.log(this)
                    });
                    <?php }?>

                </script>
            <?php } ?>

        </div>
    </div>
<?php
$output_string = ob_get_contents();
ob_end_clean();