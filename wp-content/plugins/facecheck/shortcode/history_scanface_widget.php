<!-- BEGIN -->
<?php 
/*template name: History */ 
if (!is_user_logged_in()) { // !count($_SESSION['photo_ids'])
    include dirname(__DIR__) . '/template/not_login_user.php';
} else {

global $params_shortcode;
global $wpdb;
global $facecheck_shortcode;

$name = !empty($_GET['fname']) ? addslashes($_GET['fname']) : '';
$prof = !empty($_GET['fprof']) ? addslashes($_GET['fprof']) : null;
$date_from = !empty($_GET['ffrom']) ? date("Y-m-d H:i:s",strtotime(addslashes($_GET['ffrom']))) : false;
$date_to = !empty($_GET['fto']) ? date("Y-m-d H:i:s",strtotime(addslashes($_GET['fto']))) : false;
$fsort = !empty($_GET['fsort']) ? addslashes($_GET['fsort']) : false;
if($date_from && !$date_to) {
    $date_to = date('Y-m-d');
    $_GET['fto'] = $date_to;
}
if(!$date_from && $date_to) {
    $date_from = '2000-01-01'; 
    $_GET['ffrom'] = $date_from;
}
$ids_nologin = '';
if($_SESSION['facecheck_user']->id==0 && is_array($_SESSION['photo_ids'])){
    $ids_nologin = 'AND id in ('.implode(',',$_SESSION['photo_ids']).')';
}
$pr_sel = ($fsort=='pc.percent' || $fsort=='pc.percent desc') ? true : false;
$prof_join = ($prof || $pr_sel) ? "INNER JOIN `{$wpdb->prefix}facecheck_user_report_professions` AS `pc` ON `pc`.`photo_id`=`p`.`id`
                        LEFT JOIN `{$wpdb->prefix}facecheck_professions` AS `pr` ON `pc`.`profession_id`=`pr`.`id`" : '';
$prof_where = ($prof) ? "AND `pr`.`name`='$prof'" : '';
$prof_select = ($prof || $pr_sel) ? ", pc.percent" : '';
$date = ($date_from) ? "AND `p`.`date`>='$date_from' AND `p`.`date`<='$date_to'" : '';
$name_where = ($name) ? "AND `p`.`name` LIKE '%{$name}%'" : '';
$fsort = ($fsort) ? "ORDER BY $fsort, xx " : 'ORDER BY xx, date desc';
        
$count_photo_on_page = 16;

$pagesCount = $wpdb->get_var("
    SELECT CEILING(COUNT(`p`.`id`) / $count_photo_on_page)
    FROM `{$wpdb->prefix}facecheck_photos` AS `p`
    $prof_join
    WHERE `p`.`user_id`='{$_SESSION['facecheck_user']->id}' $prof_where $date $name_where $ids_nologin
");
$page = isset($_GET['page_num']) ? $_GET['page_num'] : '1';

$limit = $page * $count_photo_on_page - $count_photo_on_page;
$sql = "
    SELECT `p`.*, if(p.name='','яяяяяя',p.name) as xx $prof_select
    FROM `{$wpdb->prefix}facecheck_photos` AS `p`
    $prof_join
    WHERE `p`.`user_id`='{$_SESSION['facecheck_user']->id}' $prof_where $date $name_where $ids_nologin
    GROUP BY `p`.`id`
    $fsort LIMIT $limit, $count_photo_on_page
";
    //var_dump($sql); exit;
//del photos without date
$wpdb->delete($wpdb->prefix . 'facecheck_photos', ['markers' => '']);

$photos = $wpdb->get_results($sql, ARRAY_A);

$userGroups = $_SESSION['facecheck_user']->groups();
//var_dump($sql); exit;
if ($photos) {
    $photo_id = get_query_var('facecheck_photo_id');
    if ($photo_id) {
        foreach ($photos as $ph) {
            if ($ph['id'] == $photo_id) {
                $photo = Facecheck::getPhoto($photo_id);
                break;
            }
        }
    }

    if (!isset($photo)) {
        $photo = Facecheck::getPhoto($photos[0]['id']);
    }

    if ($page == 1) {
        $afterDeletePage = 1;
    } elseif (count($photos) == 1) {
        $afterDeletePage = $page - 1;
    } else {
        $afterDeletePage = $page;
    }

    $photoGroups = Facecheck::getPhotoGroups($photo->id);

    if (count($userGroups)) {
        foreach ($userGroups as $index => $group) {
            if (isset($photoGroups[$group['id']])) {
                $userGroups[$index]['selected'] = true;
            } else {
                $userGroups[$index]['selected'] = false;
            }
        }
    }

    $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections`", ARRAY_A);
    $sections = array();
    foreach ($rows as $row) {
        $sections[$row['id']] = $row;
    }
    $report = facecheck_get_result($photo->id);
}

$queryArgs = array();
if (isset($photo_id) && isset($photo) && $photo_id == $photo->id) {
    $queryArgs['facecheck_photo_id'] = $photo_id;
}

if (isset($page) && $page > 1) {
    $queryArgs['page_num'] = $page;
}

$name = isset($_GET['fname']) ? $queryArgs['fname'] = $_GET['fname'] : false;
$prof = isset($_GET['fprof']) ? $queryArgs['fprof'] = $_GET['fprof'] : false;
$date_from = isset($_GET['ffrom']) ? $queryArgs['ffrom'] = $_GET['ffrom'] : false;
$date_to = isset($_GET['fto']) ? $queryArgs['fto'] = $_GET['fto'] : false;
$fsort = isset($_GET['fsort']) ? $queryArgs['fsort'] = $_GET['fsort'] : false;

if ($reverse) {
    $queryArgs['reverse'] = '';
}

$profCats = Facecheck::getProfessionCategories();
$rows = Facecheck::getProfessions(isset($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : 0);
foreach ($rows as $row) {
    if (!key_exists($row['id_category'], $profCats)) {
        $profCats[$row['id_category']] = array(
            'id' => 0,
            'name' => 'Другие профессии',
            'profs' => array()
        );
    }

    if (!key_exists('profs', $profCats[$row['id_category']]) || !is_array($profCats[$row['id_category']]['profs'])) {
        $profCats[$row['id_category']]['profs'] = array();
    }

    $profCats[$row['id_category']]['profs'][$row['id']] = $row;
}

$specifications = Facecheck::getSpecifications();
$prof_spec = $wpdb->get_results("
    SELECT
        `prof`.`id_profession`,
        `prof`.`id_specification`,
        `prof`.`description`,
        `prof`.`left_value`,
        `prof`.`right_value`
    FROM `{$wpdb->prefix}facecheck_professions_specifications` AS `prof`
", ARRAY_A);

foreach ($prof_spec as $item) {
    if (!key_exists('profs', $specifications[$item['id_specification']]) || !is_array($specifications[$item['id_specification']]['profs'])) {
        $specifications[$item['id_specification']]['profs'] = array();
//        echo$item['id_specification']."\n";
    }
        $specifications[$item['id_specification']]['profs'][$item['id_profession']] = $item;

}

$tmp_page = $page;
$page = $tmp_page;
unset($tmp_page);

$emails = $wpdb->get_results("
    SELECT `email`
    FROM `{$wpdb->prefix}facecheck_users_emails`
    WHERE `id_user` = {$_SESSION['facecheck_user']->id}
    ORDER BY `id`
", ARRAY_A);
ob_start();
?>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        var isLogged = true;
        var page_url = '<?= get_permalink($facecheck_shortcode['history_scanface']); ?>';

        var professions = <?= json_encode($profCats) ?>;
        var specifications = <?= json_encode($specifications) ?>;
        var specsCount = <?= count($specifications) ?>;
        var profs = <?=json_encode(Facecheck::getProfessinsNames())?>;

  $( function() {

    $( "#fprof" ).autocomplete({
      source: profs
    });
    var dateFormat = "dd.mm.yy";
    trans = {
          monthNames: ['Январь', 'Февраль', 'Март', 'Апрель','Май', 'Июнь', 'Июль', 'Август', 'Сентябрь','Октябрь', 'Ноябрь', 'Декабрь'],
          dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
          monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
          firstDay: 1,
          dateFormat: dateFormat,
          defaultDate: "+1w",
          changeMonth: true,
        };
    var from = $( "#ffrom" )
        .datepicker(trans)
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
        to = $( "#fto" ).datepicker(trans)
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
    $(document).ready(function(){
        $("#s2id_autogen1").on("mouseover", function(e) { 
            $('.hasDatepicker').datepicker("hide"); 
        });
    })
  } );
  </script>
    <?=$params_shortcode['title']?><?=$params_shortcode['question']?>
    <div class="facecheck-container-wrap stepContent">
        <div class="container main-content">
            <form name="filter_form" action="" method="get">
            <div class="row">
                <div class="filter span_12">
				<div class="my-filters-sf-first">
				  <div class="filt-sf-f"> <input type="text" id="fname" name="fname" value="<?=$name?>" placeholder="По имени"/> </div>
				  <div class="filt-sf-f"> <input type="text" id="fprof" name="fprof" value="<?=$prof?>" placeholder="По профессии"/> </div>
                  <div class="filt-sf-f"> <input type="text" id="ffrom" name="ffrom" value="<?=$date_from?>" placeholder="Дата От"> 
                    <input type="text" id="fto" name="fto" value="<?=$date_to?>" placeholder="Дата До"> </div>
				</div>
				<div class="my-filters-sf-second">
                    <div class="filt-sf-f"> <select name="fsort">
                        <option value="p.name" <?php if($fsort=='p.name') echo 'selected';?>>Имя по возрастанию</option>
                        <option value="p.name desc" <?php if($fsort=='p.name desc') echo 'selected';?>>Имя по убыванию</option>
                        <option value="p.date" <?php if($fsort=='p.date') echo 'selected';?>>Дата по возрастанию</option>
                        <option value="p.date desc" <?php if($fsort=='p.date desc' || !isset($fsort)) echo 'selected';?>>Дата по убыванию</option>
                        <option value="pc.percent" <?php if($fsort=='pc.percent') echo 'selected';?>>Соответствие профессии по возрастанию</option>
                        <option value="pc.percent desc" <?php if($fsort=='pc.percent desc') echo 'selected';?>>Соответствие профессии по убыванию</option>-->
                    </select> </div>
                    <div class="filt-sf-f"> <input type="submit" value="Поиск">
                    <a href="<?=get_permalink($post->ID)?>"></a> </div>
				</div>
                </div>
            </div>
            </form>

            <?php if (count($photos)) { ?>
                <div class="row history-container">
                    <div class="headHistory">
                        <?php if ($pagesCount > 1) { ?>
                            <div id="pages">
                                <span><?= $page ?> из <?= $pagesCount ?></span>

                                <?php $args = $queryArgs;
                                unset($args['facecheck_photo_id']);
                                unset($args['page_num']); ?>
                                <?php if ($page == '1') { ?>
                                    <a class="prev" href="#" onclick="return false;"></a>
                                <?php } elseif ($page == 2) { ?>
                                    <a class="prev"
                                       href="<?= add_query_arg($args, get_permalink($post->ID)) ?>"></a>
                                <?php } else { ?>
                                    <?php $args['page_num'] = $page - 1; ?>
                                    <a class="prev"
                                       href="<?= add_query_arg($args, get_permalink($post->ID)) ?>"></a>
                                <?php } ?>

                                <?php $args = $queryArgs;
                                unset($args['facecheck_photo_id']);
                                unset($args['page_num']); ?>
                                <?php if ($page == $pagesCount) { ?>
                                    <a class="next" href="#" onclick="return false;"></a>
                                <?php } else { ?>
                                    <?php $args['page_num'] = $page + 1; ?>
                                    <a class="next"
                                       href="<?= add_query_arg($args, get_permalink($post->ID)) ?>"></a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>

                    <?php if($prof){ ?><h1>Профессия '<?=$prof?>'</h1> <?php } ?>
                    <div class="pagePhotos">
                        <?php $args = $queryArgs; ?>
                        <?php
                        foreach ($photos as $index => $ph) {
                            if(empty((int)$ph['date'])) {
                                continue;
                            }
                            ?>
                            <?php $args['facecheck_photo_id'] = $ph['id']; ?>
                            <div class="item <?php if ($ph['id'] == $photo->id) echo 'active'; ?> <?php if ($index == 3) echo 'last'; ?>">
                                <a href="<?= add_query_arg($args, get_permalink($facecheck_shortcode['history_scanface'])) ?>" class="image"><img src="<?= content_url('/photos/face_' . $ph['id'] . '.jpg') ?>"/></a>
                                <div class="descr">
                                    <span class="name"><?= $ph['name'] ?></span>
                                    <span class="date"><?= date('d.m.Y H:i:s',strtotime($ph['date'])) ?></span>
                                    <?php if($prof || $pr_sel){ ?>
                                        <span class="percent"><?=$ph['percent']?>%</span>
                                    <?php } ?>
                                </div>
                                <div class="icons-act report-buttons">
                                    <a class="view" href="<?= add_query_arg($args, get_permalink($facecheck_shortcode['history_scanface'])) ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>
                                    <!--<a class="letter" href="#">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><br>-->
                                    <a class="delete" href="/wp-admin/admin-ajax.php?action=facecheck_delete_photo&id=<?=$ph['id']?>&redirect=<?= get_permalink(829) /* testing-history */?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            <?php } else { ?>
                <div class="row">
                    <p>Извините, отчетов не найдено.</p>
                </div>
            <?php } ?>
        </div>
        <div style="display: none;">
            <div id="registration-popup" class="fchk popup">
                <div class="content">
                    <p>Зарегистрируйтесь, что бы получить<br/>больше возможностей</p>
                    <a class="fchk-button accent-color" href="<?= get_permalink($facecheck_shortcode['registration_scanface']) ?>">Бесплатная
                        регистрация</a>
                </div>
            </div>

            <div id="report_letter_success-popup" class="fchk popup">
                <div class="content">
                    <p>Отчет отправлен</p>
                </div>
            </div>

            <div id="report_letter_fail-popup" class="fchk popup">
                <div class="content">
                    <p>Произойшла ошибка при отправлении отчета</p>
                </div>
            </div>

            <div id="report_save_success-popup" class="fchk popup">
                <div class="content">
                    <p>Отчет сохранен</p>
                </div>
            </div>

            <div id="report_save_fail-popup" class="fchk popup">
                <div class="content">
                    <p>Произойшла ошибка при сохранении отчета</p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END -->