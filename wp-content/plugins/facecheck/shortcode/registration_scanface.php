<?php
/*template name: Registration */
global $facecheck_shortcode;
$form_data = array();
if (!empty($_POST)) {
    $form_data = facecheck_user_registration();
}

get_header();


?>

<div class="facecheck-container-wrap">
    <div class="container main-content">
        <div class="auth">
            <h1>Зарегистрируйтесь</h1>
            <form action="" method="post">
                <input type="hidden" name="success_page" value="<?= (isset($_SESSION['redirect_result_id']) ? add_query_arg('facecheck_photo_id', $_SESSION['redirect_result_id'], get_permalink($facecheck_shortcode['result_scanface'])) : get_permalink($facecheck_shortcode['upload_scanface'])) ?>" />

                <div>
                    <input type="text" name="name_scanface" class="name" value="<?=(isset($form_data['name']) ? $form_data['name'] : '')?>" placeholder="Имя Фамилия" />
                    <?php if (isset($form_data['errors']['name'])) { ?><label for="name" class="error"><?=$form_data['errors']['name']?></label><?php } ?>
                </div>

                <div>
                    <input type="text" name="email" class="email" value="<?=(isset($form_data['email']) ? $form_data['email'] : '')?>" placeholder="Электронная почта" />
                    <?php if (isset($form_data['errors']['email'])) { ?><label for="email" class="error"><?=$form_data['errors']['email']?></label><?php } ?>
                </div>

                <div>
                    <input type="password" name="password" value="" placeholder="Придумайте пароль" />
                    <?php if (isset($form_data['errors']['password'])) { ?><label for="password" class="error"><?=$form_data['errors']['password']?></label><?php } ?>
                </div>

                <input class="fchk-button accent-color submit" type="submit" value="Зарегистрироваться" />
            </form>
            <div class="sfSocialLogin"><?php echo do_shortcode('[miniorange_social_login theme="default"]') ?></div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
