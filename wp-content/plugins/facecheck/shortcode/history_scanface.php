<?php 

global $params_shortcode;
global $wpdb;
global $facecheck_shortcode;

if($_POST && $_POST['ses_id'] == $_SESSION['ses_id']) {
    /* только в этом случае приступаем к проверке данных формы */
    facecheck_save_markers();
    setRandomSessid();
}
$name = isset($_GET['filter']) ? $_GET['filter'] : '';
$category = isset($_GET['facecheck_category']) ? $_GET['facecheck_category'] : null;
$reverse = isset($_GET['reverse']) ? true : false;

$page = isset($_GET['page_num']) ? $_GET['page_num'] : '1';
if ($category) {
    $pagesCount = $wpdb->get_var("
        SELECT CEILING(COUNT(`p`.`id`) / 4)
        FROM `{$wpdb->prefix}facecheck_photos` AS `p`
        LEFT JOIN `{$wpdb->prefix}facecheck_photo_category` AS `pc` ON `pc`.`photo_id`=`p`.`id`
        WHERE `p`.`user_id`='{$_SESSION['facecheck_user']->id}' AND `pc`.`category_id` IN ({$category}) AND `p`.`date`!='0000-00-00 00:00:00' AND `p`.`name` LIKE '%{$name}%'
    ");
} else {
    $pagesCount = $wpdb->get_var("SELECT CEILING(COUNT(`id`) / 4) FROM `{$wpdb->prefix}facecheck_photos` WHERE `user_id`='{$_SESSION['facecheck_user']->id}' AND `date`!='0000-00-00 00:00:00' AND `name` LIKE '%{$name}%'");
}
$limit = $page * 4 - 4;

if ($category) {
    $photos = $wpdb->get_results("
        SELECT `p`.*
        FROM `{$wpdb->prefix}facecheck_photos` AS `p`
        LEFT JOIN `{$wpdb->prefix}facecheck_photo_category` AS `pc` ON `pc`.`photo_id`=`p`.`id`
        WHERE `p`.`user_id`='{$_SESSION['facecheck_user']->id}' AND `pc`.`category_id` IN ({$category}) AND `p`.`date`!='0000-00-00 00:00:00' AND `p`.`name` LIKE '%{$name}%'
        GROUP BY `p`.`id`
        ORDER BY `p`.`date` " . ($reverse ? 'ASC' : 'DESC') . " LIMIT $limit, 4
    ", ARRAY_A);
} else {
    $photos = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_photos` WHERE `user_id`='{$_SESSION['facecheck_user']->id}' AND `date`!='0000-00-00 00:00:00' AND `name` LIKE '%{$name}%' ORDER BY `date` " . ($reverse ? 'ASC' : 'DESC') . " LIMIT $limit, 4", ARRAY_A);
}

$userGroups = $_SESSION['facecheck_user']->groups();

if ($photos) {
    $photo_id = get_query_var('facecheck_photo_id');
    if ($photo_id) {
        $photo = Facecheck::getPhoto($photo_id);
    }

    if ($page == 1) {
        $afterDeletePage = 1;
    } elseif (count($photos) == 1) {
        $afterDeletePage = $page - 1;
    } else {
        $afterDeletePage = $page;
    }

    $photoGroups = Facecheck::getPhotoGroups($photo->id);

    if (count($userGroups)) {
        foreach ($userGroups as $index => $group) {
            if (isset($photoGroups[$group['id']])) {
                $userGroups[$index]['selected'] = true;
            } else {
                $userGroups[$index]['selected'] = false;
            }
        }
    }

    $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections`", ARRAY_A);
    $sections = array();
    foreach ($rows as $row) {
        $sections[$row['id']] = $row;
    }
    $report = facecheck_get_result($photo->id);
}

$queryArgs = array();
if (isset($photo_id) && isset($photo) && $photo_id == $photo->id) {
    $queryArgs['facecheck_photo_id'] = $photo_id;
}

if (isset($page) && $page > 1) {
    $queryArgs['page_num'] = $page;
}

if (isset($name) && $name != '') {
    $queryArgs['filter'] = $name;
}

if ($reverse) {
    $queryArgs['reverse'] = '';
}

$profCats = Facecheck::getProfessionCategories();
$rows = Facecheck::getProfessions(isset($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : 0);
foreach ($rows as $row) {
    if (!key_exists($row['id_category'], $profCats)) {
        $profCats[$row['id_category']] = array(
            'id' => 0,
            'name' => 'Другие профессии',
            'profs' => array()
        );
    }

    if (!key_exists('profs', $profCats[$row['id_category']]) || !is_array($profCats[$row['id_category']]['profs'])) {
        $profCats[$row['id_category']]['profs'] = array();
    }

    $profCats[$row['id_category']]['profs'][$row['id']] = $row;
}

$specifications = Facecheck::getSpecifications();
$prof_spec = $wpdb->get_results("
    SELECT
        `prof`.`id_profession`,
        `prof`.`id_specification`,
        `prof`.`description`,
        `prof`.`left_value`,
        `prof`.`right_value`
    FROM `{$wpdb->prefix}facecheck_professions_specifications` AS `prof`
", ARRAY_A);

foreach ($prof_spec as $item) {
    if (!key_exists('profs', $specifications[$item['id_specification']]) || !is_array($specifications[$item['id_specification']]['profs'])) {
        $specifications[$item['id_specification']]['profs'] = array();
//        echo$item['id_specification']."\n";
    }
        $specifications[$item['id_specification']]['profs'][$item['id_profession']] = $item;

}
$photo_id = $photo->id;
$sel_prof = Facecheck::getSelectedPofessions($_SESSION['facecheck_user']->id,$photo_id,$report);
$access = Facecheck::getAccess($photo->full_access);
$access_spec = $access_sec = array();
foreach($access['spec'] as $s){
    $access_spec[] = $s['id_spec'];
}
foreach($access['sec'] as $s){
    $access_sec[] = $s['id_sections'];
}

$access = Facecheck::checkPermissionUpload(get_current_user_id(),$photo->full_access);
$full_access = $access;
if (is_array($access)) {
    $full_access = false;
    if (in_array(3,$access) || in_array(4,$access)) {
        $full_access = true;
    }
}

$tmp_page = $page;
$page = $tmp_page;
unset($tmp_page);

$emails = $wpdb->get_results("
    SELECT `email`
    FROM `{$wpdb->prefix}facecheck_users_emails`
    WHERE `id_user` = {$_SESSION['facecheck_user']->id}
    ORDER BY `id`
", ARRAY_A);
ob_start();
?>

<script type="text/javascript">
    var isLogged = true;
    var page_url = '<?= get_permalink($facecheck_shortcode['history_scanface']); ?>';
    var professions = <?= json_encode($profCats) ?>;
    var specifications = <?= json_encode($specifications) ?>;
    var specsCount = <?= count($specifications) ?>;
    var page_url_report = '<?= get_permalink($facecheck_shortcode['history_scanface']) . $_SERVER['REQUEST_URI']; ?>';
</script>

<div class="facecheck-container-wrap stepContent">
    <div class="main-content">
        <?php if (count($photos)) { ?>
            <div class="row">
                <div class="reportHeader">
                    <ul class="report-buttons" style="float:right;<?php if (!is_user_logged_in()) { echo "display:none;"; }?>">
                        <li>
                            <?php $args = $queryArgs;
                            unset($args['facecheck_photo_id']); ?>
                            <?php if ($afterDeletePage == 1) unset($args['page_num']); else $args['page_num'] = $afterDeletePage; ?>
                            <a class="delete"
                               href="/wp-admin/admin-ajax.php?action=facecheck_delete_photo&id=<?= $photo->id ?>&redirect=<?= get_permalink(829) /* testing-history */?>"></a>
                            <span class="hint">Удалить отчет</span>
                        </li>
                        <li>
                            <a class="letter" href="#" onclick="return false;"></a>

                            <div class="hint">
                                <div style="margin-bottom: 5px;">Отправить отчет по адресам</div>

                                <?php $i = 0; ?>
                                <?php foreach ($emails as $email) { ?>
                                    <div>
                                        <input type="checkbox" class="email_checkbox" id="email_<?= $i ?>"
                                               value="<?= $email['email'] ?>" checked/>
                                        <label for="email_<?= $i ?>"><?= $email['email'] ?></label>
                                    </div>
                                    <?php $i++ ?>
                                <?php } ?>

                                <div>
                                    <a id="newAddress" href="#" onclick="return false;">Новый адрес</a><br>
                                </div>
                                <button onclick="sendLetter()">Отправить</button>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="report">
                    <div class="persone">
                        <div class="image"><img src="<?= content_url('/photos/face_' . $photo->id . '.jpg') ?>"/></div>
                        <div class="info">
                            <form action="" method="post">
                                <input type="hidden" id="report_id" value="<?= $photo->id ?>"/>
                                <div class="line">
                                    <span class="name">Дата тестирования:</span>
                                    <span class="date"><?= date('d.m.Y H:i:s',strtotime($photo->date)) ?></span>
                                </div>
                                <div class="line">
								    <span class="name">Имя тестируемого:</span>
                                    <input type="text" id="report_name" class="save_report" value="<?= $photo->name ?>" onkeyup="if(this.value.length>50)this.value=this.value.substr(0,49)"/>
                                </div>
                                <div class="line">
                                    <span class="name">Выбранные профессии:</span>
                                    <div class="checkboxes">
                                        <div class="profs">
                                        <span class="down-1" for-id="report_comment" onclick="showProfs(this)">+</span>
                                        <?php if(count($sel_prof)){
                                            foreach($sel_prof as $p){?>
                                                <a href="#prof<?=$p['id']?>"><?=$p['name']?> - <?=$p['percent']?>%</a><br>
                                        <?php }} ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="line">
                                    <span class="name">Личный коментарий:</span>
                                    <div class="checkboxes">
                                        <div class="comment">
                                            <span class="down-1" id="span_report_comment" onclick="showComment()">+</span>
                                            <textarea id="report_comment" onfocus="showComment()" onblur="showComment()" class="save_report" onkeyup="if(this.value.length>500)this.value=this.value.substr(0,499)"><?= $photo->comment ?></textarea>
                                        </div>
                                    </div>
                                </div>

								<div class="report-jump-sf">
                                <div class="line-2">
                                    <button onclick="location.href='#candidate';return false;">Описательная характеристика</button>
                                </div>
                                <div class="line-1">
                                    <button onclick="location.href='#profile';return false;">Профиль кандидата</button>
                                </div>
                                <div class="line-3">
                                    <button onclick="location.href='#profession';return false;">Сравнение с профилем профессии</button>
                                </div>
								</div> 
                            </form>

                        </div>
                    </div>

                    <?php if ($report) { ?>
                         <div class="candidate-menu" onclick="jQuery('#sections').toggle(); downToggle(this);"><a name="candidate"></a>
                            <h2 class="scanface-header-2">Описательная характеристика</h2> <span class="down active">^</span>
                        </div>
                        <div id="sections">
                        <?php foreach ($report['sections'] as $index => $section) {
                                    $count_access = true;
                                if(isset($access[4]) && !in_array($index,$access_sec)) $count_access = false;
                        ?>
                            <div class="section<?=(!in_array($index,$access_sec) && !$full_access || !$count_access)?' no-access':''?>">
                                <div class="image"><img src="<?= plugins_url('facecheck/sicons/' . $section['section_id'] . '.jpg') ?>"/></div>
				<div class="section-text-sf">
                                    <div class="title"> <?= $sections[$index]['name'] ?>
                                        <?php if((in_array($index,$access_sec)|| $full_access) && $count_access){ ?><a href="#" class="toggle" onclick="return false;"></a><?php } ?>
                                    </div>
                                    <div class="content">
                                    <?php
                                    if((in_array($index,$access_sec)|| $full_access) && $count_access) { ?>
                                        <?= $section['text'] ?>
                                    <?php } else { ?>
                                        <div class="legend__diagramm-lock" style="left: 50%;">
                                            <div class="get-access">
                                                <span class="lock"><img src="<?=$params_shortcode['report_restrict_message']['params']['image']?>"></span>
                                                <div class="text"><?=$params_shortcode['report_restrict_message']['content']?></div>
                                              <?php if($params_shortcode['report_restrict_message']['params']['button1-link']!='' && !is_user_logged_in()) { ?>
                                                <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button1-link']?>'">
                                                     <?=$params_shortcode['report_restrict_message']['params']['button1-text']?>
                                                </button>
                                              <?php } ?>
                                              <?php if($params_shortcode['report_restrict_message']['params']['button2-link']!='' && is_user_logged_in()) { ?>
                                                <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button2-link']?>'">
                                                    <?=$params_shortcode['report_restrict_message']['params']['button2-text']?>
                                                </button>
                                              <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
				</div>
                            </div>
                        <?php } ?>
                        <?php if(!$full_access) { ?>
                            <div>
                                <span class="gorizont-line-sf"></span>
                                <div><?=$params_shortcode['report_candidate_characteristic']?></div>
                            </div>
                        <?php } ?>
                        </div>

                        <div class="candidate-menu" onclick="jQuery('#profile-of-candidate').toggle(); downToggle(this);">
                            <a name="profile"></a>
                            <h2 class="scanface-header">Профиль кандидата</h2> <span class="down active">^</span>
                        </div>
                        <div id="profile-of-candidate">
                        <?php foreach ($report['specifications'] as $index => $specification) { ?>
                            <div class="legend<?=(!in_array($index,$access_spec) && !$full_access)?' no-access':''?>" data-spec-id="<?= $index ?>"
                                 data-spec-val="<?= $specification['value'] ?>">

                                <div class="legend__left">
                                    <div class="legend__circle-dash"></div>
                                    <div class="legend__head">
                                        <div class="legend__head-title legend__head-title-from">
                                            <?= $specification['name'] ?>
                                        </div>

                                        <div class="legend__head-title legend__head-title-to">
                                            
                                        </div>
                                    </div>
                                    <div class="legend__diagramm">
                                        <div class="legend__diagramm-progress">
                                            <div class="legend__diagramm-progress-inner"
                                                 style="position: relative;"></div>
                                            <?php if(in_array($index,$access_spec)|| $full_access) { 
                                                $percent = $specification['value'] * 100 / ($specification['right_value'] - $specification['left_value']) ?>
                                                <div class="legend__diagramm-circle" style="left: <?=$percent?>%;"></div>
                                            <?php } else { ?>
                                            <?php } ?>
                                        <ul class="legend__diagramm-grid">
                                            <?php for ($v = 1; $v <= 10; $v++) { ?>
                                                <li><span class="grid-sf"></span></li>
                                            <?php } ?>
                                        </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="legend__right">
                                <?php if(in_array($index,$access_spec)|| $full_access){ ?>
                                    <p><?= $specification['left_text'] ?></p>
                                    <p><?= $specification['right_text'] ?></p>
                                <?php } else {
                                    ?>
                                    <div class="legend__diagramm-lock" style="left: 50%;">
                                        <div class="get-access">
                                            <span class="lock"><img src="<?=$params_shortcode['report_restrict_message']['params']['image']?>"></span>
                                            <div class="text"><?=$params_shortcode['report_restrict_message']['content']?></div>
                                            <?php if($params_shortcode['report_restrict_message']['params']['button1-link']!='' && !is_user_logged_in()) { ?>
                                                <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button1-link']?>'">
                                                    <?=$params_shortcode['report_restrict_message']['params']['button1-text']?>
                                                </button>
                                            <?php } ?>
                                            <?php if($params_shortcode['report_restrict_message']['params']['button2-link']!='' && is_user_logged_in()) { ?>
                                                <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button2-link']?>'">
                                                    <?=$params_shortcode['report_restrict_message']['params']['button2-text']?>
                                                </button>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                                </div>

                                <div class="clear"></div>
                            </div>
                        <?php } ?>
                        <?php if(!$full_access) { ?>
                            <div>
                                <span class="gorizont-line-small"></span>
                                <div><?=$params_shortcode['report_candidate_profile']?></div>
                            </div>
                        <?php } ?>
                       </div>
                    
                    <!-- Соответствие тестируемого выбранной професси -->
                    <?php 
                        foreach($profCats as $p){
                            if(!count($p['profs'])) unset($profCats[$p['id']]);
                        }
                        if(count($profCats)){ ?>
                        <div class="candidate-menu" onclick="jQuery('.profession').toggle(); downToggle(this);"><a name="profession"></a>
                            <h2 class="scanface-header-3">Сравнение с профилем профессии</h2> <span class="down active">^</span>
                        </div>
                    
                        <div class="profession">
                            <div>
                                <div><?=$params_shortcode['report_profinfo']?></div>
                            </div>
                            <?php if (is_user_logged_in() && $full_access) { ?>
                            <button onclick="toggleProf()">Выбрать профессию</button>
                            
                            
                            <div id="professions" <?php if(count($sel_prof)){?>style="display:none"<?php } ?>>
                                <div class="search">
                                    <input type="text" name="search_prof" onkeyup="findProf(this.value,<?=$photo_id?>)" placeholder="Поиск профессии"  AUTOCOMPLETE="off" /><br>
                                    <ul class="find-prof"></ul>
                                </div>
                                <?php /*
                                <div class="list_group">
                                    <div class="column c1">
                                <?php 
                                    $i=0; 
                                    foreach ($profCats as $item) {
                                        $c = ceil(count($profCats)/3);
                                        $line = $i%$c;
                                ?>    
                                    <?php if($i!=0 && $line===0 && $i/$c!=3){?>
                                    </div><div class="column c<?=$i/$c+1?>"><?php } ?>
                                        <a href="javascript:void()" onclick="listingProf(jQuery(this).find('span'),<?=$item['id']?>);return false;"><span class="down left" onclick="listingProf(this,<?=$item['id']?>)">V</span><?= $item['name'] ?></a>
                                        <ul id="list<?=$item['id']?>" class="list_prof">
                                            <?php foreach ($item['profs'] as $prof) { ?>
                                                <li onclick="addProf('<?=$prof['id']?>','<?=$photo_id?>');"><?= $prof['name'] ?></li>
                                            <?php } ?>
                                        </ul>
                                <?php $i++; } ?>
                                    </div>
                                </div> 
                                <?php */

                                $categories = Facecheck::getProfessionCategories();
                                $categories[0] = array(
                                    'id' => 0,
                                    'name' => 'Другие профессии'
                                );

                                $professions = Facecheck::getProfessions($_SESSION['facecheck_user']->id);
                                foreach ($professions as $profession) {
                                    $categories[$profession['id_category']]['professions'][] = $profession;
                                }
                                $my_professions = Facecheck::getMyProfessions($_SESSION['facecheck_user']->id);
                                foreach ($my_professions as $profession) {
                                    $my_categories[$profession['id_category']]['professions'][] = $profession;
                                }

                                ?>
                                <div class="facecheck-container-wrap">
                                    <div class="container main-content">
                                        <?php if(count($my_categories)) { ?>
                                        <div class="category" id="sfUserCategories">
                                            <div class="title">Мои категории:<a href="#" class="toggle"
                                                                                          onclick="return false;"></a></div>
                                            <ul class="content">
                                                <li>
                                                    <a class="new" href="<?= add_query_arg(array('id_category' => $category['id']), get_permalink($facecheck_shortcode['specifications_scanface'])) ?>">+Новая профессия</a>
                                                </li>
                                                <?php foreach ($my_categories as $category) {
                                                if (isset($category['professions'])) {
                                                    foreach ($category['professions'] as $profession) { ?>
                                                        <li>
                                                            <a <?= $profession['id_user'] ? '' : 'class="base"' ?>
                                                                onclick="addProf('<?=$profession['id']?>','<?=$photo_id?>');return false;"
                                                                href="#"><?= $profession['name'] ?></a>
                                                            <?php if ($profession['id_user']) { ?>
                                                                <a class="delete" href="" data-id="<?= $profession['id'] ?>"
                                                                   onclick="return false;">удалить</a>
                                                            <?php } else { ?>
                                                                <span class="base_label" href="">базовая профессия</span>
                                                            <?php } ?>
                                                        </li>
                                                <?php }}} ?>
                                            </ul>
                                        </div>
                                        <?php } ?>

                                        <div class="professions">
                                            <?php foreach ($categories as $category) { ?>
                                            <?php if (isset($category['professions'])) { ?>
                                            <div class="category">
                                                <div class="title"><?= $category['name'] ?><a href="#" class="toggle" onclick="return false;"></a></div>

                                                <ul class="content" style="display:none">
                                                    <?php foreach ($category['professions'] as $profession) { ?>
                                                            <li>
                                                                <a <?= $profession['id_user'] ? '' : 'class="base"' ?>
                                                                    onclick="addProf('<?=$profession['id']?>','<?=$photo_id?>');return false;"
                                                                    href="#"><?= $profession['name'] ?></a>
                                                            </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <?php }} ?>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                            jQuery(function () {   
                                                jQuery('.professions .title .toggle').click(function () {
                                                var $this = jQuery(this).parent().next();
                                                var bar = 0;
                                                jQuery(this).toggleClass('open'); 
                                                if(jQuery(this).hasClass('open')) bar = 1;
                                                jQuery('.toggle').removeClass('open');
                                                jQuery('.professions ul').hide();
                                                if(bar) {
                                                    $this.show();
                                                    jQuery(this).addClass('open'); 
                                                }
                                                else {
                                                    jQuery(this).removeClass('open'); 
                                                }
                                            });

                                            jQuery('a.delete').click(function () {
                                                var $this = jQuery(this);
                                                if (confirm("Вы действительно хотите удалить эту профессию?")) {
                                                    jQuery.ajax({
                                                        url: '/wp-admin/admin-ajax.php',
                                                        type: 'post',
                                                        data: {
                                                            action: 'facecheck_delete_profession',
                                                            profession: $this.data('id')
                                                        },
                                                        success: function (response) {
                                                            $this.parent().hide();
                                                        }
                                                    });
                                                }
                                            })
                                        });
                                    </script>
                                </div>
                            </div> 
                            
                            <!-- Прикрепленные профессии -->
                        <div class="selected_prof" <?php if(!count($sel_prof)){?>style="display:none"<?php } ?>>
                                <?php if(count($sel_prof)){
                                    foreach($sel_prof as $p){?>
                                <div id="prof<?=$p['id']?>" class="prof-name"><?=$p['name']?> <?=$p['percent']?>% соответствия<span class="delete" onclick="delProf(<?=$p['id']?>)">X</span> <span class="down" onclick="listProf(this,<?=$p['id']?>)">V</span></div><a name="#prof<?=$p['id']?>"></a>
                                    <div id="prof_detail_<?=$p['id']?>" class="prof-detail" style="display:none">
                                        <?php foreach($p['detail'] as $pp){?>
                                            <div class="legend<?=(in_array($pp['id'],$access_spec)|| $full_access)?'':' no-access'?>" data-spec-id="<?=$pp['id']?>" data-spec-val="<?= $pp['value'] ?>">
                                                <div class="legend__left">
                                                    <div class="legend__circle-dash"></div>
                                                    <div class="legend__head">
                                                       <div class="legend__head-title legend__head-title-from">
                                                           <?= $pp['name'] ?>
                                                       </div>

                                                       <div class="legend__head-title legend__head-title-to">

                                                       </div>
                                                   </div>
                                                   <div class="legend__diagramm">
                                                       <div class="legend__diagramm-progress">
                                                           <div class="legend__diagramm-progress-inner"
                                                                style="position: relative; 
                                                                <?php if(in_array($pp['id'],$access_spec)|| $full_access) { ?>
                                                                    left: <?=$pp['left_value']?>0%; width: <?=($pp['right_value'] - $pp['left_value'])?>0%;
                                                                <?php } ?>">
                                                        </div>
                                                           <?php if(in_array($pp['id'],$access_spec)|| $full_access) { 
                                                               $percent = $pp['value'] * 100 / ($pp['s_right_value'] - $pp['s_left_value']) ?>
                                                               <div class="legend__diagramm-circle" style="left: <?=$percent?>%;"></div>
                                                           <?php } else { ?>
                                                               <div class="legend__diagramm-lock" style="left: 50%;">
                                                                   <div class="get-access">
                                                                       <span class="lock"><img src="<?=$params_shortcode['report_restrict_message']['params']['image']?>"></span>
                                                                       <div class="text"><?=$params_shortcode['report_restrict_message']['content']?></div>
                                                                     <?php if($params_shortcode['report_restrict_message']['params']['button1-link']!='' && !is_user_logged_in()) { ?>
                                                                       <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button1-link']?>'">
                                                                            <?=$params_shortcode['report_restrict_message']['params']['button1-text']?>
                                                                       </button>
                                                                     <?php } ?>
                                                                     <?php if($params_shortcode['report_restrict_message']['params']['button2-link']!='' && is_user_logged_in()) { ?>
                                                                       <button onclick="location.href='<?=$params_shortcode['report_restrict_message']['params']['button2-link']?>'">
                                                                           <?=$params_shortcode['report_restrict_message']['params']['button2-text']?>
                                                                       </button>
                                                                     <?php } ?>
                                                                   </div>
                                                               </div>
                                                           <?php } ?>
                                                       </div>
                                                       <ul class="legend__diagramm-grid">
                                                           <?php for ($v = 1; $v <= 10; $v++) { ?>
                                                               <li><span class="grid-sf-2"></span></li>
                                                           <?php } ?>
                                                       </ul>
                                                   </div>
                                               </div>

                                               <div class="legend__right">
                                                   <?php if(in_array($pp['id'],$access_spec)|| $full_access) { ?><p><?=$pp['description']?></p><?php } ?>
                                               </div>

                                               <div class="clear"></div>
                                           </div>
                                        <?php } ?>
                                    </div>
                                <?php }
                                }else{ ?>
                                <div class="no-prof">У вас пока нет прикрепленных профессий :(</div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <?php if(!$full_access) { ?>
                            <div>
                                <div><?=$params_shortcode['report_restrict_info']?></div>
                            </div>
                            <?php } ?>
                            <?php if (is_user_logged_in() && $full_access) { ?>
                            <button id="bottombutton" onclick="toggleProf()">Выбрать профессию</button>
                            <?php } ?>
                        </div>
                        <?php } ?> 
                     
                </div>
            </div>
            <?php } ?> 
        <?php } else { ?>
            <div class="row">
                <p>Извините, отчетов не найдено.</p>
            </div>
        <?php } ?>
    </div>
    <div style="display: none;">
        <div id="registration-popup" class="fchk popup">
            <div class="content">
                <p>Зарегистрируйтесь, что бы получить<br/>больше возможностей</p>
                <a class="fchk-button accent-color" href="<?= get_permalink($facecheck_shortcode['registration_scanface']) ?>">Бесплатная
                    регистрация</a>
            </div>
        </div>

        <div id="report_letter_success-popup" class="fchk popup">
            <div class="content">
                <p>Отчет отправлен</p>
            </div>
        </div>

        <div id="report_letter_fail-popup" class="fchk popup">
            <div class="content">
                <p>Произойшла ошибка при отправлении отчета</p>
            </div>
        </div>

        <div id="report_save_success-popup" class="fchk popup">
            <div class="content">
                <p>Отчет сохранен</p>
            </div>
        </div>

        <div id="report_save_fail-popup" class="fchk popup">
            <div class="content">
                <p>Произойшла ошибка при сохранении отчета</p>
            </div>
        </div>
        
        <div id="upload-popup" class="fchk popup">
            <div class="content">
                <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/loading.gif"/>
                <p>Пожалуйста подождите, профессия добавляется...</p>
            </div>
        </div>
        <div id="send-letter" class="fchk popup">
            <div class="content">
                <img src="<?= home_url() ?>/wp-content/plugins/facecheck/assets/img/loading.gif"/>
                <p>Пожалуйста подождите, отчет отправляется...</p>
            </div>
        </div>
    </div>
</div>

<?php
$output_string = ob_get_contents();
ob_end_clean();
//} // end if not login