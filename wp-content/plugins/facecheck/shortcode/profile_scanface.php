<?php
/*template name: Profile */
if (!is_user_logged_in()) {
    include dirname(__DIR__) . '/template/not_login_user.php';
} else {
    
global $wpdb;
global $facecheck_shortcode;
if (!empty($_POST)) {
//    die();
    $errors = facecheck_save_profile();
    array(
        'min_password_strength' => apply_filters( 'woocommerce_min_password_strength', 3 ),
        'i18n_password_error'   => esc_attr__( 'Please enter a stronger password.', 'woocommerce' ),
        'i18n_password_hint'    => esc_attr( wp_get_password_hint() ),
    );
}
ob_start();
?>

    <div class="facecheck-container-wrap">
        <div class="container main-content">
            <div class="auth">
                <form  method="post">
                    <div>
                        <input type="text" value="<?=$_SESSION['facecheck_user']->email?>" readonly />
                    </div>

                    <div>
                        <input type="text" name="user_name" value="<?=$_SESSION['facecheck_user']->name?>" />
                        <?php if (isset($errors['name'])) { ?><label for="email" class="error"><?=$errors['name']?></label><?php } ?>
                    </div>

                    <div>
                        <input type="password" name="user_old_password" value="" placeholder="Старый пароль" />
                        <?php if (isset($errors['old_password'])) { ?><label for="email" class="error"><?=$errors['old_password']?></label><?php } ?>
                    </div>

                    <div>
                        <input type="password" name="user_new_password" value="" placeholder="Новый пароль" />
                        <?php if (isset($errors['new_password'])) { ?><label for="email" class="error"><?=$errors['new_password']?></label><?php } ?>
                        <div id="password-strength" class="woocommerce-password-strength" aria-live="polite"></div>
                    </div>
                    <input type="submit" class="fchk-button accent-color submit" name="submit" value="Сохранить" />
                </form>
            </div>
        </div>
    </div>
    <script>
        function reset_new_pass($submitButton, $strengthResult, $hint) {
            // Сброс формы и определителя
            $submitButton.attr( 'disabled', 'disabled' ).addClass( 'disabled' );
            $strengthResult.removeClass( 'short bad good strong' );
            $hint.remove();
        }
        /* global wp, pwsL10n, wc_password_strength_meter_params */
        function checkPasswordStrength( $pass1,
                                        $strengthResult,
                                        $submitButton,
                                        blacklistArray, $hint) {

            var meter     = $strengthResult;
            var pass1 = $pass1.val();
            var error     = '';
            var hint_html = '<small class="woocommerce-password-hint">' + '<?= wp_get_password_hint() ?>' + '</small>';

            if ( '' === pass1 ) {
                // Сброс формы и определителя
                $submitButton.removeAttr( 'disabled', 'disabled' ).removeClass( 'disabled' );
                $hint.remove();
                $strengthResult.empty();
                $strengthResult.removeClass( 'short bad good strong' );
                return false;
            }

            // Сброс формы и определителя
            reset_new_pass($submitButton, $strengthResult, $hint);

            // Расширение массива черного списка  данными полей ввода и информацией о сайте
            blacklistArray = blacklistArray.concat( wp.passwordStrength.userInputBlacklist() )

            // Определение надежности пароля
            var strength = wp.passwordStrength.meter( pass1, blacklistArray, pass1 );
            // Error to append
            if ( strength < 3 ) {
                error = ' - ' + '<?= esc_attr__( 'Please enter a stronger password.', 'woocommerce' ) ?>';
            }


            // Добавление результатов анализа надежности
            switch ( strength ) {
                case 0 :
                    meter.addClass( 'short' ).html( pwsL10n['short'] + error );
                    meter.after( hint_html );
                    break;
                case 1 :
                    meter.addClass( 'bad' ).html( pwsL10n.bad + error );
                    meter.after( hint_html );
                    break;
                case 2 :
                    meter.addClass( 'bad' ).html( pwsL10n.bad + error );
                    meter.after( hint_html );
                    break;
                case 3 :
                    meter.addClass( 'good' ).html( pwsL10n.good + error );
                    break;
                case 4 :
                    meter.addClass( 'strong' ).html( pwsL10n.strong + error );
                    break;
                case 5 :
                    meter.addClass( 'short' ).html( pwsL10n.mismatch );
                    break;
            }

            // Функция определителя возвращает результат, даже если значение pass2 не заполнено,
            // однако кнопка подтверждения пароля будет активна только в том случае,
            // если пароль является надежным и оба поля заполнены
            if ( 3 === strength ) {
                $submitButton.removeAttr( 'disabled', 'disabled' ).removeClass( 'disabled' );
            }

            return true;
        }

        jQuery( document ).ready( function( $ ) {
            // Привязка к триггеру checkPasswordStrength
            $( 'body' ).on( 'keyup', 'input[name=user_new_password]',
                function( event ) {
                    checkPasswordStrength(
                        $('input[name=user_new_password]'),         // Поле ввода пароля
                        $('#password-strength'),           // Оценка надежности
                        $('input[type=submit]'),           // Кнопка подтверждения пароля
                        ['black', 'listed', 'word'], // Слова из черного списка
                        $('form').find( '.woocommerce-password-hint' )
                    );
                }
            );
        });
    </script>
<?php
$output_string = ob_get_contents();
ob_end_clean();
} // end if not login