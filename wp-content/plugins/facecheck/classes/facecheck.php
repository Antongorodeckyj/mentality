<?php

//require_once ABSPATH . 'wp-includes/unirest/Unirest.php';
require_once __DIR__ . '/unirest/Unirest.php';

class Facecheck
{
    private static $_session;

    /**
     * метод для инициализации плагина
     *
     */
    public static function install()
    {
        global $wpdb;

        /* $wpdb->query("
             CREATE TABLE `{$wpdb->prefix}facecheck_professions`
             (
               `id` INT NOT NULL AUTO_INCREMENT,
               `name` VARCHAR(128) NOT NULL,
               PRIMARY KEY (`id`)
             )
             AUTO INCREMENT = 1
         ");*/

        $wpdb->query("
          CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_users_emails` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `id_user` INT(11) NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `id_user` (`id_user`,`email`)
            ) AUTO_INCREMENT=1
        ");


        $wpdb->query("
          CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_specifications` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NOT NULL,
              `left_value` INT(11) DEFAULT NULL,
              `right_value` INT(11) NOT NULL,
              `left_text` TEXT NOT NULL,
              `right_text` TEXT NOT NULL,
              `position` INT(11) NOT NULL,
              `left_title` VARCHAR(255) NOT NULL,
              `right_title` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`)
            ) AUTO_INCREMENT=1
        ");


        $wpdb->query("
        CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_reports_specifications` (
          `faceparts` TEXT NOT NULL,
          `specification_id` INT(11) NOT NULL,
          `value` INT(11) NOT NULL
        )
        ");

        $wpdb->query("
        CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_professions_specifications` (
          `id_profession` INT(11) NOT NULL,
          `id_specification` INT(11) NOT NULL,
          `left_value` INT(11) NOT NULL,
          `right_value` INT(11) NOT NULL,
          `description` TEXT NOT NULL,
          UNIQUE KEY `id_profession` (`id_profession`,`id_specification`)
        )
        ");

        $wpdb->query("
        CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_profession_categories` (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `name` VARCHAR(255) NOT NULL,
          `parent` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`)
        ) AUTO_INCREMENT=1
        ");


        if (!$wpdb->query("SELECT id FROM `{$wpdb->prefix}facecheck_profession_categories`")) {
            $wpdb->query("INSERT INTO `{$wpdb->prefix}facecheck_profession_categories` (`name`) VALUES ('Руководитель'), ('Исполнитель')");
        }


        $wpdb->query("
                   CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_custom_specifications` (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `id_user` INT(11) NOT NULL,
          `id_profession` INT(11) NOT NULL,
          `id_specification` INT(11) NOT NULL,
          `left` INT(11) NOT NULL,
          `right` INT(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) AUTO_INCREMENT=1
        ");


        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_professions` (
          `id` INT(11) NOT NULL AUTO_INCREMENT,
          `name` VARCHAR(255) NOT NULL,
          `id_category` INT(11) NOT NULL,
          `id_user` INT(11) NOT NULL,
          PRIMARY KEY (`id`)
        ) AUTO_INCREMENT=1
        ");

        $wpdb->query("
           CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_users` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `email` varchar(128) NOT NULL,
              `password` varchar(32) NOT NULL,
              `name` varchar(128) NOT NULL,
              `date_birth` date NOT NULL,
              `date` datetime NOT NULL,
              `use_personal_specifications` tinyint(4) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM AUTO_INCREMENT=310 DEFAULT CHARSET=utf8
            ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_photos`
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `user_id` INT NOT NULL,
                `img` VARCHAR(36) NOT NULL,
                `face` VARCHAR(36) NOT NULL,
                `markers` TEXT NOT NULL,
                `date` DATETIME NOT NULL,
                `name` VARCHAR(128) NOT NULL,
                `comment` TEXT NOT NULL,
                `access` TEXT NOT NULL,
                `log` INT(1) DEFAULT '0',
                PRIMARY KEY (`id`)
            )
            AUTO_INCREMENT = 1
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_faceparts`
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(128) NOT NULL,
                PRIMARY KEY (`id`)
            )
            AUTO_INCREMENT = 1
        ");

        if (!$wpdb->query("SELECT id FROM `{$wpdb->prefix}facecheck_faceparts`")) {
            $wpdb->query("INSERT INTO `{$wpdb->prefix}facecheck_faceparts` (`name`) VALUES ('Брови'), ('Глаза'), ('Рот')");
        }


        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_facepart_types`
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `id_facepart` INT NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`)
            )
            AUTO_INCREMENT = 1
        ");

        if (!$wpdb->query("SELECT id FROM `{$wpdb->prefix}facecheck_facepart_types`")) {
            $wpdb->query("INSERT INTO `{$wpdb->prefix}facecheck_facepart_types` (`id_facepart`, `name`) VALUES
            ('1', 'Тип 1'), ('1', 'Тип 2'), ('1', 'Тип 3'), ('1', 'Тип 4'),
            ('2', 'Тип 1'), ('2', 'Тип 2'), ('2', 'Тип 3'), ('2', 'Тип 4'),
            ('3', 'Тип 1'), ('3', 'Тип 2'), ('3', 'Тип 3'), ('3', 'Тип 4')
        ");
        }


        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_report_sections`
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `name` VARCHAR(255) NOT NULL,
                `position` INT NOT NULL,
                PRIMARY KEY (`id`)
            )
            AUTO_INCREMENT = 1
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_reports`
            (
                `faceparts` TEXT NOT NULL,
                `section_id` INT NOT NULL,
                `text` TEXT NOT NULL
            )
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_user_categories`
            (
                `id` INT NOT NULL AUTO_INCREMENT,
                `user_id` INT NOT NULL,
                `name` VARCHAR(128) NOT NULL,
                `date` DATETIME NOT NULL,
                PRIMARY KEY (`id`)
            )
            AUTO_INCREMENT = 1
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_photo_category`
            (
                `photo_id` INT NOT NULL,
                `category_id` INT NOT NULL
            )
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_access_level`
             (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `name` text,
              `slug` varchar(255) DEFAULT NULL,
              `not_reg` int(1) DEFAULT NULL,
              `registered` int(1) DEFAULT NULL,
              PRIMARY KEY (`id`)
            )
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_access_to_sections`
             (
              `id_access` int(11) DEFAULT NULL,
              `id_sections` int(11) DEFAULT NULL
            )
        ");

        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_access_to_spec`
             (
              `id_access` int(11) DEFAULT NULL,
              `id_spec` int(11) DEFAULT NULL
            )
        ");
        $wpdb->query("
            CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}facecheck_order_user`
             (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `id_order` int(11) DEFAULT NULL,
              `id_user` int(11) DEFAULT NULL,
              `prod_id` int(11) DEFAULT NULL,
              `count_test` int(11) DEFAULT NULL,
              `day_count` int(11) DEFAULT NULL,
              `date_exp` int(11),
              PRIMARY KEY (`id`)
            )
        ");


        global $facecheck_shortcode;
        if (!empty($facecheck_shortcode)) {
            foreach ($facecheck_shortcode as $id_post) {
                // Создаем массив данных
                $my_post = array();
                $my_post['ID'] = $id_post;
                $my_post['post_status'] = 'publish';
                // Обновляем данные в БД
                wp_update_post($my_post);
//            wp_delete_post($id_post, true);
            }
        } else {

            $pages = [
                ['slug' => 'facecheck_login', 'shortcode' => 'login_scanface'],
                ['slug' => 'facecheck_registration', 'shortcode' => 'registration_scanface'],
                ['slug' => 'facecheck_History', 'shortcode' => 'history_scanface'],
                ['slug' => 'facecheck_Upload', 'shortcode' => 'upload_scanface'],
                ['slug' => 'facecheck_Markers', 'shortcode' => 'markers_scanface'],
                ['slug' => 'facecheck_Result', 'shortcode' => 'result_scanface'],
                ['slug' => 'facecheck_Professions', 'shortcode' => 'professions_scanface'],
                ['slug' => 'facecheck_Profile', 'shortcode' => 'profile_scanface'],
                ['slug' => 'facecheck_specifications', 'shortcode' => 'specifications_scanface'],
                ['slug' => 'facecheck_new_password', 'shortcode' => 'new_password_scanface']
            ];

            foreach ($pages as $page) {
                wp_insert_post(array(
                    'post_title' => $page['slug'],
                    'post_type' => 'page', // тип записи
                    'post_name' => mb_strtolower($page['slug']), // URL, будут совпадения? WordPress сам все испраит.
//                'comment_status' => 'closed', // обсждение закрыть
//                'ping_status' => 'closed', // пинги запретить
                    'post_content' => '[' . $page[shortcode] . ']',
                    'post_status' => 'publish', // опубликовать
//                'post_author' => get_current_user_id(), // кто будет автором
                ));
            }
        }

        if (!is_dir(WP_CONTENT_DIR . '/photos')) {
            mkdir(WP_CONTENT_DIR . '/photos');
        }


    }

    /**
     *удаление плагина
     */
    public static function uninstall()
    {
        global $wpdb;

        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_users`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_photos`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_faceparts`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_facepart_types`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_report_sections`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_reports`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_user_categories`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_photo_category`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_professions`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_users_emails`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_specifications`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_reports_specifications`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_professions_specifications`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_profession_categories`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_custom_specifications`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_access_to_spec`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_access_to_sections`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_access_level`");
        $wpdb->query("DROP TABLE `{$wpdb->prefix}facecheck_order_user`");

        $dir = opendir(WP_CONTENT_DIR . '/photos');
        while ($file = readdir($dir)) {
            if ($file != '.' && $file != '..') {
                unlink(WP_CONTENT_DIR . '/photos/' . $file);
            }
        }
        global $facecheck_shortcode;

        foreach ($facecheck_shortcode as $id_post) {
            wp_delete_post($id_post, true);
        }


        closedir($dir);
        rmdir(WP_CONTENT_DIR . '/photos');

    }

    /**
     * Деактивация плагина
     */
    public static function deactivate()
    {


        global $facecheck_shortcode;

        foreach ($facecheck_shortcode as $id_post) {
            // Создаем массив данных
            $my_post = array();
            $my_post['ID'] = $id_post;
            $my_post['post_status'] = 'draft';
            // Обновляем данные в БД
            wp_update_post($my_post);
        }

    }

    public static function init()
    {
        if (session_id() == "") {
            session_set_cookie_params(2592000);
            session_start();
            //setcookie(session_name(),session_id(),time()+2592000);
        }

        if (!isset($_SESSION['facecheck'])) {
            $_SESSION['facecheck'] = array();
        }
        self::$_session =& $_SESSION['facecheck'];
    }

    public static function getFaceparts()
    {
        global $wpdb;

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_faceparts` ORDER BY `id`", ARRAY_A);
        $result = array();

        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }

    public static function getFacepartTypes()
    {
        global $wpdb;

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_facepart_types` ORDER BY `id`", ARRAY_A);
        $result = array();

        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }

    public static function getSections()
    {
        global $wpdb;

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_report_sections` ORDER BY `position`", ARRAY_A);
        $result = array();

        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }

    public static function getSpecification()
    {
        global $wpdb;
//        $specification = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_specifications` WHERE `id`='{$_GET['specification']}'", ARRAY_A);
        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_specifications` ORDER BY `position`", ARRAY_A);
        $result = array();

        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }

    /**
     *
     * Получение уровней доступа
     * @param bool $id
     * @return array|null|object|void
     */
    public static function getAccessLevel($id = false)
    {
        global $wpdb;
        if ($id) {
            $row = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_access_level` WHERE `id`='{$id}'", ARRAY_A);
            return $row;
        }
        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_access_level`", ARRAY_A);

        $result = array();
        foreach ($rows as $row) {
            $users = [];
            $post_id = $wpdb->get_results("SELECT `post_id` FROM `{$wpdb->prefix}postmeta` WHERE `meta_key`='wdm_access_level' AND `meta_value`='{$row['id']}'", ARRAY_A);
            foreach ($post_id as $post) {
                $res = $wpdb->get_results("SELECT DISTINCT `id_user` FROM `{$wpdb->prefix}facecheck_order_user`  
                                                  WHERE `prod_id`='{$post['post_id']}'
                                                  AND `count_test`>0
                                                  OR `date_exp`+86400 > ".time(), ARRAY_A);
                foreach ($res as $r) {
                    $users[$r['id_user']] = $r;
                }

            }
            $result[$row['id']] = $row;
            $result[$row['id']]['users'] = count($users);
        }
        return $result;
    }

    /**
     * соотношение уровня доступа к секциям
     * @param $id_access
     * @return array
     */
    public static function getAccessSection($id_access)
    {
        global $wpdb;

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_access_to_sections` WHERE `id_access`='{$id_access}'", ARRAY_A);
        $result = array();
        foreach ($rows as $row) {
            $result[$row['id_sections']] = $row;
        }
        return $result;
    }


    /**
     * соотношение уровня доступа к характеристике
     * @param $id_access
     * @return array
     */
    public static function getAccessSpec($id_access)
    {
        global $wpdb;

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_access_to_spec` WHERE `id_access`='{$id_access}'", ARRAY_A);
        $result = array();
        foreach ($rows as $row) {
            $result[$row['id_spec']] = $row;
        }
        return $result;
    }

        /**
     * проверка прав на загрузку фото
     * @param $user_id
     * @param $photo_access_count - если на данную конкретную фотографию был куплен пакет на количество
     * @return array|bool
     */
    public static function checkPermissionUpload($user_id,$photo_access_count=false)
    {
        if (current_user_can('administrator')) {
            return true;
        }
        global $wpdb;
        $result = [];
        if ($user_id != 0) {
            
            // Для пользоавтеля без пакета но зарегистрированного
            $registered = $wpdb->get_results("
                            SELECT * FROM {$wpdb->prefix}facecheck_access_level WHERE registered=1
                            ", ARRAY_A);

            if (!empty($registered)) {
                foreach ($registered as $reg) {
                    $result[$reg['id']] = $reg['id'];
                }
            }
/*
            $permissions = $wpdb->get_results("
                SELECT {$wpdb->prefix}facecheck_order_user.day_count,{$wpdb->prefix}facecheck_order_user.id_order, {$wpdb->prefix}postmeta.meta_value as access_level FROM {$wpdb->prefix}facecheck_order_user
                 LEFT JOIN {$wpdb->prefix}postmeta ON {$wpdb->prefix}postmeta.post_id={$wpdb->prefix}facecheck_order_user.prod_id
                 WHERE {$wpdb->prefix}postmeta.meta_key='wdm_access_level'
                 AND id_user=$user_id
                 AND date_exp+86400 > TO_SECONDS(NOW())
                ", ARRAY_A);
*/
            // Для пользователя с безлимитом
            $permissions = $wpdb->get_results("
                SELECT *
                 FROM {$wpdb->prefix}facecheck_order_user o
                 WHERE id_user=$user_id
                 AND o.date_exp+86400 > ".time()."
                ", ARRAY_A);

            if (!empty($permissions)) {
                
                return true;
                /*
                foreach ($permissions as $permission) {
                    if ((int)$permission['day_count'] > 0) {
                        $prod_id = $permission['id_order'];
                        $day_count = $permission['day_count'];
                        $rs = $wpdb->get_results("
                SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key='_completed_date' AND post_id={$prod_id} AND ((TO_SECONDS( meta_value)+({$day_count}*86400))>=TO_SECONDS(NOW()))
                ", ARRAY_A);
                        if (!empty($rs)) {
                            $result[$permission['access_level']] = $permission['access_level'];
                        }
                    } elseif ($permission['day_count'] == "-1") {
                        $result[$permission['access_level']] = $permission['access_level'];
                    }
                }
*/
            }
            
            // Для пользователя с количеством тестирований
            $tarif = $wpdb->get_results("
                            SELECT * FROM {$wpdb->prefix}facecheck_access_level WHERE name='count_tests'
                            ", ARRAY_A);

            if (!empty($tarif) && $photo_access_count) {
                $result = [];
                foreach ($tarif as $reg) {
                    $result[$reg['id']] = $reg['id'];
                }
                return $result;
            }
        } else {
            $not_reg = $wpdb->get_results("
                                SELECT * FROM {$wpdb->prefix}facecheck_access_level WHERE not_reg=1
                                ", ARRAY_A);

            if (!empty($not_reg)) {
                foreach ($not_reg as $nt) {
                    $result[$nt['id']] = $nt['id'];
                }
            }
        }
        //$result[] = '3';
        if (!empty($result)) {
            return $result;
        }
        return false;
    }
    
    /**
     * проверка прав
     * @return array
     */
    public static function getAccess($photo_access_count=false)
    {
        global $wpdb;
        $permissions = self::checkPermissionUpload(get_current_user_id(),$photo_access_count);
        if ($permissions === true) {
            $sec = $wpdb->get_results("
               SELECT
              {$wpdb->prefix}facecheck_access_to_sections.id_sections
            FROM {$wpdb->prefix}facecheck_access_level
              LEFT JOIN {$wpdb->prefix}facecheck_access_to_sections ON {$wpdb->prefix}facecheck_access_to_sections.id_access = {$wpdb->prefix}facecheck_access_level.id", ARRAY_A);
            $spec = $wpdb->get_results("
               SELECT
                  {$wpdb->prefix}facecheck_access_to_spec.id_spec
                FROM {$wpdb->prefix}facecheck_access_level
                  LEFT JOIN {$wpdb->prefix}facecheck_access_to_spec ON {$wpdb->prefix}facecheck_access_to_spec.id_access = {$wpdb->prefix}facecheck_access_level.id", ARRAY_A);
        } else {
            $res = [];
            foreach ($permissions as $id_access_level) {
                $res[] = $id_access_level;
            }
            $res = implode(',', $res);
            $sec = $wpdb->get_results("
               SELECT
              {$wpdb->prefix}facecheck_access_to_sections.id_sections
            FROM {$wpdb->prefix}facecheck_access_level
              LEFT JOIN {$wpdb->prefix}facecheck_access_to_sections ON {$wpdb->prefix}facecheck_access_to_sections.id_access = {$wpdb->prefix}facecheck_access_level.id
            WHERE {$wpdb->prefix}facecheck_access_level.id IN({$res})
                ", ARRAY_A);
            $spec = $wpdb->get_results("
               SELECT
                  {$wpdb->prefix}facecheck_access_to_spec.id_spec
                FROM {$wpdb->prefix}facecheck_access_level
                  LEFT JOIN {$wpdb->prefix}facecheck_access_to_spec ON {$wpdb->prefix}facecheck_access_to_spec.id_access = {$wpdb->prefix}facecheck_access_level.id
                WHERE {$wpdb->prefix}facecheck_access_level.id IN({$res})
                ", ARRAY_A);
        }

        $access = [];
        foreach ($sec as $val) {
            $access['sec'][$val['id_sections']] = $val;
        }
        foreach ($spec as $val) {
            $access['spec'][$val['id_spec']] = $val;
        }
        return $access;
    }

    /**
     * получение категорий профессий
     * @return array
     */
    public static function getProfessionCategories($sort = false, $method_sort = "ASC")
    {
        global $wpdb;
        if ($sort) {
            if ($sort == "sort_pos") {
                $order = "id";
            } elseif ($sort == "sort_name") {
                $order = "name";
            }
        } else {
            $order = "name";
        }

        $rows = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_profession_categories` ORDER BY `{$order}` {$method_sort}", ARRAY_A);

        $result = array();
        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }

    /**
     * получение профессий
     * @param int $user
     * @return array
     */
    public static function getProfessions($user = 0)
    {
        global $wpdb;

        if ($user) {
            $rows = $wpdb->get_results("
                SELECT *
                FROM `{$wpdb->prefix}facecheck_professions`
                WHERE `id_user` = 0 OR `id_user` = $user
                ORDER BY `id_category`, `id_user`, `name`
            ", ARRAY_A);
        } else {
            $rows = $wpdb->get_results("
                SELECT * FROM `{$wpdb->prefix}facecheck_professions`
                WHERE `id_user` = 0
                ORDER BY `id_category`, `name`
            ", ARRAY_A);
        }

        $result = array();
        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }
    public static function getMyProfessions($user)
    {
        global $wpdb;

        $rows = $wpdb->get_results("
            SELECT *
            FROM `{$wpdb->prefix}facecheck_professions`
            WHERE `id_user` = $user
            ORDER BY `id_category`, `id_user`, `name`
        ", ARRAY_A);

        $result = array();
        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        return $result;
    }
    /**
     * получение характеристики по профессии
     * @param int $profession
     * @return array
     */
    public static function getSpecifications($profession = 0)
    {
        global $wpdb;

        $rows = $wpdb->get_results("
            SELECT *, 0 AS `prof_left`, 0 AS `prof_right`, '' AS `prof_desc`
            FROM `{$wpdb->prefix}facecheck_specifications`
        ", ARRAY_A);

        $result = array();

        foreach ($rows as $row) {
            $result[$row['id']] = $row;
        }

        $prows = array();
        if ($profession) {
            $rows = $wpdb->get_results("
                SELECT `s`.*, `ps`.`left_value` AS `prof_left`, `ps`.`right_value` AS `prof_right`, `ps`.`description` AS `prof_desc`
                FROM `{$wpdb->prefix}facecheck_specifications` AS `s`
                LEFT JOIN `{$wpdb->prefix}facecheck_professions_specifications` AS `ps` ON `ps`.`id_specification` = `s`.`id`
                WHERE `ps`.`id_profession` = $profession
            ", ARRAY_A);

            foreach ($rows as $row) {
                $prows[$row['id']] = $row;
            }
        }

        foreach ($result as $id => $row) {
            if (isset($prows[$id])) {
                $result[$id]['prof_left'] = $prows[$id]['prof_left'];
                $result[$id]['prof_right'] = $prows[$id]['prof_right'];
                $result[$id]['prof_desc'] = $prows[$id]['prof_desc'];
            } else {
                $result[$id]['prof_left'] = 0;
                $result[$id]['prof_right'] = 0;
                $result[$id]['prof_desc'] = '';
            }
        }

        return $result;
    }

    public static function getCustomSpecifications($profession)
    {
        global $wpdb;

        $result = array();

        $rows = $wpdb->get_results("
            SELECT `id_specification`, `left`, `right`
            FROM `{$wpdb->prefix}facecheck_custom_specifications`
            WHERE `id_user` = '{$_SESSION['facecheck_user']->id}' AND `id_profession` = $profession
        ", ARRAY_A);

        foreach ($rows as $row) {
            $result[$row['id_specification']] = $row;
        }

        return $result;
    }

    /**
     * сохранение результата
     * @param $faceparts
     * @param $text
     * @param $specification
     */
    public static function saveReport($faceparts, $text, $specification)
    {
        global $wpdb;

        if (is_array($faceparts)) {
            $faceparts = json_encode($faceparts);
        }

        $sections = self::getSections();

        foreach ($sections as $section) {
            if ($wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_reports` WHERE `faceparts`='{$faceparts}' AND `section_id`='{$section['id']}'")) {
                $wpdb->update($wpdb->prefix . 'facecheck_reports', array('section_id' => $section['id'], 'text' => wp_kses_stripslashes($text[$section['id']])), array('faceparts' => $faceparts));
            } else {
                $wpdb->insert($wpdb->prefix . 'facecheck_reports', array('faceparts' => $faceparts, 'section_id' => $section['id'], 'text' => wp_kses_stripslashes($text[$section['id']])));
            }
        }

        foreach ($specification as $id => $value) {
            if ($wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_reports_specifications` WHERE `specification_id` = {$id} AND `faceparts` = '$faceparts'")) {
                $wpdb->update($wpdb->prefix . 'facecheck_reports_specifications', array('value' => $value), array('faceparts' => $faceparts, 'specification_id' => $id));
            } else {
                $wpdb->insert($wpdb->prefix . 'facecheck_reports_specifications', array('faceparts' => $faceparts, 'specification_id' => $id, 'value' => $value));
            }
        }
    }

    /**
     * получение отчета
     * @param $faceparts
     * @return array
     */
    public static function getReport($faceparts, $id_photo = null)
    {
        global $wpdb;/*
        if (!current_user_can('administrator')) {
//            $accesss = self::getAccess();
            $res_access = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_photos` WHERE `id`='{$id_photo}'", ARRAY_A);
            $accesss = unserialize($res_access['access']);
        }
*/
        if (is_array($faceparts)) {
            $faceparts = json_encode($faceparts);
        }

        $rows1 = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_reports` WHERE `faceparts`='{$faceparts}' ORDER BY `section_id`", ARRAY_A);
        $rows2 = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_reports_specifications` WHERE `faceparts` = '{$faceparts}'", ARRAY_A);
        $rows3 = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_specifications`", ARRAY_A);

        $specifications = array();
        foreach ($rows3 as $row) {
            /*if (!current_user_can('administrator') && !key_exists($row['id'], $accesss['spec'])) {
                continue;
            }*/
            $specifications[$row['id']] = $row;
        }

        foreach ($rows2 as $row) {
            /*if (!current_user_can('administrator') && !empty($accesss['spec']) && !key_exists($row['specification_id'], $accesss['spec'])) {
                continue;
            }*/
            $specifications[$row['specification_id']]['value'] = $row['value'];
        }

        foreach ($specifications as $id => $specification) {
            if (!isset($specification['value'])) {
                $specifications[$id]['value'] = '';
            }
        }

        $report = array(
            'sections' => array(),
            'specifications' => array()
        );
        foreach ($rows1 as $row) {
            /*if (!current_user_can('administrator') && !key_exists($row['section_id'], $accesss['sec'])) {
                continue;
            }*/
            $report['sections'][$row['section_id']] = $row;
        }

        $report['specifications'] = $specifications;
        return $report;
    }

    /**
     * добавление фото
     * @param $source
     * @return int
     */
    public static function addPhoto($source)
    {
        global $wpdb;

        $is_file = is_file($source);
        $imagefile_data = base64_encode($is_file ? file_get_contents($source) : $source);

        ob_start();
        include __DIR__ . '/../templates/requests/uploadNewImage_File.xml.php';
        $request = ob_get_clean();

        $response = Unirest::post(
            'http://www.betafaceapi.com/service.svc/UploadNewImage_File',
            array(
                "Content-Type" => "application/xml"
            ),
            $request
        );

        $response = new SimpleXMLElement($response->body);
        $img = $response->img_uid->__toString();

        $wpdb->insert(
            $wpdb->prefix . 'facecheck_photos',
            array(
                'img' => $img,
                'user_id' => isset($_SESSION['facecheck_user']) ? $_SESSION['facecheck_user']->id : '0',
                'date' => '0000-00-00 00:00:00'
            )
        );

        $id = $wpdb->insert_id;

        if ($is_file) {
            if (move_uploaded_file($source, WP_CONTENT_DIR . '/photos/photo_' . $id . '.jpg')) {

            } else {
                rename($source, WP_CONTENT_DIR . '/photos/photo_' . $id . '.jpg');
            }
        } else {
            file_put_contents(WP_CONTENT_DIR . '/photos/photo_' . $id . '.jpg', $source);
        }

        return $id;
    }

    /**
     * удаление фото
     * @param $id
     */
    public static function deletePhoto($id)
    {
        global $wpdb;

        if (file_exists(WP_CONTENT_DIR . '/photos/photo_' . $id . '.jpg')) {
            unlink(WP_CONTENT_DIR . '/photos/photo_' . $id . '.jpg');
        }
        if (file_exists(WP_CONTENT_DIR . '/photos/face_' . $id . '.jpg')) {
            unlink(WP_CONTENT_DIR . '/photos/face_' . $id . '.jpg');
        }

        $wpdb->delete($wpdb->prefix . 'facecheck_photos', array('id' => $id));
        $wpdb->delete($wpdb->prefix . 'facecheck_photo_category', array('photo_id' => $id));
    }

    /**
     *
     * получение маркеров
     * @param $id
     * @param bool $reset
     * @return array
     */
    public static function getMarkers($id, $reset = false)
    {
        global $wpdb;

        list($img, $face, $markers) = $wpdb->get_row("SELECT `img`, `face`, `markers` FROM `{$wpdb->prefix}facecheck_photos` WHERE `id`='{$id}'", ARRAY_N);

        if (empty($face)) {
            ob_start();
            include __DIR__ . '/../templates/requests/getImageInfo.xml.php';
            $request = ob_get_clean();

            $response = Unirest::post(
                'http://www.betafaceapi.com/service.svc/GetImageInfo',
                array(
                    "Content-Type" => "application/xml"
                ),
                $request
            );

            $response = new SimpleXMLElement($response->body);
            if ($response->int_response->__toString() == '1') {
                return false;
            }

            if (isset($response->faces->FaceInfo)) {
                $face = $response->faces->FaceInfo->uid->__toString();
                $wpdb->update($wpdb->prefix . 'facecheck_photos', array('face' => $face), array('id' => $id));
            } else {
                return 'not_face';
            }
        }

        if (empty($markers) || $reset) {
            ob_start();
            include __DIR__ . '/../templates/requests/getFaceImage.xml.php';
            $request = ob_get_clean();

            $response = Unirest::post(
                'http://www.betafaceapi.com/service.svc/GetFaceImage',
                array(
                    "Content-Type" => "application/xml"
                ),
                $request
            );

            $response = new SimpleXMLElement($response->body);
            if ($response->int_response->__toString() == '1') {
                return false;
            }

            file_put_contents(WP_CONTENT_DIR . '/photos/face_' . $id . '.jpg', base64_decode($response->face_image->__toString()));

            $faceInfo = $response->face_info;

            $allMarkers = array();
            $markers = array();
            foreach ($faceInfo->points->children() as $point) {
                $allMarkers[$point->type->__toString()] = array(
                    'x' => $point->x->__toString(),
                    'y' => $point->y->__toString()
                );
            }

            //рот
            $markers['ml'] = $allMarkers['2048'];
            $markers['mt'] = $allMarkers['4259840'];
            $markers['mr'] = $allMarkers['2304'];
            $markers['mb'] = $allMarkers['4521984'];

            //глаза
            $markers['ylo'] = $allMarkers['2555904'];
            $markers['ylt'] = $allMarkers['2686976'];
            $markers['yli'] = $allMarkers['2818048'];
            $markers['ylb'] = $allMarkers['2949120'];

            $markers['yri'] = $allMarkers['2293760'];
            $markers['yrt'] = $allMarkers['2424832'];
            $markers['yro'] = $allMarkers['2031616'];
            $markers['yrb'] = $allMarkers['2162688'];

            //брови
            $markers['blo'] = $allMarkers['3866624'];
            $markers['blm'] = array(
                'x' => ($allMarkers['3735552']['x'] + $allMarkers['3997696']['x']) / 2,
                'y' => ($allMarkers['3735552']['y'] + $allMarkers['3997696']['y']) / 2
            );
            $markers['bli'] = $allMarkers['3604480'];

            $markers['bri'] = $allMarkers['3080192'];
            $markers['brm'] = array(
                'x' => ($allMarkers['3211264']['x'] + $allMarkers['3473408']['x']) / 2,
                'y' => ($allMarkers['3211264']['y'] + $allMarkers['3473408']['y']) / 2
            );
            $markers['bro'] = $allMarkers['3342336'];
            /** @var boolean $_SESSION
             * проверка на логирование теста
             */
            if ($_SESSION['facecheck_logs'] == true) {
                $log = 1;
                $_SESSION['facecheck_logs'] = false;
            } else {
                $log = 0;
            }
            $wpdb->update($wpdb->prefix . 'facecheck_photos', array('markers' => json_encode($markers), 'log' => $log), array('id' => $id));
        }

        return $markers;
    }

    /**
     * сохранение маркеров
     * @param $id
     * @param $markers
     */
    public static function saveMarkers($id, $markers, $date = null)
    {
        global $wpdb;

        if (is_array($markers)) {
            $markers = json_encode($markers);
        }

        $access = self::getAccess();
        $date = date('Y-m-d H:i:s');
        if (isset($_SESSION['facecheck_user']) && isset($_SESSION['chk_upl']) && $_SESSION['chk_upl'] == 2) {
            $user_id = get_current_user_id();
            $count_test = $wpdb->get_results("
                SELECT {$wpdb->prefix}facecheck_order_user.id_order FROM {$wpdb->prefix}facecheck_order_user
                 WHERE id_user=$user_id
                 AND count_test > 0
                ", ARRAY_A);
                $full_access = (count($count_test)>0)?1:0;
            self::setCountTest();
        }
        $wpdb->update($wpdb->prefix . 'facecheck_photos', array('markers' => $markers, 'date' => $date, 'access' => serialize($access),'full_access' =>$full_access), array('id' => $id));
        
        
        $_SESSION['photo_ids'][] = $id;

    }

    public static function getPhoto($id)
    {
        global $wpdb;

        /** @var array
         * получение данных тестирования по Id фотографии
         * проверка на принадлежность фото запрашиваему
         * $res */
        $res = $wpdb->get_row("SELECT * FROM `{$wpdb->prefix}facecheck_photos` WHERE `id`='$id' AND user_id='" . get_current_user_id() . "'");
        if (empty($res) && !headers_sent()) {
            wp_redirect('/');// редирект при пустом результате
            exit();
        }
        return $res;
    }
    
    /*
     * Вычисление угла между тремя точками a,b,c, в данном случае угла точки b (bx,by)
     */
    public static function ygol($ax,$ay,$bx,$by,$cx,$cy){
	$abx = $ax - $bx;
	$aby = $ay - $by;
	$cbx = $cx - $bx;
	$cby = $cy - $by;
	
	return rad2deg(acos(($abx*$cbx+$aby*$cby)/(sqrt($abx*$abx+$aby*$aby)*sqrt($cbx*$cbx+$cby*$cby))));
    }
    
    /**
     * получение результата
     * @param $id
     * @return array
     */
    public static function getResult($id)
    {
        global $wpdb;

        $photo = self::getPhoto($id);
        if ($photo->user_id && isset($_SESSION['facecheck_user'])) {
            $wpdb->update($wpdb->prefix . 'facecheck_photos', array('user_id' => $_SESSION['facecheck_user']->id), array('id' => $photo->id));
        }

        if (!empty($photo->markers)) {
            $markers = json_decode($photo->markers, true);
            $faceparts = array();

            $mMiddleLine = ($markers['ml']['y'] + $markers['mt']['y'] + $markers['mb']['y'] + $markers['mr']['y']) / 4; //средняя линия рта

            //брови
            $bMiddleLine = ($markers['blo']['y'] + $markers['blm']['y'] + $markers['bli']['y'] + $markers['bro']['y'] + $markers['brm']['y'] + $markers['bri']['y']) / 6; // средняя линия бровей
            /* Старая формула бровей
            if (($markers['blo']['y'] >= $markers['bli']['y']) && ($markers['bri']['y'] < $markers['bro']['y'])) { // тип 1
                $faceparts[1] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='1' AND `name`='Тип 1'");
            } elseif (($markers['blo']['y'] < $markers['bli']['y']) && ($markers['bri']['y'] >= $markers['bro']['y'])) { // тип 2
                $faceparts[1] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='1' AND `name`='Тип 2'");
            } elseif (($markers['blo']['y'] >= $markers['bli']['y']) && ($markers['bri']['y'] >= $markers['bro']['y'])) { // тип 3
                $faceparts[1] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='1' AND `name`='Тип 3'");
            } else { // тип 4
                $faceparts[1] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='1' AND `name`='Тип 4'");
            }
            */
            // новая формула бровей
            $type = 1;
            $yeb = self::ygol($markers['blm']['x'],$markers['blm']['y'],$markers['brm']['x'],$markers['brm']['y'],$markers['brm']['x'],$markers['brm']['y']+10);
            if($yeb<90-get_option('facecheck_constant_yogl')) $type = 3;
            if($yeb>90+get_option('facecheck_constant_yogl')) $type = 4;
            if($type==1){
                $dej = self::ygol($markers['blo']['x'],$markers['blo']['y'],$markers['blm']['x'],$markers['blm']['y'],$markers['bli']['x'],$markers['bli']['y']);
                $abg = self::ygol($markers['bro']['x'],$markers['bro']['y'],$markers['brm']['x'],$markers['brm']['y'],$markers['bri']['x'],$markers['bri']['y']);
                if((($dej+$abg)/2)<180-get_option('facecheck_constant_yrkb'))
                    $type = 2;
            }
            $faceparts[1] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='1' AND `name`='Тип $type'");
            
            // глаза
            $yLHeight = $markers['ylb']['y'] - $markers['ylt']['y']; // высота левого глаза
            $yRHeight = $markers['yrb']['y'] - $markers['yrt']['y']; // высота правого глаза
            $yHeight = ($yLHeight + $yRHeight) / 2; // средняя высота глаза
            $faceHeight = $mMiddleLine - $bMiddleLine; // расстояние от средней линии рта до средней линии бровей

            if (abs($yLHeight - $yRHeight) > $yHeight * get_option('facecheck_constant_eyetoeye') / 100) { // разница в размере глаз больше "eyetoeye" процентов среднего глаза
                if ($yLHeight > $yRHeight) { // тип 4
                    $faceparts[2] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='2' AND `name`='Тип 4'");
                } else { // тип 3
                    $faceparts[2] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='2' AND `name`='Тип 3'");
                }
            } elseif ($yHeight > $faceHeight * get_option('facecheck_constant_eyestoface') / 100) { // тип 2
                $faceparts[2] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='2' AND `name`='Тип 2'");
            } else { // тип 1
                $faceparts[2] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='2' AND `name`='Тип 1'");
            }

            //рот
            $mLRHeight = abs($markers['ml']['y'] - $markers['mr']['y']); // расстояние между крайними точками рта
            $mBTHeight = $markers['mb']['y'] - $markers['mt']['y']; // расстояние между средними точками рта (высота рта)
            if ($mLRHeight < $mBTHeight * get_option('facecheck_constant_mlrtombt')) { // расстояние между крайними точками рта меньше 10% от высоты рта
                if (($mMiddleLine - $markers['mt']['y']) >= ($markers['mb']['y'] - $mMiddleLine)) { // тип 1
                    $faceparts[3] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='3' AND `name`='Тип 1'");
                } else { //тип 2
                    $faceparts[3] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='3' AND `name`='Тип 2'");
                }
            } else {
                if ($markers['ml']['y'] > $markers['mr']['y']) { // тип 3
                    $faceparts[3] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='3' AND `name`='Тип 3'");
                } else { // тип 4
                    $faceparts[3] = $wpdb->get_var("SELECT `id` FROM `{$wpdb->prefix}facecheck_facepart_types` WHERE `id_facepart`='3' AND `name`='Тип 4'");
                }
            }

            if ($photo->date == '0000-00-00 00:00:00') {
                $wpdb->update($wpdb->prefix . 'facecheck_photos', array('date' => date('Y-m-d H:i:s')), array('id' => $photo->id));
            }
            return self::getReport($faceparts, $id);
        }
    }

    /**
     * сохранение фото
     * @param $id
     * @param $name
     * @param $comment
     * @param array $groups
     */
    public static function savePhoto($id, $name, $comment, $groups = array())
    {
        global $wpdb;

        $wpdb->update($wpdb->prefix . 'facecheck_photos', array('name' => $name, 'comment' => $comment), array('id' => $id));
    }

    /**
     * Сохранение профессии для отчета
     * @param int $user_id - id пользоваетеля
     * @param int $photo_id - id отчета (фото)
     * @param int $profession_id - id выбранной профессии
     */
    public static function saveUserReportProfession($user_id,$photo_id,$profession_id)
    {
        global $wpdb;
        
        $item = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}facecheck_user_report_professions WHERE user_id=$user_id AND photo_id=$photo_id AND profession_id=$profession_id", ARRAY_A);
        if(!count($item)){
            $e = $wpdb->insert($wpdb->prefix.'facecheck_user_report_professions', array('user_id'=>$user_id,'photo_id'=>$photo_id, 'profession_id'=>$profession_id));
        }
    }
    
    public static function findProf($val,$photo_id)
    {
        global $wpdb;
        $profs = $wpdb->get_results("
                SELECT p.*
                FROM {$wpdb->prefix}facecheck_professions p
                WHERE p.id_user=0 AND p.name like '$val%'"
            , ARRAY_A);
        $html = '';
        foreach($profs as $p){
            $html .= "<li onclick='addProf(\"{$p['id']}\",$photo_id)'>{$p['name']}</li>";
        }
        echo $html;
        exit;
    }
    public static function getSelectedPofessions($user_id,$photo_id,$report)
    {
        global $wpdb;
        $item = $wpdb->get_results("
            SELECT rp.*, p.name
            FROM {$wpdb->prefix}facecheck_user_report_professions rp
            LEFT JOIN {$wpdb->prefix}facecheck_professions p ON p.id = rp.profession_id
            WHERE user_id=$user_id AND 
                  photo_id=$photo_id
            ORDER BY percent desc"
        , ARRAY_A);

        if(count($item)){
            $proffs = array();
            foreach($item as $i){
                $proffs[]=$i['profession_id'];
            }
            $prof_sections = $wpdb->get_results("
                SELECT ps.*, s.name, s.left_title, s.right_title, s.id, s.right_value s_right_value, s.left_value s_left_value
                FROM {$wpdb->prefix}facecheck_professions_specifications ps
                LEFT JOIN {$wpdb->prefix}facecheck_specifications s ON s.id = ps.id_specification
                WHERE `id_profession` in (".implode(',',$proffs).")"
            , ARRAY_A);

            if(count($prof_sections)){
                $details = array();
                $percent = array();
                foreach($prof_sections as $i){
                    $i['value'] = $report['specifications'][$i['id']]['value'];
                    $percent[$i['id_profession']] = ($i['value']>=$i['left_value'] && $i['value']<=$i['right_value']) ? $percent[$i['id_profession']]+1 : $percent[$i['id_profession']];
                    $details[$i['id_profession']][]=$i;
                }
                foreach($item as &$i){
                    $i['detail']=$details[$i['profession_id']];
                    if($i['percent']==0){
                        $i['percent']=ceil($percent[$i['profession_id']]*100/count($report['specifications']));
                        $wpdb->query("UPDATE {$wpdb->prefix}facecheck_user_report_professions SET percent = '{$i['percent']}' WHERE id={$i['id']}");
                    }
                }
            }
        }

        return $item;
    }
    /**
     * обновление фото
     * @param $photo_id
     * @param array $group_ids
     */
    public static function updatePhotoGroups($photo_id, $group_ids = array())
    {
        global $wpdb;

        $wpdb->delete($wpdb->prefix . 'facecheck_photo_category', array('photo_id' => $photo_id));

        foreach ($group_ids as $gid) {
            $wpdb->insert($wpdb->prefix . 'facecheck_photo_category', array('photo_id' => $photo_id, 'category_id' => $gid));
        }
    }

    public static function getPhotoGroups($photo_id)
    {
        global $wpdb;

        $rows = $wpdb->get_results("
            SELECT `uc`.*
            FROM `{$wpdb->prefix}facecheck_photo_category` AS `pc`
            LEFT JOIN `{$wpdb->prefix}facecheck_user_categories` AS `uc` ON `uc`.`id`=`pc`.`category_id`
            WHERE `pc`.`photo_id`='{$photo_id}'
            GROUP BY `uc`.`id`
        ", ARRAY_A);

        $result = array();
        if ($rows) {
            foreach ($rows as $row) {
                $result[$row['id']] = $row;
            }
        }

        return $result;
    }

    /**
     * запись покупки пользователя
     * @param $order_id
     */
    public static function saveUserOrder($order_id)
    {
        global $wpdb;
        $user_id = (int)$wpdb->get_row("SELECT {$wpdb->prefix}postmeta.meta_value FROM wp_postmeta WHERE meta_key='_customer_user' AND post_id=$order_id", ARRAY_A) ["meta_value"];
        $products = $wpdb->get_results("
    SELECT DISTINCT {$wpdb->prefix}posts.ID as prod_id FROM {$wpdb->prefix}posts
    LEFT JOIN {$wpdb->prefix}postmeta ON {$wpdb->prefix}postmeta.post_id ={$wpdb->prefix}posts.ID
    WHERE ID IN (
          SELECT {$wpdb->prefix}woocommerce_order_itemmeta.meta_value FROM {$wpdb->prefix}woocommerce_order_itemmeta
            LEFT JOIN {$wpdb->prefix}woocommerce_order_items ON {$wpdb->prefix}woocommerce_order_itemmeta.order_item_id = {$wpdb->prefix}woocommerce_order_items.order_item_id
          WHERE meta_key='_product_id' AND {$wpdb->prefix}woocommerce_order_items.order_id = $order_id)
    ", ARRAY_A);

        $values = "";
        foreach ($products as $items) {
            if (strlen($values) > 0) {
                $values .= " ,";
            }
            $prod_id = $items['prod_id'];
            $products_meta = $wpdb->get_results("
        SELECT * FROM {$wpdb->prefix}postmeta WHERE post_id={$items['prod_id']}
    ", ARRAY_A);
            $date_exp = "";
            $day_count = "-1";
            $count_test = "-1";

            foreach ($products_meta as $prod_meta) {

                switch ($prod_meta['meta_key']) {
                    case '_regular_price':
                        $reg_price = $prod_meta['meta_value'];
                        break;
                    case 'wdm_upc_field':
                        $day_count = $prod_meta['meta_value'] == "" ? "-1" : $prod_meta['meta_value'];
                        break;
                    case 'wdm_maxuse_field':
                        $count_test = $prod_meta['meta_value'] == "" ? "-1" : $prod_meta['meta_value'];
                        break;
                }
            }
            if($day_count>0) {
                $date = $wpdb->get_row("
                SELECT max(date_exp) date_exp
                 FROM {$wpdb->prefix}facecheck_order_user o
                 WHERE id_user=$user_id
                 AND ( o.date_exp+86400 > ".time()." )
                ", ARRAY_A);
                 if($date['date_exp']>0) $time = $date['date_exp'];
                 else $time = time();
                $date_exp = $time + $day_count*86400;
                
                $wpdb->query("
                    UPDATE {$wpdb->prefix}facecheck_order_user SET count_test = 0 WHERE id_user={$user_id} AND count_test>0
                ");
            }
            $wpdb->query("
        INSERT INTO `{$wpdb->prefix}facecheck_order_user` (`id_order`,`id_user`,`prod_id`,`count_test`,`day_count`,`date_exp`) VALUE ('{$order_id}','{$user_id}','{$prod_id}','{$count_test}','{$day_count}','{$date_exp}')
        ");
        }

    }

    /**
     *установка счетчика загруженых фото
     */
    public static function setCountTest()
    {
        global $wpdb;
        $user_id = get_current_user_id();
        $wpdb->query("
          UPDATE {$wpdb->prefix}facecheck_order_user SET count_test = count_test-1 WHERE id_user={$user_id} AND count_test>0 LIMIT 1
        ");


    }

    /**
     *получение лога(ов) тестирования из таблицы facecheck_photos
     * @param int $id Id лога
     * @return array|string
     */
    public static function getLogs($id = null)
    {
        global $wpdb;
        if ($id) {
            $res = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}facecheck_photos WHERE `log`='1' AND `id`='{$id}'", ARRAY_A);
        } else {
            $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}facecheck_photos WHERE `log`='1'", ARRAY_A);
        }
        return $res;
    }
    public static function getProfessinsNames()
    {
        global $wpdb;
        $profs = [];
        $profs1 = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}facecheck_professions` WHERE id_user=0", ARRAY_A);
        foreach($profs1 as $p){
            $profs[]=$p['name'];
        }
        return $profs;
    }
}