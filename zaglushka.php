<?php 
// ����������� ���������� ����������
function is_mobile(){
	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		return false; // tablet 
	}
	if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		return true;
	}
	if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		return true;
	}
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda ','xda-');
	 
	if (in_array($mobile_ua,$mobile_agents)) {
		return true;
	}
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
		$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
		if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
		  return false; // tablet 
		}
		return true;
	}
	return false;
}
// ����������� �������� ������������� � ������
 function user_browser() {
	$agent = $_SERVER['HTTP_USER_AGENT'];
	preg_match("/(MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info); // ���������� ���������, ������� ��������� ����������� 90% ���������
        list(,$browser,$version) = $browser_info; // �������� ������ �� ������� � ����������
        if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) { // ����������� _�����_������_ ������ ����� (�� 8.50)
			return array('name'=>'Opera','version'=>$opera[1]);
		}
        if ($browser == 'MSIE') { // ���� ������� �������� ��� IE
                return array('name'=>'IE','version'=>$version);
        }
        if ($browser == 'Firefox') { // ���� ������� �������� ��� Firefox
                preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff); // ���������, �� ���������� �� ��� �� ������ Firefox
                if ($ff) return array('name'=>$ff[1],'version'=>$ff[2]);
        }
        if ($browser == 'Opera' && $version == '9.80') array('name'=>'Opera','version'=>substr($agent,-5)); // ���� ������� �������� ��� Opera 9.80, ���� ������ ����� �� ����� ������
        if ($browser == 'Version') return array('name'=>'Safari','version'=>$version); // ���������� ������
        if (!$browser && strpos($agent, 'Gecko')) return array('name'=>'IE','version'=>'1'); // ��� ������������ ��������� ���������, ���� ��� �� ������ Gecko, � ��������� ��������� �� ����
        
		return array('name'=>$browser,'version'=>$version);
}
// ���� � ���������� ��������� ������� ��������, ��� ��������� ������ �� ������
if(is_mobile()){
	include 'zaglushka_mobile.html';
	exit;
}
// ���� ������������ ��������, �� ������� ��������, ��� ��� ����� �������� ��� �� ������
$browser = user_browser();
if(($browser['name']=='IE'/* && (int)$browser['version']<=6*/) || 
   ($browser['name']=='Safari') || 
//   ($browser['name']=='Firefox' && (int)$browser['version']<=52) || 
   ($browser['name']=='Opera' && (int)$browser['version']<=21))
{
	ini_set('display_errors', 0);
	include 'zaglushka.html';
	exit;
}
